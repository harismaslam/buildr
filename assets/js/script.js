jQuery(document).ready(function () {
    jQuery(".commonform").validate({
        rules: {
            email: {
                required: true,
                fullEmail: true
            },
            password: {
                required: true,
                minlength: 8
            },
            password_confirm: {
                equalTo: "#password"
            },
            zipcode:{
                number:true,
                maxlength: 6
            },
            phone: {
                number:true,
                minlength: 10,
                maxlength: 10
            }
        },
        messages: {
            zipcode:{number:"Please enter a valid zipcode."},
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "userfile") {
                error.insertAfter(element.parent().parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    jQuery.validator.addMethod("fullEmail", function (value) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(value);
    }, 'Please enter a valid email address.');

    jQuery.validator.addMethod("greaterThan", function (value, element, params) {
        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) >= new Date($(params).val());
        }
        return isNaN(value) && isNaN($(params).val())
                || (Number(value) > Number($(params).val()));
    }, 'End date must be greater than or equal to Start date.');

});

jQuery(document).ready(function () {
    var hash = document.location.hash;
    var prefix = "tab_";
    if (hash) {
        jQuery('.nav-tabs a[href=' + hash.replace(prefix, "") + ']').tab('show');
    }
});

jQuery(function () {
    jQuery('[data-toggle="tooltip"]').tooltip({html: true});
});

