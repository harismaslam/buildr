-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2016 at 03:12 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `buildr`
--
CREATE DATABASE IF NOT EXISTS `buildr` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `buildr`;

-- --------------------------------------------------------

--
-- Table structure for table `apartment`
--

CREATE TABLE IF NOT EXISTS `apartment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apt_name` text NOT NULL,
  `apt_addr` text NOT NULL,
  `app_location` varchar(255) NOT NULL,
  `apt_img` text NOT NULL,
  `apt_designer` varchar(255) NOT NULL,
  `apt_designer_id` int(11) NOT NULL,
  `apt_owner` varchar(255) NOT NULL,
  `apt_owner_id` int(11) NOT NULL,
  `apt_lat` double NOT NULL,
  `apt_long` double NOT NULL,
  `proj_cat_id` int(11) NOT NULL,
  `in_trash` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `apartment`
--

INSERT INTO `apartment` (`id`, `apt_name`, `apt_addr`, `app_location`, `apt_img`, `apt_designer`, `apt_designer_id`, `apt_owner`, `apt_owner_id`, `apt_lat`, `apt_long`, `proj_cat_id`, `in_trash`) VALUES
(1, 'Green Apple Regency', 'Pune, India', '', 'Image_0000s_0001s_0003_Interior-Design-Small-Living-Room-NYC.png', 'Nijesh Buildr', 11, 'Haris Aslam', 9, 8.60105, 76.930218, 2, 0),
(2, 'Olive Ocean Regency', 'chennai, India', '', 'Home-Extended_0002s_0002_Interior-Designers-Image1.png', 'Nijesh Buildr', 12, 'Haris Aslam', 15, 8.598334, 76.875286, 4, 0),
(3, 'Manchester Regency', 'Mumbai, India', '', 'Home-Extended_0001s_0001_Architects,-Designers-Image6.png', 'Arjun B', 12, 'Rishana R', 15, 8.563368, 76.861553, 2, 0),
(4, 'Smart city Heights', 'Trivandrum, India', '', 'Home-Extended_0001s_0001_Architects,-Designers-Image7.png', 'Arjun B', 12, 'Rishana R', 15, 8.522287, 76.97691, 5, 0),
(5, 'SFS Arcade', 'Koramangla, Banglore, India', '', 'Home-Extended_0001s_0001_Architects,-Designers-Image8.png', 'Arjun B', 12, 'Rishana R', 15, 8.531114, 76.906528, 1, 0),
(6, 'King Space Palm Shades', 'Koramangla, Banglore, India', '', 'Home-Extended_0001s_0001_Architects,-Designers-Image9.png', 'Arjun B', 12, 'Rishana R', 15, 8.569818, 76.840611, 1, 0),
(7, 'Iris Blue Park', 'Koramangla, Banglore, India', '', 'Home-Extended_0001s_0001_Architects,-Designers-Image10.png', 'Arjun B', 12, 'Rishana R', 15, 8.557936, 76.886616, 3, 0),
(8, 'Skyline Tower', 'Delhi, India', '', 'Home-Extended_0001s_0001_Architects,-Designers-Image11.png', 'Arjun B', 12, 'Rishana R', 15, 8.6075, 76.902065, 1, 0),
(9, 'Diamond Valley', 'Hyderabad, India', '', 'Home-Extended_0003s_0001_Modular-Kitchen-Image6.png', 'Arjun B', 0, 'Rishana R', 0, 8.553522, 76.86945, 1, 0),
(10, 'Diamond Valley', 'Hyderabad, India', '', 'Home-Extended_0003s_0001_Modular-Kitchen-Image11.png', 'Arjun B', 0, 'Rishana R', 0, 8.505989, 76.932278, 1, 0),
(11, 'Test project', 'Kazhakootam, Trivandrum', '', 'pay.png', '', 11, '', 0, 0, 0, 1, 0),
(12, 'Test project2', 'Kakkanad Cohin Kerala', '', '', '', 11, '', 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `apartment_images`
--

CREATE TABLE IF NOT EXISTS `apartment_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apt_gall_img_title` varchar(255) NOT NULL,
  `apt_gall_img` text NOT NULL,
  `apt_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `apartment_images`
--

INSERT INTO `apartment_images` (`id`, `apt_gall_img_title`, `apt_gall_img`, `apt_id`) VALUES
(4, 'Bathroom', 'Home-Extended_0004s_0001_Plumber-Pros5.png', 3),
(5, 'Kitchen', 'Home-Extended_0003s_0001_Modular-Kitchen-Image5.png', 3),
(6, 'Living Room', 'Home-Extended_0006s_0001_Eco-Friendly-Image5.png', 3),
(7, 'Bathroom', 'Home-Extended_0004s_0001_Plumber-Pros6.png', 4),
(9, 'Living Room', 'Home-Extended_0006s_0001_Eco-Friendly-Image6.png', 4),
(10, 'Bathroom', 'Home-Extended_0004s_0001_Plumber-Pros7.png', 5),
(11, 'Kitchen', 'Home-Extended_0003s_0001_Modular-Kitchen-Image7.png', 5),
(12, 'Living Room', 'Home-Extended_0006s_0001_Eco-Friendly-Image7.png', 5),
(13, 'Bathroom', 'Home-Extended_0004s_0001_Plumber-Pros8.png', 6),
(14, 'Kitchen', 'Home-Extended_0003s_0001_Modular-Kitchen-Image8.png', 6),
(15, 'Living Room', 'Home-Extended_0006s_0001_Eco-Friendly-Image8.png', 6),
(16, 'Bathroom', 'Home-Extended_0004s_0001_Plumber-Pros9.png', 7),
(17, 'Kitchen', 'Home-Extended_0003s_0001_Modular-Kitchen-Image9.png', 7),
(18, 'Living Room', 'Home-Extended_0006s_0001_Eco-Friendly-Image9.png', 7),
(19, 'Bathroom', 'Home-Extended_0004s_0001_Plumber-Pros10.png', 8),
(20, 'Kitchen', 'Home-Extended_0003s_0001_Modular-Kitchen-Image10.png', 8),
(21, 'Living Room', 'Home-Extended_0006s_0001_Eco-Friendly-Image10.png', 8),
(27, 'Bathroom', 'Home-Extended_0004s_0001_Plumber-Pros13.png', 2),
(28, 'Living Room', 'banner-brands.jpg', 11);

-- --------------------------------------------------------

--
-- Table structure for table `apartment_images_questions`
--

CREATE TABLE IF NOT EXISTS `apartment_images_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `questioner` varchar(255) NOT NULL,
  `questioner_id` int(11) NOT NULL,
  `answerer` varchar(255) NOT NULL,
  `answerer_id` int(11) NOT NULL,
  `apt_img_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `apartment_images_questions`
--

INSERT INTO `apartment_images_questions` (`id`, `question`, `answer`, `questioner`, `questioner_id`, `answerer`, `answerer_id`, `apt_img_id`) VALUES
(1, 'what is the type of glass used for windows', 'and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Paris', 18, 'Kannan', 16, 27),
(2, 'what is the type of glass used for windows\r\n\r\nCan you say the estimate for this design, including labour charge', 'and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining', 'Angelina', 19, 'Haris', 9, 27),
(3, 'what is the type of glass used for windows', 'and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining', 'Angelina', 19, 'Haris', 9, 21);

-- --------------------------------------------------------

--
-- Table structure for table `apartment_profs`
--

CREATE TABLE IF NOT EXISTS `apartment_profs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prof_id` int(11) NOT NULL,
  `apt_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `idea`
--

CREATE TABLE IF NOT EXISTS `idea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `tags` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `idea`
--

INSERT INTO `idea` (`id`, `title`, `content`, `user_id`, `user_name`, `cat_id`, `tags`) VALUES
(1, 'How to keep kitchen dry', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 0, 'Rishana R', 2, 'kitchen, dry'),
(2, 'How to arrange living room items', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 0, 'Haris Aslam', 5, 'living room, arrange'),
(3, 'How to keep kitchen clean', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nIt is a long established fact that a reader wil\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 0, 'Berlin R', 5, 'kitchen, clean, odour');

-- --------------------------------------------------------

--
-- Table structure for table `import_architects`
--

CREATE TABLE IF NOT EXISTS `import_architects` (
  `Name_of_company` varchar(34) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `What_style_they_are_doing` varchar(122) CHARACTER SET utf8 DEFAULT NULL,
  `Name_of_the_Designer` varchar(29) CHARACTER SET utf8 DEFAULT NULL,
  `Company_Website` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Contact_nos` varchar(41) CHARACTER SET utf8 DEFAULT NULL,
  `Eamil_ID` varchar(35) CHARACTER SET utf8 DEFAULT NULL,
  `Client_details` varchar(188) CHARACTER SET utf8 DEFAULT NULL,
  `Column_9` int(11) DEFAULT NULL,
  `Coverage_Area` varchar(9) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `import_architects`
--

INSERT INTO `import_architects` (`Name_of_company`, `Address`, `What_style_they_are_doing`, `Name_of_the_Designer`, `Company_Website`, `Contact_nos`, `Eamil_ID`, `Client_details`, `Column_9`, `Coverage_Area`) VALUES
('Kamat & Rozario Architecture', ' #509, Ground Floor, 4th C Cross Road, HRBR 2nd Block, Kalyan Nagar, Bengaluru, Karnataka 560043', 'NA', 'Lester Rozario', 'www.kamatrozario.com', '9886746401', 'contact@kamatrozario.com', 'NA', NULL, 'Bangalore'),
('Archemist Architects', '10/28 NK plaza Dr Rajkumar Road,, Rajaji Nagar, Bengaluru, Karnataka 560010', 'Architectural Design,Sustainable Design,Interior Design,Construction Supervision,Project Management,Documentation Services', 'Divya', 'www.archemistarchitects.com', '080 -2352 7212', 'NA', 'Bharathshah,B.S krishnappa,krishnappa  & natarajan,shankaran,satyanarayana,prabhakaran,suveer,', NULL, 'Bangalore'),
('CollectiveProject', '686, 9th A Main Rd, Defence Colony, Indiranagar, Bengaluru, Karnataka 560038', 'NA', 'Cyrus Patell ,Eliza Higgins  ', 'www.collective-project.com', 'NA', ' info@collective-project.com', NULL, NULL, 'Bangalore'),
('Bindu Narayan', '"T-5 Shalimar Arcade, 10th Cross"Bangalore 560027', 'Building design, architecture, interior design, engineering, green buildings and turnkey projects ', 'Bindu Narayan', 'www.coordinates.in', '080-65791077 9900600815', 'NA', 'NA', NULL, 'Bangalore'),
('Living Edge Architects & Designers', ' 50, 16th A Main Rd, HAL 2nd Stage, Indiranagar, Bengaluru, Karnataka 560008', 'NA', 'Shone Saju', 'www.lead.com', '                           080 - 41250257', 'admin@lead.co.in', 'Pavan Residence,Shanmugam Residence,Thomas Residence,Sowmya Ramesh Residence,Ramesh Residence,Renju Residence,John Developers Layout,Sparrow CMAC Layout,Apartment at Coimbatore,CBSE School', NULL, 'Bangalore'),
('InForm Architects', ' 422, 2nd Floor, 9th Main Rd, Banashankari Stage II, Bengaluru, Karnataka 560070', 'NA', 'NA', 'www.informarchitects.com', '                            080 2671 3360', 'general@informarchitects.com', NULL, NULL, 'Bangalore'),
('DePanache Interiors', '3rd Floor, 5th Block, KHB Colony, 454, 2nd Cross Rd, Koramangala, Bengaluru, Karnataka 560095', 'NA', 'NA', 'www.depanache.in', '080-40952727', 'atreyee@depanacheinteriors.com', NULL, NULL, 'Bangalore'),
('Architecture Continuous', '#13, UDN Altius, 4th Floor, (Above Federal Bank), 100 ft Ring Road, 15th Cross Rd, JP Nagar Phase 6, Bengaluru, Karnataka 560078', 'NA', ' Adithya  G. Kashyap', 'www.architecturecontinuous.com', '93799 18468', 'business@architecturecontinuous.com', 'Greencity-Eutopia,Poorvi Dev, Mangalore,Sampurna Bhoomika,Thirumala Anemone,Whispering winds,DSI Commercial,Unicon-Foland,Purple circle,Chennai Monument', NULL, 'Bangalore'),
('Naveen Architects', 'No.112, Ground Floor, Oxford Towers, No.139, Airport Road, Bangalore - 560008', 'NA', 'Naveen ', 'NA', '080-41262260 41262261, 41151600, 41151700', 'NA', 'Rajiv Talwar,Prakash Narayanan,', NULL, 'Bangalore'),
('Vijaykumar Associates', ' 5 & 6, 24th Mn, Chaitanya Complex, 12th Crs, 2nd Phase, J P Ngr, Bengaluru, Karnataka 560078', 'NA', 'Vijay kumar', 'www.vkaa.net', '9844016568', 'mailus@vkaa.net', NULL, NULL, 'Bangalore');

-- --------------------------------------------------------

--
-- Table structure for table `import_electrician`
--

CREATE TABLE IF NOT EXISTS `import_electrician` (
  `Name_of_company` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(90) CHARACTER SET utf8 DEFAULT NULL,
  `Name_of_the_Designer` varchar(12) CHARACTER SET utf8 DEFAULT NULL,
  `Company_Website` varchar(37) CHARACTER SET utf8 DEFAULT NULL,
  `Mobile` varchar(23) CHARACTER SET utf8 DEFAULT NULL,
  `Contact_no` varchar(12) CHARACTER SET utf8 DEFAULT NULL,
  `Eamil_ID` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `Client_details` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `Coverage_Area` varchar(9) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `import_electrician`
--

INSERT INTO `import_electrician` (`Name_of_company`, `Address`, `Name_of_the_Designer`, `Company_Website`, `Mobile`, `Contact_no`, `Eamil_ID`, `Client_details`, `Coverage_Area`) VALUES
('KNZ Construction', 'No.40,Khamar Manzil,Opp.Johnson School,M.G paly,Bommanahalli, Bangalore, Karnataka.', 'NA', 'http://knzconstruction.in/electrical/', '9004030022', '08050048640 ', 'info@knzconstruction.in', 'NA', 'Bangalore'),
('Bestmaintain ', '#531, Amarjyoti HBCS Layout, Koramangala Intermediate Ring Road Domlur, Bangalore - 560071', 'NA', 'http://www.bestmaintain.com/about-us', '8494930024', '8151 800 800', 'info@bestmaintain.com', 'NA', 'Bangalore'),
('Total Care Services ', 'No.20 /3, Ground Floor, 1st Cross, Magrath Road, Ashok Nagar, Bangalore 560025', ' James /Rita', 'http://www.totalcareservices.in', '094480 41248 9008337697', '080 25306448', 'info@totalcareservices.in', 'NA', 'Bangalore'),
('Handy Fix', '#426, 3rd Cross, 4th Block,1st Stage, HBR Layout, Kalyan Nagar Post,Bangalore – 560 043.', 'NA', 'http://www.handyfix.in', '8105685679', '8892300000', 'aravind@handyfix.in', 'NA', 'Bangalore');

-- --------------------------------------------------------

--
-- Table structure for table `import_interior`
--

CREATE TABLE IF NOT EXISTS `import_interior` (
  `Name_of_the_Designer` varchar(21) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(146) CHARACTER SET utf8 DEFAULT NULL,
  `What_style_they_are_doing` varchar(218) CHARACTER SET utf8 DEFAULT NULL,
  `Description_of_each_work` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `Client_data_and_cost_of_project` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `Precise_budget_and_year_of_the_work` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `Company_Website` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `Email_id` varchar(43) CHARACTER SET utf8 DEFAULT NULL,
  `Contact_no` varchar(14) CHARACTER SET utf8 DEFAULT NULL,
  `Contact_person` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Mobile_no` varchar(21) CHARACTER SET utf8 DEFAULT NULL,
  `Client_details` varchar(530) CHARACTER SET utf8 DEFAULT NULL,
  `Coverage_Area` varchar(9) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `import_interior`
--

INSERT INTO `import_interior` (`Name_of_the_Designer`, `Address`, `What_style_they_are_doing`, `Description_of_each_work`, `Client_data_and_cost_of_project`, `Precise_budget_and_year_of_the_work`, `Company_Website`, `Email_id`, `Contact_no`, `Contact_person`, `Mobile_no`, `Client_details`, `Coverage_Area`) VALUES
('ELENZA INTERIOR', 'Survey No. 28/1, 28/2, 29/1, Channenahalli Village,, Tavarekere, Hobli, Bengaluru, Karnataka 562130', 'Interior & Furniture, Interior Designers,Modular Kitchens, Wardrobes, Cabinets and Shutters', 'NA', 'NA', 'NA', 'http://www.elenzaindia.com/contact.html', ' dipan.malde@elenzaindia.com', '096111 01673', 'Dipan Malde', '9902171673', 'NA', 'Bangalore'),
('Cross Currents ', '64, 6th Main Rd, Defence Colony, Indiranagar, Bengaluru, Karnataka 560038', 'Developing a concept for each space,Providing a furniture layout,Providing working drawings to support the final concept,Selection of tiles and sanitary fittings,Selection of soft furnishing,Selection of light fittings', 'NA', 'NA', 'NA', 'http://crosscurrents.co.in/contact.html', ' info@crosscurrents.co.in', '80 2520 4438', 'NA', '      080-2520 4438', 'NA', 'Bangalore'),
('RITU INTERIORS', 'Brigade Rd, Ashok Nagar, Bengaluru, Karnataka 560025', 'Flooring to Ceilings in Modular Kitchens, Bed Rooms, Living Areas, Pooja Rooms, Bathrooms etc.', 'NA', 'NA', 'NA', 'www.rituinteriots.co.in ', 'info@rituinteriors.co.in ', '91-8970970108', 'Mahesh Porwal ', '9448254533', 'NA', 'Bangalore'),
('Kuvio Studio', '#4A, 2nd Floor, 8th Main Road, 4th Block, Koramangala ,Bangalore - 560034 ', 'Interior  designing ', 'NA', 'NA', 'NA', 'http://www.kuviostudio.com/', 'richa@kuviostudio.com', '7760299007', 'Richa Singh', '7760299007', 'NA', 'Bangalore'),
('Bluesky Concepts', 'No. 40/1, 5th Cross, 3rd Main, Wilson Garden,Bangalore  - 560027', 'designers we are psychologists,turnkey projects', 'NA', 'NA', 'NA', 'http://www.blueskyconcepts.in/contact_us.html', 'blueskyconcepts12@gmail.com', '096869 72677', 'Anand J', '         96 86 972677', 'Ghangadhar,kishor,krishnappa,lokesh,mahadev,mahesh,manjunath,podar,ravi,saji paul,sandeep,shabeer,vijaynagar,thimmareddy', 'Bangalore'),
('M&M Connect Interiors', 'No: 91, 2nd floor, 1st C Main Road,Jakkasandra Extn, Koramangala Bangalore India 34', 'comprehensive range of interior design and furnishing solutions for residential and commercial spaces.', 'NA', 'NA', 'NA', 'http://www.savioandrupa.com/contact.html', ' savioandrupa@gmail.com connect@m-and-m.in ', '91 80 25530777', 'NA', '080 - 25525777', 'DESIGNER''S DEN,MUNNAR VILLA ,LEGACY ARISTON,SOBHA ALTHEA ,SKYLINE IVY ,LEAGUE ,CLASSIC CONDO ,BTM ,PURVA HIGHLANDS,ELECTRONIC CITY', 'Bangalore'),
('Design Arc Interior', '8th Main Rd, Sector 3, HSR Layout, Bengaluru, Karnataka 560102', 'interior designing, commercial interior designing as well as 3D visualization', 'NA', 'NA', 'NA', 'http://www.designarcinteriors.com', 'info@designarcinteriors.com', '080 - 25530777', 'NA', '9620789719', 'NA', 'Bangalore'),
('Pancham interior', '26, Kalpana Chawla Rd, Central Excise Colony, RMV Extension Stage 2, Bengaluru, Karnataka 560094', 'All kind of Interior Designer', 'NA', 'NA', 'NA', 'http://www.panchaminteriors.in/interior_design_services.html', 'info@panchaminteriors', '080-23419491', 'NA', '9449799491', 'Vijay Gupta,', 'Bangalore'),
('Gd Design', '465, Bnsk 2nd Stg, Subramanya Pura Road, Bendre Nagar, Bendre Nagar, Bengaluru, Karnataka 560070', 'Architectural  Interior  Urban design and Masterplaning ', 'NA', 'NA', 'NA', 'http://www.indiamart.com/gddesign/about-us.html', 'gddesign9@gmail.com', '8586971866', 'Naval Kishore Suthar', '9845163136', 'Big Bazaar – 1 No,Bangalore Central ,E-Zone Bangalore - 2 C,APC (American PC) ,Café Terra ,Momoz Café ,Manipal Hospital ,BGS Hospital ,Umesh Baheti R Block – 404,Cosmos Mall , Rohit Duplex ,Sigma Mall ,Accenture ,Hotel Prince paradise ,Sharma Panjabi Resta urant ,Venkateshwara, Mini Adhikari,IMS Health,Satyanarayan,RMZ Galleria ,Mala,I-Gate,Alliance Group , Shyam Sunder,Suniti Bhimani ,Parmesh ,Apollo Cardle,RMZ Club House, KRISHNA MOHAN , SIJJO ,Vishal khator ,Manohar , Sunandita Sahu ,Guha,Birendra , Praveen Kumar ,Vinod ,', 'Bangalore'),
('Swastik Interiors', '# 12,  Matru Bhawan, Near Sunrise School, NTI Layout, Bhoopasandra, Sanjaynagar, RMV Extention 2nd Stage, Near Hebbal. Bangalore, karnataka-560094', 'All kind of Interior Designer', 'NA', 'NA', 'NA', 'http://www.swastikinteriors.com/contact_us', '  info@swastikinteriors.com', '9980071714', 'Som Prakash', '9980326058', 'NA', 'Bangalore');

-- --------------------------------------------------------

--
-- Table structure for table `import_modular`
--

CREATE TABLE IF NOT EXISTS `import_modular` (
  `Company_Name` varchar(17) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(109) CHARACTER SET utf8 DEFAULT NULL,
  `website` varchar(28) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(86) CHARACTER SET utf8 DEFAULT NULL,
  `contact_No` varchar(57) CHARACTER SET utf8 DEFAULT NULL,
  `Coverage_Area` varchar(9) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `import_modular`
--

INSERT INTO `import_modular` (`Company_Name`, `Address`, `website`, `email`, `contact_No`, `Coverage_Area`) VALUES
('Jackonblock', '#9, 1st Main, 1st Cross, LIC Colony,Jayanagar,3rd Block, Bangalore, Karnataka 560011', 'http://www.jackonblock.com/', 'care@jackonblock.com ', '91 (80) 49383333', 'Bangalore'),
('Bro4u.com', '#4, 1st H Main Road, Lal Bahadur Nagar, B Channasandra, Bengaluru, Karnataka 560043', 'https://bro4u.com/', 'reachus@bro4u.com', '91 80 42037143 / 080 4203 7143 / whatsapp: 91-7795556666', 'Bangalore'),
('Handyfix', '#426, 3rd Cross, 4th Block,1st Stage, HBR Layout, Kalyan Nagar Post,Bangalore – 560 043.', 'http://www.handyfix.in/', 'aravind@handyfix.in / complaint@handyfix.in / contactus@handyfix.in / info@handyfix.in', '8105685679, 8892342302', 'Bangalore'),
('Zepper', ' F-58, Diamond District, Old Airport Road,, Near Hotel Leela Palace, Indiranagar, Bengaluru, Karnataka 560008', 'http://www.zepper.in/', 'hello@zepper.in', '080-41429815', 'Bangalore'),
('Bestmaintain', 'NO-1343, 4TH FLOOR, SOUTH END, Bengaluru, Karnataka 560069', 'http://www.bestmaintain.com/', 'info@bestmaintain.com', '91-7676011011 /076760 11011 ', 'Bangalore'),
('Smartfix', 'No. 448,10th Cross, Uttarahalli Main, Bengaluru, Karnataka 560061', 'http://www.smartfix.in/', 'Contact@smartfix.in', '91 8105052575', 'Bangalore'),
('REPAIRMAN', '4th Cross Rd, Cox Town, Bengaluru, Karnataka 560005', 'http://www.repairman.in/', 'info@repairman.in', '076765 35433', 'Bangalore'),
('HomeTriangle', 'HAL 2nd Stage, Kodihalli, Bengaluru, Karnataka', 'https://hometriangle.com', 'contact@hometriangle.com', '+91 76 76 000100', 'Bangalore'),
('Any Services', 'Cox Town, Bengaluru, Karnataka', 'http://anyservices.in/', ' info@anyservices.in', '9845027027 / +91 9845076301 / 080 32492827 /080 41487342 ', 'Bangalore'),
('PRISM CONSULTANCY', '135/2, 1st Floor, 11th Cross Malleshwaram Bangalore, Karnataka 560003', 'www.prismconsultancy.net', 'prismbangalore@gmail.com / prismconsultant@yahoo.co.in', ' 080 - 2356 0141 / 2344 9432 / 2346 2842', 'Bangalore');

-- --------------------------------------------------------

--
-- Table structure for table `import_plumbing`
--

CREATE TABLE IF NOT EXISTS `import_plumbing` (
  `Name_of_company` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(108) CHARACTER SET utf8 DEFAULT NULL,
  `Name_of_the_Designer` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `Company_Website` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Contact_no` varchar(39) CHARACTER SET utf8 DEFAULT NULL,
  `Eamil_ID` varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  `Client_details` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `Coverage_Area` varchar(9) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `import_plumbing`
--

INSERT INTO `import_plumbing` (`Name_of_company`, `Address`, `Name_of_the_Designer`, `Company_Website`, `Contact_no`, `Eamil_ID`, `Client_details`, `Coverage_Area`) VALUES
('Housejoy', 'Sarvaloka Services On Call Pvt Ltd. Housejoy SM towers,#261, 3rd cross, Domlur, 2nd stage,Bengaluru - 560071', 'Jacob David', 'www.housejoy.in', '1860-123-4569 ', ' cs@housejoy.in services@housejoy.in', 'NA', 'Bangalore'),
('Sunshyn', 'No.14,15 & 16, Sri Adichunchanagiri Complex, Vijayanagar, Bangalore - 560040,', 'NA', 'http://sunshyneinternational.in/photo_gallery', '080-41535011,9379570042', 'support@sunshyneinternational.in', 'NA', 'Bangalore'),
('Mayur Constructions ', 'Market Rd, Basavanagudi, Bengaluru, Karnataka 560004', 'NA', 'http://www.mayurconstructions.com', '080-48413471', 'mayurconstructions1@gmail.com', 'NA', 'Bangalore'),
('Sri Vinayaka', 'No. 2, 2nd Floor, 15th Cross, 8th Main, Karlo Chambers, Malleswaram, Bangalore  ', 'NA', 'http://www.vinayaka-plumbing.com', '080-26606963,9901088885,9980799996  ', 'admin@vinayaka-plumbing.com ', 'NA', 'Bangalore'),
('AnyServices.in', ' 14, Webster Road, Cox Town, Bangalore - 560005', 'NA', 'http://www.anyservices.in/', ' 080 32492827 ,080 41487342,9845076301 ', 'info@anyservices.in', 'NA', 'Bangalore'),
('Bestmaintain ', 'NO-1343, 4TH FLOOR, SOUTH END, Bengaluru, Karnataka 560069', 'NA', 'http://www.bestmaintain.com/about-us', '8494930024', 'info@bestmaintain.com', 'NA', 'Bangalore'),
('Total Care Services ', 'Ashok Nagar, Bengaluru, Karnataka 560025', 'NA', 'http://www.totalcareservices.in', '080 25306448', 'info@totalcareservices.in', 'NA', 'Bangalore');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profs_cats`
--

CREATE TABLE IF NOT EXISTS `profs_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added_on` datetime NOT NULL,
  `is_verified` int(11) NOT NULL,
  `in_trash` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `profs_cats`
--

INSERT INTO `profs_cats` (`id`, `name`, `image`, `user_id`, `added_on`, `is_verified`, `in_trash`) VALUES
(1, 'Architects, Designers', 'Home-Extended_0001s_0001_Architects,-Designers-Image.png', 1, '2015-12-18 18:59:28', 1, 0),
(2, 'Interior Designers', 'Home-Extended_0002s_0002_Interior-Designers-Image.png', 1, '2015-11-25 18:53:57', 1, 0),
(3, 'Modular Kitchen Pros', 'Home-Extended_0003s_0001_Modular-Kitchen-Image.png', 1, '2015-11-25 20:10:34', 1, 0),
(4, 'Plumbing Providers', 'Home-Extended_0004s_0001_Plumber-Pros.png', 1, '2015-11-25 18:59:14', 1, 0),
(5, 'Electrical Pros', 'Home-Extended_0005s_0001_Electrician-Pros-Image.png', 1, '2015-11-25 18:53:09', 1, 0),
(6, 'Interior2', 'Home-Extended_0008s_0001_Vasthu-Designs.png', 1, '2015-11-25 18:55:11', 1, 1),
(7, 'Electrical Pros22', 'Home-Extended_0004s_0001_Plumber-Pros1.png', 1, '2015-11-25 19:37:06', 1, 1),
(8, 'test cat', 'Home-Extended_0002s_0002_Interior-Designers-Image1.png', 1, '2015-12-18 18:59:41', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_cats`
--

CREATE TABLE IF NOT EXISTS `project_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added_on` datetime NOT NULL,
  `is_verified` int(11) NOT NULL,
  `in_trash` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `project_cats`
--

INSERT INTO `project_cats` (`id`, `name`, `image`, `user_id`, `added_on`, `is_verified`, `in_trash`) VALUES
(1, 'Eco Friendly Designs', 'Home-Extended_0006s_0001_Eco-Friendly-Image.png', 1, '2015-11-25 19:58:35', 1, 0),
(2, 'Feng Shui Designs', 'Home-Extended_0007s_0001_Feng-Shui-Image.png', 1, '2015-11-25 19:58:56', 1, 0),
(3, 'Vasthu Designs', 'Home-Extended_0008s_0001_Vasthu-Designs.png', 1, '2015-11-25 19:59:11', 1, 0),
(4, 'Minimalist Designs', 'Home-Extended_0008s_0001_Vasthu-Designs1.png', 1, '2015-11-25 19:59:35', 1, 0),
(5, 'Trendy Designs', 'Home-Extended_0006s_0001_Eco-Friendly-Image1.png', 1, '2015-11-25 19:59:49', 1, 0),
(6, 'Trendy DesignsdddvvvddsdCX', 'Home-Extended_0007s_0001_Feng-Shui-Image1.png', 1, '2015-11-25 20:00:24', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` double NOT NULL,
  `review_title` varchar(255) NOT NULL,
  `review_msg` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `reviewed_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `rating`, `review_title`, `review_msg`, `user_id`, `reviewed_by`, `added_on`) VALUES
(1, 4.5, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft. I had earlier worked with designs of my office', 12, 20, '2016-02-28 10:45:55'),
(2, 4.5, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 12, 17, '2016-02-28 10:48:17'),
(3, 5, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 12, 18, '2016-02-28 10:48:38'),
(4, 4.5, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 12, 19, '2016-02-28 10:49:04'),
(5, 4, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 15, 19, '2016-02-28 10:53:25'),
(6, 4.5, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 15, 17, '2016-02-28 10:53:46'),
(7, 5, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 11, 19, '2016-02-28 11:08:14'),
(8, 5, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 11, 18, '2016-02-28 11:08:38'),
(9, 4, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 22, 20, '2016-02-28 11:08:38'),
(10, 3.5, 'Newline Interiors, a budget friendly excellence.', 'I would say, Newline is one the best budget friendly designers in our city. They are remarkable in cost savings, material selection and more. Sandra, is one of the brilliant engineers who made my home to the finest craft.', 22, 17, '2016-02-28 11:10:42'),
(11, 4, 'Test review', 'Test review message', 18, 22, '2016-02-28 23:55:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(10) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `dob` date NOT NULL,
  `gender` int(11) NOT NULL,
  `company` varchar(100) DEFAULT '',
  `phone` varchar(20) DEFAULT NULL,
  `photo` text NOT NULL,
  `website` varchar(255) NOT NULL,
  `prof_cat_id` int(11) NOT NULL,
  `lat` double NOT NULL,
  `long` double NOT NULL,
  `otp` text NOT NULL,
  `otp_time` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `address`, `city`, `state`, `zipcode`, `dob`, `gender`, `company`, `phone`, `photo`, `website`, `prof_cat_id`, `lat`, `long`, `otp`, `otp_time`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@buildr.com', '', NULL, NULL, NULL, 1268889823, 1464112835, 1, 'Admin', 'istrator', '', '', '', 0, '0000-00-00', 0, 'ADMIN', '0', '', '', 0, 0, 0, '0', '2016-02-28 23:23:01'),
(9, '::1', NULL, '$2y$08$t/oTLhnBji6//5g12V3tyuvr27xIlET4/Hj9TEtZAKfXoeT/jirMW', NULL, 'harismaslam@gmail.com', NULL, NULL, NULL, NULL, 1448726055, NULL, 1, 'Haris', 'Aslam', '#22, Varkala', 'Trivandrum', 'KL', 0, '2015-11-05', 1, NULL, '8129994413', '', '', 5, 8.753437, 76.699333, '0', '2016-02-28 23:23:01'),
(10, '::1', NULL, '$2y$08$aLyE4HHdUwJnu9O6wgQmo.Tb/UT1fUmd7em1DF9GeAef3PwZ57Fsi', NULL, 'vishnu.ios@gmail.com', NULL, NULL, NULL, NULL, 1448728367, 1448729941, 1, 'Vishnu', 'S', '#22, Kazhakoottam', 'Trivandrum', 'HR', 695582, '2015-11-11', 1, NULL, '8859741233', '', '', 3, 8.603426, 76.822929, '0', '2016-02-28 23:23:01'),
(11, '::1', NULL, '$2y$08$0XGZHogpIxjKECTW74XMe.cTT1UB9zMh6edvNGFMnLOVdsQtliUzO', NULL, 'nijesh11@gmail.com', NULL, NULL, NULL, NULL, 1448730265, NULL, 1, 'Nijesh', 'Buildr', '', 'Pathanamthitta', 'KL', 0, '0000-00-00', 1, NULL, '', '', '', 1, 9.273589, 76.783104, '0', '2016-02-28 23:23:01'),
(12, '::1', NULL, '$2y$08$wET5qNZZi2IR26xqQG66suV6hpZ0YEc3oqfO6D6SwiewtXIkePuzC', NULL, 'arjun.test@gmail.com', NULL, NULL, NULL, NULL, 1448771341, NULL, 1, 'Arjun', 'B', 'Alappuzha', '', 'KL', 0, '0000-00-00', 1, NULL, '', 'Service-Provider-Listing-Page_0003_Balayage-effect.png', '', 2, 9.471159, 76.3906, '0', '2016-02-28 23:23:01'),
(14, '::1', NULL, '$2y$08$JLs4FykEnLJU2S5/lYqTSuoE4MJGeexzA7qPnD0XxFnzaZWS4iLHC', NULL, 'berlin.test@wibits.com', NULL, NULL, NULL, NULL, 1448775352, NULL, 1, 'Berlin', 'R', '', 'Nagercoil', 'TN', 0, '0000-00-00', 1, NULL, '', '31.jpeg', '', 3, 8.29847, 77.23732, '0', '2016-02-28 23:23:01'),
(15, '::1', NULL, '$2y$08$kkDDTO7AxLMWxF.m5PLj5Oi5YQ/1nWKqU66qxyzFbsQlnMrpGpyn6', NULL, 'test22@gmail.com', NULL, NULL, NULL, NULL, 1448775483, NULL, 1, 'Rishana', 'R', '', '', 'KL', 0, '0000-00-00', 0, NULL, '', '21.jpeg', '', 2, 8.770403, 76.70208, '0', '2016-02-28 23:23:01'),
(16, '::1', NULL, '$2y$08$/q/CIW7n7A7t2sjmWZ70Ie5tqaneHbx//BD/.h.cKfR8X58jPGf96', NULL, 'kannan@ggg.ll', NULL, NULL, NULL, NULL, 1448775617, NULL, 1, 'Kannan', 'T', '', 'Kottayam', 'KL', 0, '2015-11-03', 0, 'Arjun Interior Designs', '', 'Capture1.PNG', 'http://ddd.com', 4, 0, 0, '0', '2016-02-28 23:23:01'),
(17, '::1', NULL, '$2y$08$QFjOFX4E37GRTRw1w6oObe0sz7unu6vVPEAOs2AcZyL9d324KLGiG', NULL, 'sathya.nn@gmail.com', NULL, NULL, NULL, NULL, 1448776623, NULL, 1, 'Sathya', 'N', '', '', 'JK', 0, '0000-00-00', 0, NULL, '', '22.jpeg', '', 0, 0, 0, '0', '2016-02-28 23:23:01'),
(18, '::1', NULL, '$2y$08$EMGqsbgjwzpoTFTbFVb4LeWCXd0yvs.n7pSlx4pOCYLbpQWVu56QS', NULL, 'paris.hilton@gg.ll', NULL, NULL, NULL, NULL, 1448778178, NULL, 1, 'Paris', 'Hiton', '', '', 'AP', 0, '0000-00-00', 0, NULL, '', '221.jpeg', '', 0, 0, 0, '0', '2016-02-28 23:23:01'),
(19, '::1', NULL, '$2y$08$WLvaVAUXmmA4X2gmxcNTt.YhtbRbW4ZJSfi7S0ECM0l7CgKTYVnHm', NULL, 'mail2@anjloe.com', NULL, NULL, NULL, NULL, 1448778456, NULL, 1, 'Angelina', 'Jolie', '', '', 'MH', 555556, '0000-00-00', 1, NULL, '', 'images.jpg', '', 0, 0, 0, '0', '2016-02-28 23:23:01'),
(20, '::1', NULL, '$2y$08$0dn8zX4JEPXF6wJiQnrHdOHq650aAu5lllUe2I.nARjPQR6ot3I32', NULL, 'favaz.s@aol.com', NULL, NULL, NULL, NULL, 1450507432, NULL, 1, 'Favaz', 'Shah', '', '', 'KL', 0, '0000-00-00', 1, NULL, '', '', '', 0, 8.76734, 76.6876, '0', '2016-02-28 23:23:01'),
(21, '::1', NULL, '$2y$08$xBovAui5rriAxy/TMgZvyehzU6iSnE7HZgsrAWmyY8zf8ZjaAqnAi', NULL, 'rooney.test@gmail.com', NULL, NULL, NULL, NULL, 1450512529, NULL, 1, 'Wayne', 'Rooney', '', 'Trivandrum', 'KL', 0, '0000-00-00', 1, NULL, '', '', '', 3, 8.483918, 76.988411, '0', '2016-02-28 23:23:01'),
(22, '::1', NULL, '$2y$08$ya2ILQBMeUttcK0J6iomQu7T/swTqxXvk75S/SAXvRJaU4e1x8g.O', NULL, 'vikram.singh@buildr.in', NULL, NULL, NULL, NULL, 1456637252, NULL, 1, 'Vikram', 'Singh', 'Chinnakada', 'Kollam', 'KL', 0, '0000-00-00', 1, NULL, '', '', '', 1, 8.737973, 76.530418, '0', '2016-02-28 23:23:01'),
(23, '::1', NULL, '$2y$08$v.iUc0oG3jd.SjZ6j9LcJ.joeQTHpC9ECdJkQh02gSl2laerHMX3q', NULL, 'ashish.nehra@buildr.in', NULL, NULL, NULL, NULL, 1456637325, NULL, 1, 'Ashish', 'Nehra', 'Kottiyam', 'Kollam', 'KL', 0, '0000-00-00', 1, NULL, '', 'profile1.png', '', 5, 8.29847, 76.699333, '0', '2016-02-28 23:23:01'),
(24, '::1', NULL, '$2y$08$kD2PyDztAT9rZzfQD8RQHu/pNnuUhcHuLUpYWFwbPokmjJUByOq76', NULL, 'michael.carrick@buildr.in', NULL, NULL, NULL, NULL, 1456637548, NULL, 1, 'Michael', 'Carrick', 'Attingal', 'Trivandrum', 'KL', 0, '0000-00-00', 1, NULL, '', '', '', 4, 8.603426, 76.70208, '0', '2016-02-28 23:23:01'),
(32, '::1', NULL, '$2y$08$ar1KciWbH4hqm/MYWB5RrOw9ipU7O9z1jdqhirKRBQPdy9AFSaof.', NULL, '', NULL, NULL, NULL, NULL, 1456681454, NULL, 1, 'Ben', NULL, '', '', '', 0, '0000-00-00', 0, NULL, '7025650567', '', '', 0, 0, 0, '2013', '1456682872'),
(33, '::1', NULL, '$2y$08$XPl7FwGWI7/rG7IpP0kVH.cnEfyVLtuft5J6Wj/Xdp87mWG19/77y', NULL, '', NULL, NULL, NULL, NULL, 1456682691, NULL, 1, NULL, NULL, '', '', '', 0, '0000-00-00', 0, NULL, '7025560657', '', '', 0, 0, 0, '7475', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(8, 9, 2),
(9, 10, 2),
(10, 11, 2),
(11, 12, 2),
(13, 14, 2),
(14, 15, 2),
(15, 16, 2),
(16, 17, 2),
(17, 18, 2),
(18, 19, 2),
(19, 20, 2),
(20, 21, 2),
(21, 22, 2),
(22, 23, 2),
(23, 24, 2),
(31, 32, 2),
(32, 33, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_privileges`
--

CREATE TABLE IF NOT EXISTS `user_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `privilege` varchar(255) NOT NULL,
  `admin` int(11) NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_privileges`
--

INSERT INTO `user_privileges` (`id`, `privilege`, `admin`, `members`) VALUES
(1, 'add_profs', 1, 0),
(2, 'view_profs', 1, 0),
(3, 'add_project', 1, 0),
(4, 'view_project', 1, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
