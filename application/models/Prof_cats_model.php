<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Prof_cats_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    public function view_cats() {
        $this->db->where('in_trash', 0);
        $this->db->order_by('id', 'asc');
        $query = $this->db->get('profs_cats');
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }
    
    public function add_category($data){
        $this->db->insert('profs_cats', $data);
        return $this->db->insert_id();
    }

    public function get_category($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('profs_cats');
        return ($query->num_rows() != 0) ? $query->row() : FALSE;
    }

    public function upd_category($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('profs_cats', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function delete_category($id) {
        $this->db->where('id', $id);
        $this->db->update('profs_cats', array('in_trash' => 1));
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
}