<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Idea_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function view_idea() {
//        $this->db->where('in_trash', 0);
        $this->db->select('*,pc.name cat_name, ida.id iid');
        $this->db->from('idea ida');
        $this->db->join('project_cats pc', 'pc.id=ida.cat_id');
        $this->db->order_by('ida.id', 'asc');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }
    
    public function add_idea($data){
        $this->db->insert('idea', $data);
        return $this->db->insert_id();
    }
    
    public function update_idea($data, $id){
        $this->db->where('id',$id);
        $this->db->update('idea', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
    
    public function get_idea($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('idea');
        return ($query->num_rows() != 0) ? $query->row() : FALSE;
    }

}
