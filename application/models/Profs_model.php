<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profs_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_profs($cat_id = NULL) {
        $this->db->select('u.*, u.id uid, pf.id cat_id, pf.name cat_name, avg(rating) avg_rat, count(rw.id) tot_rw');
        $this->db->from('users u');
        $this->db->join('profs_cats pf', 'pf.id=u.prof_cat_id');
        $this->db->join('reviews rw', 'u.id=rw.user_id', 'LEFT');
        if ($cat_id == NULL) {
            $this->db->where('u.prof_cat_id!=', 0);
        } else {
            $this->db->where('u.prof_cat_id', $cat_id);
        }
        $this->db->group_by('u.id');
        $this->db->order_by('avg_rat', 'DESC');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }

    public function get_prof($id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row() : NULL;
    }

    public function get_prof_detail($prof_id) {
        $this->db->select('u.*, u.id uid, pf.id cat_id, pf.name cat_name, avg(rating) avg_rat, count(rw.id) tot_rw');
        $this->db->from('users u');
        $this->db->join('profs_cats pf', 'pf.id=u.prof_cat_id');
        $this->db->join('reviews rw', 'u.id=rw.user_id', 'LEFT');
        $this->db->where('u.id', $prof_id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row() : NULL;
    }

    public function delete_user($id) {
        $this->db->delete('users', array('id' => $id));
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get_top_profs($cat_id) {
        $this->db->select('u.*, u.id uid, pf.id cat_id, pf.name cat_name, avg(rating) avg_rat, count(rw.id) tot_rw');
        $this->db->from('users u');
        $this->db->join('profs_cats pf', 'pf.id=u.prof_cat_id');
        $this->db->join('reviews rw', 'u.id=rw.user_id', 'LEFT');
        $this->db->where('u.prof_cat_id', $cat_id);
        $this->db->group_by('rw.user_id');
        $this->db->order_by('avg_rat', 'DESC');
        $this->db->limit(1, 0);

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row() : NULL;
    }

}
