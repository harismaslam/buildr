<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_users() {
        $this->db->select('*, u.id uid');
        $this->db->from('users u');
        $this->db->join('users_groups ug', 'ug.user_id=u.id');
        $this->db->where('u.prof_cat_id', 0);
        $this->db->where('ug.group_id!=', 1);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }

    public function get_owners() {
        $this->db->select('*, u.id uid');
        $this->db->from('users u');
        $this->db->join('users_groups ug', 'ug.user_id=u.id');
        $this->db->where('ug.group_id!=', 1);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }

    public function get_designers() {
        $this->db->select('id');
        $this->db->from('profs_cats');
        $this->db->where('in_trash', 0);
        $this->db->like('name', 'design');
        $this->db->get();
        $sub_query = $this->db->last_query();

        $this->db->select('*, u.id uid');
        $this->db->from('users u');
        $this->db->join('users_groups ug', 'ug.user_id=u.id');
//        $this->db->where_in('u.prof_cat_id',array(1,5));
        $this->db->where("u.prof_cat_id in ($sub_query)", NULL, FALSE);
        $this->db->where('ug.group_id!=', 1);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }

    public function get_user($id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row() : NULL;
    }

    public function delete_user($id) {
        $this->db->delete('users', array('id' => $id));
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get_user_by_phone($phone) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('phone', $phone);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

}
