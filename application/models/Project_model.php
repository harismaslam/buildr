<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function add_project($data) {
        $this->db->insert('apartment', $data);
        return $this->db->insert_id();
    }

    public function update_project($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('apartment', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
    
    public function del_project($proj_id) {
        $this->db->where('id', $proj_id);
        $this->db->delete('apartment');
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get_apts($prof_id = NULL, $cat_id = NULL) {
        $this->db->where('in_trash', 0);
        if ($cat_id != NULL) {
            $this->db->where('proj_cat_id', $cat_id);
        }
        if (!empty($prof_id)) {
            $this->db->where('apt_designer_id', $prof_id);
        }
        $this->db->order_by('id', 'asc');
        $query = $this->db->get('apartment');
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }

    public function get_apt($id) {
        $this->db->select('*');
        $this->db->from('apartment');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row() : NULL;
    }

    public function add_project_gal_img($data) {
        return $this->db->insert_batch('apartment_images', $data);
    }

    public function get_project_gal($id) {
        $this->db->where('apt_id', $id);
        $this->db->order_by('id', 'asc');
        $query = $this->db->get('apartment_images');
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }

    public function get_project_gal_item($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('apartment_images');
        return ($query->num_rows() > 0) ? $query->row() : NULL;
    }

    public function del_gal_img($id) {
        $this->db->delete('apartment_images', array('id' => $id));
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get_profs_project_img($prof_id, $limit) {
        $this->db->select('*');
        $this->db->from('apartment a');
        $this->db->join('apartment_images ai', 'ai.apt_id = a.id', 'full');
        $this->db->where('apt_designer_id', $prof_id);
        if (!empty($limit)) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }

}
