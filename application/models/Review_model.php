<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Review_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function add_review($data){
        $this->db->insert('reviews', $data);
        return $this->db->insert_id();
    }
    
    public function update_review($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('reviews', $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
    
    public function get_user_top_reviews($user_id){
        $this->db->select('*');
        $this->db->from('reviews');
        $this->db->where('user_id',$user_id);
        $this->db->order_by('rating','DESC');
        $this->db->order_by('added_on','DESC');
        $this->db->limit(1,0);
        
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row() : NULL;
    }
    
    public function get_user_avg_rating($user_id){
        $this->db->select('avg(rating) avg_rating');
        $this->db->from('reviews');
        $this->db->group_by('user_id');
        $this->db->where('user_id',$user_id);
        
        $query = $this->db->get();
//        echo $this->db->last_query();
        return ($query->num_rows() > 0) ? $query->row() : NULL;
    }
    
    public function get_user_reviews($user_id){
//        echo $user_id;
        $this->db->select('*');
        $this->db->from('reviews');
        $this->db->where('user_id',$user_id);
        $this->db->order_by('rating','DESC');
        
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : NULL;
    }
    
    public function get_user_prof_review($user_id, $prof_id){
        $this->db->where('reviewed_by', $user_id);
        $this->db->where('user_id', $prof_id);
        $query = $this->db->get('reviews');
        return ($query->num_rows() != 0) ? $query->row() : FALSE;
    }
}