<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project_img_qn_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_qns($img_id, $ans = TRUE) {
        $this->db->select('*');
        $this->db->from('apartment_images_questions');
        $this->db->where('apt_img_id', $img_id);
        if ($ans == TRUE) {
            $this->db->where('answer!=', '');
        }
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : NULL;
    }

}
