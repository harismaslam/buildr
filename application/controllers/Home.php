<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $this->load->model('Prof_cats_model');
        $this->data['prof_cats'] = $this->Prof_cats_model->view_cats();
        $cat_id = ($this->input->post('cat') != NULL) ? $this->input->post('cat') : '';
        $profs = array();
        if (!empty($cat_id)) {
            $this->load->model('Profs_model');
            $profs = $this->Profs_model->get_profs($cat_id);
        }
        if (!empty($profs)) {
            $this->load->model('Review_model');
            $this->load->model('Project_model');
            foreach ($profs as $val) {
                $prof_user_img = $val->photo;
                if (!empty($prof_user_img)) {
                    $val->photo = base_url() . 'uploads/user_profile/' . $prof_user_img;
                } else {
                    $val->photo = base_url() . 'uploads/user_profile/user-avatar.png';
                }
                unset(
                        $val->prof_cat_id, $val->otp, $val->otp_time, $val->username, $val->id, $val->password, $val->ip_address, $val->salt, $val->activation_code, $val->forgotten_password_code, $val->forgotten_password_time, $val->remember_code, $val->created_on, $val->last_login, $val->active, $val->image, $val->user_id, $val->added_on, $val->is_verified, $val->in_trash, $val->added_on
                );
                //@param user id
                $reviews = $this->Review_model->get_user_reviews($val->uid);
                $review_arr = array();
                if (!empty($reviews)) {
                    foreach ($reviews as $review) {
                        $reviewer = $this->ion_auth->user($review->reviewed_by)->row();
                        $review->reviewer_name = $reviewer->first_name . ' ' . $reviewer->last_name;
                        unset(
                                $review->user_id, $review->added_on, $review->user_id
                        );
                        $reviewer_img = $reviewer->photo;
                        if (!empty($reviewer_img)) {
                            $review->reviewer_photo = base_url() . 'uploads/user_profile/' . $reviewer_img;
                        } else {
                            $review->reviewer_photo = base_url() . 'uploads/user_profile/user-avatar.png';
                        }
                        $review_arr[] = $review;
                    }
                }
                $project_imgs = $this->Project_model->get_profs_project_img($val->uid, $limit = 2);
                $val->proj_imgs = $project_imgs;
                $val->review = empty($review_arr) ? array() : $review_arr;
            }
        }
        $this->data['profs'] = $profs;
        $this->load->view('home', $this->data);
    }

}
