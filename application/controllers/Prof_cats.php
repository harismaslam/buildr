<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prof_cats extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'dynamicload'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->data['bodyclass'] = 'signed-in';

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
    }

    public function add() {
        $this->load->view('prof_cats/add');
    }

    public function edit($id) {
        $this->load->model('Prof_cats_model');
        if ($this->input->post('edit') != NULL) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            if ($this->form_validation->run() == true) {
                $data['name'] = ($this->input->post('name') != NULL) ? $this->input->post('name') : '';
                $data['added_on'] = date('Y-m-d H:i:s');
                if ($this->ion_auth->is_admin()) {
                    $data['is_verified'] = 1;
                }

                if (!empty($_FILES['userfile']['name'])) {
                    $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . '/uploads/cat_img/';
                    $config['upload_path'] = $upload_path;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '6144';

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('userfile')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('message', $error['error']);
                        $this->session->set_flashdata('msg_type', 'alert-danger');
                        redirect("prof_cats/edit/$id");
                    } else {
                        $img_data = array('upload_data' => $this->upload->data());
                        $data['image'] = $img_data['upload_data']['file_name'];

                        $prev_img = $this->Prof_cats_model->get_category($id)->image;
                        if (!empty($sett_val)) {
                            $photo = $sett_val->photo;
                        }
                        $prev_img_path = $upload_path . $prev_img;

                        if (!empty($prev_img) && file_exists($prev_img_path)) {
                            unlink($prev_img_path);
                        }
                    }
                }
                $data['user_id'] = $this->ion_auth->get_user_id();

                $updated = $this->Prof_cats_model->upd_category($id, $data);
                if (isset($updated)) {
                    $this->session->set_flashdata('msg_type', 'alert-success');
                    $this->session->set_flashdata('message', 'Category updated');
                }
            }
        }
        $this->data['id'] = $id;
        $this->data['cat'] = $this->Prof_cats_model->get_category($id);
        $this->load->view('prof_cats/edit', $this->data);
    }

    public function save() {
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() == true) {
            $data['name'] = ($this->input->post('name') != NULL) ? $this->input->post('name') : '';
            $data['added_on'] = date('Y-m-d H:i:s');
            if ($this->ion_auth->is_admin()) {
                $data['is_verified'] = 1;
            }

            $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . '/uploads/cat_img/';
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '6144';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('userfile')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('message', $error['error']);
                $this->session->set_flashdata('msg_type', 'alert-danger');
                redirect('prof_cats/add');
            }
            $img_data = array('upload_data' => $this->upload->data());
            $data['image'] = $img_data['upload_data']['file_name'];
            $data['user_id'] = $this->ion_auth->get_user_id();

            $this->load->model('Prof_cats_model');
            $inserted = $this->Prof_cats_model->add_category($data);
            if ($inserted) {
                $this->session->set_flashdata('msg_type', 'alert-success');
                $this->session->set_flashdata('message', 'Category added');
                redirect('prof_cats');
            }
        } else {
            $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->session->set_flashdata('message', $message);
            $this->session->set_flashdata('msg_type', 'alert-danger');
            redirect('prof_cats/add');
        }
    }

    public function index() {
        $this->load->model('Prof_cats_model');
        $this->data['cats'] = $this->Prof_cats_model->view_cats();
        $this->load->view('prof_cats/list', $this->data);
    }

    public function delete($id) {
        $this->load->model('Prof_cats_model');
        $deleted = $this->Prof_cats_model->delete_category($id);
        if (isset($deleted)) {
            $this->session->set_flashdata('msg_type', 'alert-success');
            $this->session->set_flashdata('message', 'Category deleted');
        }
        redirect('prof_cats');
    }

}
