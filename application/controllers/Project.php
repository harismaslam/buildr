<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));

        $this->form_validation->set_error_delimiters('', '');

        $this->data['bodyclass'] = 'signed-in';

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
    }

    public function view($userid_enc) {
        if (user_has_privilege('view_project')) {
            $this->data['prof_id'] = $user_id = decode_url($userid_enc);
            $this->load->model('Project_model');
            $this->data['aptmts'] = $aptts = $this->Project_model->get_apts($user_id);
            $this->load->view('project/list', $this->data);
        }
    }

    public function add($userid_enc) {
        if (user_has_privilege('view_project')) {
            $user_id = decode_url($userid_enc);
            $this->load->model('User_model');
            $this->data['owners'] = $this->User_model->get_owners();
            $this->data['designers'] = $this->User_model->get_designers();
            $this->data['proj_cats'] = $this->project_cats();

            $this->data['prof_id'] = $user_id;

            $this->load->view('project/add', $this->data);
        }
    }

    protected function project_cats() {
        $this->load->model('Project_cats_model');
        $proj_cats = $this->Project_cats_model->view_cats();
        $cats_arr = array();
        foreach ($proj_cats as $val) {
            $cats_arr[$val->id] = $val->name;
        }
        return $cats_arr;
    }

    public function save($userid_enc) {
        $designer_id = decode_url($userid_enc);
        $this->form_validation->set_rules('apt_name', 'Project Name', 'required');
//        $this->form_validation->set_rules('apt_designer', 'Project Designer', 'required');
//        $this->form_validation->set_rules('apt_owner', 'Project Owner', 'required');

        if ($this->form_validation->run() == true) {
            $apt_data['apt_name'] = ($this->input->post('apt_name') != NULL) ? $this->input->post('apt_name') : '';
            $apt_data['apt_addr'] = ($this->input->post('apt_addr') != NULL) ? $this->input->post('apt_addr') : '';
            $apt_data['apt_lat'] = ($this->input->post('apt_lat') != NULL) ? $this->input->post('apt_lat') : '';
            $apt_data['apt_long'] = ($this->input->post('apt_long') != NULL) ? $this->input->post('apt_long') : '';
            $apt_data['apt_designer'] = ($this->input->post('apt_designer') != NULL) ? $this->input->post('apt_designer') : '';
            $apt_data['apt_designer_id'] = $designer_id;
            $apt_data['apt_owner'] = ($this->input->post('apt_owner') != NULL) ? $this->input->post('apt_owner') : '';
            $apt_data['apt_owner_id'] = ($this->input->post('apt_owner_id') != NULL) ? $this->input->post('apt_owner_id') : '';
            $apt_data['proj_cat_id'] = ($this->input->post('proj_cat_id') != NULL) ? $this->input->post('proj_cat_id') : '';

            $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . '/uploads/aprt_img/';
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2144';

            $this->load->library('image_lib');

            if (!empty($_FILES['userfile']['name'])) {
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('userfile')) {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message', $error['error']);
                    $this->session->set_flashdata('msg_type', 'alert-danger');
                    redirect('project/add');
                }
                $img_data = array('upload_data' => $this->upload->data());
                $filename = $apt_data['apt_img'] = $img_data['upload_data']['file_name'];

                $thumb_configs = array();
                $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/195x125/$filename", 'width' => 195, 'height' => 9999, 'maintain_ratio' => TRUE);
                $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/224x155/$filename", 'width' => 224, 'height' => 155, 'maintain_ratio' => TRUE);
                // Loop through the array to create thumbs
//                $this->load->library('image_lib');
                foreach ($thumb_configs as $thumb_config) {
                    $this->image_lib->thumb($thumb_config, $upload_path);
                }
            }

            $this->load->model('Project_model');
            $apt_id = $this->Project_model->add_project($apt_data);


            $gal_titles = $this->input->post('gal_title');
            if (!empty($gal_titles[0])) {
                $config['max_size'] = '6144';
                $this->load->library('upload');
                $this->upload->initialize($config);
                if ($this->upload->do_multi_upload("gal_file")) {
                    $img_data = $this->upload->get_multi_upload_data();
                    $gal_imgs = array();
                    foreach ($img_data as $img) {
                        $gal_imgs[] = $img['file_name'];
                    }
                } else {
                    $message = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $message);
                }

                for ($i = 0; $i < count($gal_titles); $i++) {
                    $gal_data[] = array(
                        "apt_gall_img_title" => $gal_titles[$i],
                        "apt_gall_img" => $gal_imgs[$i],
                        "apt_id" => $apt_id
                    );
                    $filename = $gal_imgs[$i];
                    $thumb_configs = array();
                    $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/195x125/$filename", 'width' => 195, 'height' => 9999, 'maintain_ratio' => TRUE);
                    $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/224x155/$filename", 'width' => 224, 'height' => 155, 'maintain_ratio' => TRUE);

                    foreach ($thumb_configs as $thumb_config) {
                        $this->image_lib->thumb($thumb_config, $upload_path);
                    }
                }

                $this->Project_model->add_project_gal_img($gal_data);
            }

            if ($apt_id) {
                $this->session->set_flashdata('msg_type', 'alert-success');
                $this->session->set_flashdata('message', 'Project added');
                redirect('project/view/' . $userid_enc);
            }
        } else {
            $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->session->set_flashdata('message', $message);
            $this->session->set_flashdata('msg_type', 'alert-danger');
            redirect('project/add');
        }
    }

    public function edit($id, $prof_id_enc) {
        $prof_id = decode_url($prof_id_enc);
        $this->load->model('Project_model');
        if ($this->input->post('edit') != NULL) {
            $this->form_validation->set_rules('apt_name', 'Project Name', 'required');
//            $this->form_validation->set_rules('apt_designer', 'Project Designer', 'required');
//            $this->form_validation->set_rules('apt_owner', 'Project Owner', 'required');
            if ($this->form_validation->run() == true) {
                $apt_img = $this->Project_model->get_apt($id)->apt_img;
                $apt_data['apt_name'] = ($this->input->post('apt_name') != NULL) ? $this->input->post('apt_name') : '';
                $apt_data['apt_addr'] = ($this->input->post('apt_addr') != NULL) ? $this->input->post('apt_addr') : '';
//                $apt_data['apt_lat'] = ($this->input->post('apt_lat') != NULL) ? $this->input->post('apt_lat') : '';
//                $apt_data['apt_long'] = ($this->input->post('apt_long') != NULL) ? $this->input->post('apt_long') : '';
//                $apt_data['apt_designer'] = ($this->input->post('apt_designer') != NULL) ? $this->input->post('apt_designer') : '';
//                $apt_data['apt_designer_id'] = ($this->input->post('apt_designer_id') != NULL) ? $this->input->post('apt_designer_id') : '';
//                $apt_data['apt_owner'] = ($this->input->post('apt_owner') != NULL) ? $this->input->post('apt_owner') : '';
//                $apt_data['apt_owner_id'] = ($this->input->post('apt_owner_id') != NULL) ? $this->input->post('apt_owner_id') : '';
                $apt_data['proj_cat_id'] = ($this->input->post('proj_cat_id') != NULL) ? $this->input->post('proj_cat_id') : '';

                $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . '/uploads/aprt_img/';
                $config['upload_path'] = $upload_path;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2144';

                $this->load->library('image_lib');

                if (!empty($_FILES['userfile']['name'])) {
                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('userfile')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('message', $error['error']);
                        $this->session->set_flashdata('msg_type', 'alert-danger');
                        redirect("project/edit/$id");
                    }
                    $img_data = array('upload_data' => $this->upload->data());
                    $filename = $apt_data['apt_img'] = $img_data['upload_data']['file_name'];

                    $thumb_configs = array();
                    $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/195x125/$filename", 'width' => 195, 'height' => 9999, 'maintain_ratio' => TRUE);
                    $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/224x155/$filename", 'width' => 224, 'height' => 155, 'maintain_ratio' => TRUE);
                    // Loop through the array to create thumbs
//                $this->load->library('image_lib');
                    foreach ($thumb_configs as $thumb_config) {
                        $this->image_lib->thumb($thumb_config, $upload_path);
                    }

                    if (file_exists($upload_path . $apt_img)) {
                        unlink($upload_path . $apt_img);
                    }

                    if (!empty($apt_img) && file_exists($upload_path . "thumbs/195x125/" . $apt_img)) {
                        unlink($upload_path . "thumbs/195x125/" . $apt_img);
                    }
                    if (!empty($apt_img) && file_exists($upload_path . "thumbs/224x155/" . $apt_img)) {
                        unlink($upload_path . "thumbs/224x155/" . $apt_img);
                    }
                }

                $this->load->model('Project_model');
                $apt_id = $this->Project_model->update_project($apt_data, $id);


                $gal_titles = $this->input->post('gal_title');
                if (!empty($gal_titles[0])) {
                    $config['max_size'] = '6144';
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    if ($this->upload->do_multi_upload("gal_file")) {
                        $img_data = $this->upload->get_multi_upload_data();
                        $gal_imgs = array();
                        foreach ($img_data as $img) {
                            $gal_imgs[] = $img['file_name'];
                        }
                    } else {
                        $message = $this->upload->display_errors();
                        $this->session->set_flashdata('message', $message);
                    }
                    for ($i = 0; $i < count($gal_titles); $i++) {
                        $gal_data[] = array(
                            "apt_gall_img_title" => $gal_titles[$i],
                            "apt_gall_img" => $gal_imgs[$i],
                            "apt_id" => $id
                        );

                        $filename = $gal_imgs[$i];
                        $thumb_configs = array();
                        $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/195x125/$filename", 'width' => 195, 'height' => 9999, 'maintain_ratio' => TRUE);
                        $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/224x155/$filename", 'width' => 224, 'height' => 155, 'maintain_ratio' => TRUE);

                        foreach ($thumb_configs as $thumb_config) {
                            $this->image_lib->thumb($thumb_config, $upload_path);
                        }
                    }
                    $this->Project_model->add_project_gal_img($gal_data);
                }

                if ($apt_id) {
                    $this->session->set_flashdata('msg_type', 'alert-success');
                    $this->session->set_flashdata('message', 'Project updated');
                    redirect("project/edit/$id/$prof_id");
                }
            }
        }
        $this->data['apt'] = $this->Project_model->get_apt($id);
        $this->data['gal'] = $this->Project_model->get_project_gal($id);
        $this->data['proj_cats'] = $this->project_cats();
        $this->data['id'] = $id;
        $this->load->view('project/edit', $this->data);
    }

    public function del_gal_img($id) {
        $apt_id = $this->uri->segment(4);
        $prof_id = $this->uri->segment(5);
        $this->load->model('Project_model');
        $gal_item = $this->Project_model->get_project_gal_item($id);
        $deleted = $this->Project_model->del_gal_img($id);

        $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/aprt_img/";

        $img_val = $gal_item->apt_gall_img;
        if ($deleted == TRUE) {
            if (file_exists($upload_path . $img_val)) {
                unlink($upload_path . $img_val);
            }
            $this->session->set_flashdata('msg_type', 'alert-success');
            $this->session->set_flashdata('message', 'Image deleted');
        }
        redirect("project/edit/$apt_id/$prof_id");
    }

    public function delete($proj_id, $prof_id) {
        $this->load->model('Project_model');
        $deleted = $this->Project_model->del_project($proj_id);
        if ($deleted) {
            $this->session->set_flashdata('msg_type', 'alert-success');
            $this->session->set_flashdata('message', 'Project deleted');
        }
        redirect('project/view/' . $prof_id);
    }

}
