<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index() {
        $this->load->model('Prof_cats_model');
        $this->data['prof_cats'] = $this->Prof_cats_model->view_cats();
        $this->load->view('welcome_message', $this->data);
    }

}
