<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'dynamicload'));

        $this->form_validation->set_error_delimiters('', '');

        $this->data['bodyclass'] = 'signed-in';

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
    }

    public function index() {
        if (user_has_privilege('view_profs')) {
            $this->load->model('User_model');
            $this->data['users'] = $this->User_model->get_users();
            $this->load->view('user/index', $this->data);
        }
    }

    public function add() {
        if (user_has_privilege('add_profs')) {
            $this->load->view('user/add', $this->data);
        }
    }

    public function save() {
        if (user_has_privilege('add_profs')) {
            $tables = $this->config->item('tables', 'ion_auth');
            $this->form_validation->set_rules('first_name', 'Frist Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == true) {
                $username = $this->input->post('first_name') . '_' . $this->input->post('last_name');
                $password = $this->input->post('password');
                $email = strtolower($this->input->post('email'));

                $input_date = $this->input->post('dob');
                $dob = '';
                if (!empty($input_date)) {
                    list($d, $m, $y) = explode('/', $input_date);
                    $dob = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y));
                }

                $photo = '';
                if (!empty($_FILES['userfile']['name'])) {
                    $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . '/uploads/user_profile/';
                    $config['upload_path'] = $upload_path;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '2048';

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('userfile')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('message', $error['error']);
                        $this->session->set_flashdata('msg_type', 'alert-danger');
                        redirect('user/add');
                    }
                    $img_data = array('upload_data' => $this->upload->data());
                    $photo = $img_data['upload_data']['file_name'];
                }

                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'zipcode' => $this->input->post('zipcode'),
                    'dob' => $dob,
                    'gender' => $this->input->post('gender'),
                    'phone' => $this->input->post('phone'),
//                    lat and long
                    'lat' => $this->input->post('lat'),
                    'long' => $this->input->post('long'),
                    'photo' => isset($photo)?$photo:'',
                );
                $group_id = array(2);
                $success = $this->ion_auth->register($username, $password, $email, $additional_data, $group_id);
                if ($success) {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    $this->session->set_flashdata('msg_type', 'alert-success');
                    redirect('user');
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    $this->session->set_flashdata('msg_type', 'alert-danger');
                    redirect('user/add');
                }
            } else {
                $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                $this->session->set_flashdata('message', $message);
                $this->session->set_flashdata('msg_type', 'alert-danger');
            }
        }
    }

    public function edit($id) {
        if (user_has_privilege('add_profs')) {
            if ($this->input->post('edit') != NULL) {
                $this->form_validation->set_rules('first_name', 'Frist Name', 'required');
                $this->form_validation->set_rules('last_name', 'Last Name', 'required');

                $tables = $this->config->item('tables', 'ion_auth');
                $curr_email = $this->ion_auth->user($id)->row()->email;
                if (($this->input->post('email') != NULL) && $curr_email != $this->input->post('email')) {
                    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
                    $this->form_validation->set_message('is_unique', '%s is already registered.');
                }
                if ($this->input->post('password') != NULL) {
                    $this->form_validation->set_rules('password', 'Password', 'required');
                }
                if ($this->form_validation->run() == true) {
                    $data['first_name'] = $this->input->post('first_name');
                    $data['last_name'] = $this->input->post('last_name');
                    $data['email'] = $this->input->post('email');
                    if ($this->input->post('password') != NULL) {
                        $data['password'] = $this->input->post('password');
                    }
                    $data['address'] = $this->input->post('address');
                    $data['city'] = $this->input->post('city');
                    $data['state'] = $this->input->post('state');
                    $data['zipcode'] = $this->input->post('zipcode');
                    $input_date = $this->input->post('dob');
                    if (!empty($input_date)) {
                        list($d, $m, $y) = explode('/', $input_date);
                        $data['dob'] = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y));
                    }

                    if (!empty($_FILES['userfile']['name'])) {
                        $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . '/uploads/user_profile/';
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '2048';

                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('userfile')) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message', $error['error']);
                            $this->session->set_flashdata('msg_type', 'alert-danger');
                            redirect("user/edit/$id");
                        }
                        $img_data = array('upload_data' => $this->upload->data());
                        $data['photo'] = $img_data['upload_data']['file_name'];

                        $prev_img = $this->ion_auth->user($id)->row()->photo;
                        $prev_img_path = $upload_path . $prev_img;

                        if (!empty($prev_img) && file_exists($prev_img_path)) {
                            unlink($prev_img_path);
                        }
                    }

                    $data['gender'] = $this->input->post('gender');
                    $data['phone'] = $this->input->post('phone');
                    $data['lat'] = $this->input->post('lat');
                    $data['long'] = $this->input->post('long');
                    $updated = $this->ion_auth->update($id, $data);

                    if ($updated) {
                        $this->session->set_flashdata('msg_type', 'alert-success');
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                    }
                } else {
                    $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                    $this->session->set_flashdata('message', $message);
                    $this->session->set_flashdata('msg_type', 'alert-danger');
                }
            }

            $this->load->model('User_model');
            $this->data['user'] = $this->User_model->get_user($id);
            $this->data['id'] = $id;
            $this->load->view('user/edit', $this->data);
        }
    }

    public function delete($id) {
        if (user_has_privilege('add_profs')) {
            $this->load->model('User_model');
            $this->User_model->delete_user($id);
            $this->session->set_flashdata('msg_type', 'alert-success');
            $this->session->set_flashdata('message', 'Supplier deleted');
            redirect("user");
        }
        redirect("user");
    }

}
