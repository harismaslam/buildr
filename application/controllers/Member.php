<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member extends CI_Controller {
    public function add_review() {
        $mem_user_id = $this->session->userdata('mem_user_id');
        $prof_id = $this->uri->segment(3);
        $this->session->set_userdata('mem_prof_id', $prof_id);
        if (empty($mem_user_id)) {
            $this->session->set_userdata('last_method', 'member/add_review');
            redirect('member/login');
        }
//        $user_id = $this->session->userdata('mem_user_id');
        if ($this->input->post('add_review') != NULL) {
            $this->load->library(array('form_validation'));
            $this->form_validation->set_rules('rating_msg', 'Review message', 'required');
            if ($this->form_validation->run() == true) {
                $data['rating'] = ($this->input->post('rating_val') != NULL) ? $this->input->post('rating_val') : '';
//        $data['review_title'] = $query['review_title'];
                $data['review_msg'] = ($this->input->post('rating_msg') != NULL) ? $this->input->post('rating_msg') : '';
                $data['user_id'] = $prof_id;
                $data['reviewed_by'] = $mem_user_id;
                $data['added_on'] = date('Y-m-d H:i:s');
//        $this->load->model('Profs_model');
                $this->load->model('Review_model');
                $review_exists = $this->Review_model->get_user_prof_review($mem_user_id, $prof_id);
                if (isset($review_exists) && $review_exists == TRUE) {
                    $review_id = $review_exists->id;
                    $upddata['review_msg'] = $data['review_msg'];
                    $upddata['rating'] = $data['rating'];
                    $inserted = $this->Review_model->update_review($review_id, $upddata);
                } else {
                    $inserted = $this->Review_model->add_review($data);
                }
                if ($inserted) {
                    $this->session->set_flashdata('message', 'Review added');
                    $this->session->set_flashdata('msg_type', 'alert-success');
                    redirect('service_provider/detail/' . $prof_id);
                }
            }
        }
        $this->load->model('Prof_cats_model');
        $this->data['prof_cats'] = $this->Prof_cats_model->view_cats();
        $this->load->view('add_review', $this->data);
    }

    public function login() {
        if ($this->input->post('login') != NULL) {
            $this->load->library(array('form_validation'));
            $this->form_validation->set_rules('first_name', 'Name', 'required');
            $this->form_validation->set_rules('phone', 'Mobile Number', 'required');

            if ($this->form_validation->run() == true) {

                $first_name = ($this->input->post('first_name') != NULL) ? $this->input->post('first_name') : '';
                $phone = ($this->input->post('phone') != NULL) ? $this->input->post('phone') : '';

                $otp = rand(1123, 9898);
                $timestamp = time();

                $this->load->model('User_model');
                $user = $this->User_model->get_user_by_phone($phone);

                if (!empty($user)) {
                    $data = array(
                        'otp' => $otp,
                        'otp_time' => $timestamp,
//                        'first_name' => $first_name,
                    );
                    $updated = $this->ion_auth->update($user->id, $data);
                } else {
                    $username = $phone;
                    $password = $phone;
                    $email = '';
                    $additional_data = array(
                        'phone' => $phone,
                        'first_name' => $first_name,
                        'otp' => $otp,
                        'otp_time' => $timestamp
                    );
                    $updated = $this->ion_auth->register($username, $password, $email, $additional_data, array(2));
                }
                if (isset($updated)) {
                    $this->load->helper('buildr');
                    $message1 = "Your OTP for signin is $otp. Don't share with anyone";
                    $message = str_replace(" ", "%20", $message1);
                    $otp_api = "http://alerts.ebensms.com/api/v3/index.php?method=sms&api_key=A45a2f192ecda08875f163567400dacac&to=$phone&sender=buildr&message=$message";
                    $otp_sent = file_get_contents($otp_api);
                    $otp_status = json_decode($otp_sent, TRUE);
                }

                if (isset($updated) && ($otp_status['status'] == 'OK')) {
                    $this->session->set_userdata('mem_phone_num', $phone);
                    redirect('member/otp');
                }
            } else {
                $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                $this->session->set_flashdata('message', $message);
                $this->session->set_flashdata('msg_type', 'alert-danger');
            }
        }
        $this->load->view('login');
    }

    public function otp() {
        $phone_num = $this->session->userdata('mem_phone_num');
        if ($this->input->post('signin') != NULL) {
            $this->load->library(array('form_validation'));
            $this->form_validation->set_rules('otp', 'OTP', 'required');
            if ($this->form_validation->run() == true) {
                $otp = ($this->input->post('otp') != NULL) ? $this->input->post('otp') : '';
                $this->load->model('User_model');
                $user = $this->User_model->get_user_by_phone($phone_num);

                $timestamp = time();

                if (($user->otp == $otp) && ($timestamp - $user->otp_time < 3600)) {
                    $this->session->set_userdata('mem_user_id', $user->id);
                    $last_method = $this->session->userdata('last_method', '');
                    if (isset($last_method) && $last_method == 'member/add_review') {
                        $prof_id = $this->session->userdata('mem_prof_id');
                        redirect('member/add_review/'.$prof_id);
                    } else {
                        redirect('/');
                    }
                } else {
                    $this->session->set_flashdata('message', 'Incorrect OTP');
                    $this->session->set_flashdata('msg_type', 'alert-danger');
                }
            }
        }
        $this->load->view('otp');
    }

}
