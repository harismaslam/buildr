<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Import from excel list
 * first import to database table
 * then run this file
 */
class Import_users extends CI_Controller {

    public function index() {
        $this->db->select('*');
        $this->db->where('email!=', '');
        $this->db->from('user_test_import');
        $query = $this->db->get();
        echo $query->num_rows();

        if ($query->num_rows() > 0) {
            $users = $query->result();

            foreach ($users as $user) {
                $name = $user->name;
                $email = $user->email;
                $password = $user->email;
                $username = $user->email;
                $name_arr = explode(' ', $name);
                $first_name = $name_arr[0];
                $last_name = $name_arr[1];
                $address = $user->address;
                $phone = $user->phone;
                $admin_id = 12;

                $additional_data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'address' => $address,
                    'phone' => $phone,
                    'admin_id' => $admin_id
                );

                

                $user_id = $this->ion_auth->register($username, $password, $email, $additional_data, $group_id);
                if ($user_id) {
                    echo $this->ion_auth->messages();
                } else {
                    echo $this->ion_auth->errors();
                }
            }
        }
    }

//    public function list2() {
//        //load our new PHPExcel library
//        $this->load->library('excel');
//        $heading = array('Email', 'Password', 'First Name', 'Last Name', 'User Type');
//
//        $this->excel->setActiveSheetIndex(0);
//
//        $this->excel->getActiveSheet()->setTitle('Customer List');
//
//        $rowNumberH = 1;
//        $colH = 'A';
//        foreach ($heading as $h) {
//            $this->excel->getActiveSheet()->setCellValue($colH . $rowNumberH, $h);
//            $this->excel->getActiveSheet()->getStyle($colH . $rowNumberH)->getFont()->setBold(true);
//            $colH++;
//        }
//        
//        $this->db->select('*');
//        $this->db->from('users u');
//        $this->db->join('users_groups ug', 'ug.user_id=u.id');
//        $this->db->join('groups gp', 'gp.id=ug.group_id');
//        $this->db->where('admin_id', 108);//live
////        $this->db->where('u.admin_id', 12);
//        $this->db->order_by('u.id', 'asc');
//        $query = $this->db->get();
//
//        if ($query->num_rows() > 0) {
//            $users = $query->result();
//
//            $row = 2;
//            foreach ($users as $user) {
//                $this->excel->getActiveSheet()->setCellValue('A'.$row, $user->email);
//                $this->excel->getActiveSheet()->setCellValue('B' . $row, $user->email);
//                $this->excel->getActiveSheet()->setCellValue('C' . $row, $user->first_name);
//                $this->excel->getActiveSheet()->setCellValueExplicit('D' . $row, $user->last_name);
//                $this->excel->getActiveSheet()->setCellValueExplicit('E' . $row, $user->description);
//                $row++;
//            }
//        }
//        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
//        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//
//        $filename = 'jons_imported_user_list.xls'; //save our workbook as this file name
//        header('Content-Type: application/vnd.ms-excel'); //mime type
//        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
//        header('Cache-Control: max-age=0');
//
//        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//
//        $objWriter->save('php://output');
//    }

}