<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliers extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'dynamicload'));

        $this->form_validation->set_error_delimiters('', '');

        $this->data['bodyclass'] = 'signed-in';

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
    }

    public function index() {
        if (user_has_privilege('view_suppliers')) {
            $this->load->model('Suppliers_model');
            $this->data['users'] = $this->Suppliers_model->get_suppliers();
            $this->load->view('suppliers/index', $this->data);
        }
    }

    public function add() {
        if (user_has_privilege('add_suppliers')) {
            $this->load->view('suppliers/add');
        }
    }

    public function save() {
        if (user_has_privilege('add_suppliers')) {
            $tables = $this->config->item('tables', 'ion_auth');
            $this->form_validation->set_rules('first_name', 'Frist Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == true) {

                $username = $this->input->post('first_name') . '_' . $this->input->post('last_name');
                $password = $this->input->post('password');
                $email = strtolower($this->input->post('email'));

                $input_date = $this->input->post('dob');
                if (!empty($input_date)) {
                    list($m, $d, $y) = explode('/', $input_date);
                    $dob = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y));
                } else {
                    $dob = '';
                }

                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'zipcode' => $this->input->post('zipcode'),
                    'dob' => $dob,
                    'gender' => $this->input->post('gender'),
                    'phone' => $this->input->post('phone'),
                );
                $group_id = array(4);
                $success = $this->ion_auth->register($username, $password, $email, $additional_data, $group_id);
                if ($success) {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    $this->session->set_flashdata('msg_type', 'alert-success');
                    redirect('suppliers');
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    $this->session->set_flashdata('msg_type', 'alert-danger');
                    redirect('suppliers/add');
                }
            } else {
                echo $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                $this->session->set_flashdata('message', $message);
                $this->session->set_flashdata('msg_type', 'alert-danger');
            }
        }
    }

    public function delete($id) {
        if (user_has_privilege('add_suppliers')) {
            $this->load->model('Suppliers_model');
            $this->Suppliers_model->delete_user($id);
            $this->session->set_flashdata('msg_type', 'alert-success');
            $this->session->set_flashdata('message', 'Supplier deleted');
            redirect("suppliers");
        } else {
            redirect("suppliers");
        }
    }

    public function edit($id) {
        if (user_has_privilege('add_suppliers')) {
            $this->load->model('Suppliers_model');
            $this->data['user'] = $this->Suppliers_model->get_supplier($id);
            $this->load->view('suppliers/edit', $this->data);
        }
    }

}
