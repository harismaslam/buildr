<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Trending extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_post() {

        $cats = array();
        $authkey = $this->post('authKey');
        $apartment_arr1 = array("apartmentID" => "1122", "apartmentName" => "Olive Ocean Regency", "apartmentAddress" => "Koramangla, Banglore, India", "apartmentImage" => base_url() . 'uploads/aprt_img/1.jpg', "designerName" => "Haris", "designerID" => "2221", "profileImage" => base_url() . 'uploads/user_profile/1.jpeg', "likes" => "322");
        $apartment_arr2 = array("apartmentID" => "2132", "apartmentName" => "Blue Ocean Regency", "apartmentAddress" => "Indiranagar, Banglore, India", "apartmentImage" => base_url() . 'uploads/aprt_img/3.jpg', "designerName" => "Vishnu", "designerID" => "2222", "profileImage" => base_url() . 'uploads/user_profile/3.jpeg', "likes" => "533");
        $apartment_arr3 = array("apartmentID" => "1152", "apartmentName" => "SHOBHA Arcades", "apartmentAddress" => "Jayanagar, Banglore, India", "apartmentImage" => base_url() . 'uploads/aprt_img/2.jpg', "designerName" => "Vishnu", "designerID" => "2222", "profileImage" => base_url() . 'uploads/user_profile/2.jpeg', "likes" => "1322");
        $apartment_arr4 = array("apartmentID" => "3222", "apartmentName" => "ARTECH Ocean Regency", "apartmentAddress" => "Vijayanagar, Banglore, India", "apartmentImage" => base_url() . 'uploads/aprt_img/1.jpg', "designerName" => "Nijesh", "designerID" => "1123", "profileImage" => base_url() . 'uploads/user_profile/1.jpeg', "likes" => "32022");
        $apartment_arr5 = array("apartmentID" => "4443", "apartmentName" => "OLIVE Ocean Regency", "apartmentAddress" => "Rajajinagar, Banglore, India", "apartmentImage" => base_url() . 'uploads/aprt_img/2.jpg', "designerName" => "Berlin", "designerID" => "2223", "profileImage" => base_url() . 'uploads/user_profile/2.jpeg', "likes" => "1412");
        $apartment_arr6 = array("apartmentID" => "3333", "apartmentName" => "SFS Ocean Regency", "apartmentAddress" => "MG Road, Banglore, India", "apartmentImage" => base_url() . 'uploads/aprt_img/3.jpg', "designerName" => "Arjun", "designerID" => "2230", "profileImage" => base_url() . 'uploads/user_profile/3.jpeg', "likes" => "22");
        $cats = array(
            array("catID" => "1", "catName" => "Eco friendly", "catImage" => base_url() . 'uploads/cat_img/1.jpg', "verifiedPeople" => "101 verified peoples near you", "verifiedHomes" => "720 verified homes near you", "apartmentList" => array($apartment_arr4, $apartment_arr5, $apartment_arr3, $apartment_arr2, $apartment_arr2,)),
            array("catID" => "2", "catName" => "Vastu", "catImage" => base_url() . 'uploads/cat_img/2.jpg', "verifiedPeople" => "27 verified peoples near you", "verifiedHomes" => "567 verified homes near you", "apartmentList" => array($apartment_arr4, $apartment_arr5, $apartment_arr2, $apartment_arr1, $apartment_arr6)),
            array("catID" => "4", "catName" => "Feng Shui", "catImage" => base_url() . 'uploads/cat_img/3.jpg', "verifiedPeople" => "41 verified peoples near you", "verifiedHomes" => "109 verified homes near you", "apartmentList" => array($apartment_arr4, $apartment_arr3, $apartment_arr2, $apartment_arr1, $apartment_arr6)),
        );
        if ($cats) {
            // Set the response and exit
            $this->response(array('status'=>'success',$cats), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $this->response(array('status' => 'failed','message' => 'No categories were found'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
