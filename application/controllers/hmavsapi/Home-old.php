<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Home extends REST_Controller {

    public $site_url;

    function __construct() {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_post() {
        $query = $this->post();

        $query2 = json_decode($query[0], TRUE);
        $query = $query2;

        $lat = $query['lat'];
        $long = $query['long'];
        $radius = $query['radius'];

//        $this->load->helper('file');
//        $txt_data = "lat=$lat, long=$long, radius=$radius";
//        write_file('newfile.txt', "\n" . $txt_data, 'a');

        $this->load->model('Project_cats_model');
        $apt_cats = $this->Project_cats_model->view_cats();

        $this->load->helper('buildr');
        foreach ($apt_cats as $val) {
            $img_url = $val->image;
            $val->image = base_url() . 'uploads/project_cats/' . $img_url;
            $apt_cat_id = $val->id;
            $total_apts = total_nearby_projs($lat, $long, $radius, $apt_cat_id);
            $val->total = $total_apts;
            $apts = get_nearby_projs($lat, $long, $radius, $apt_cat_id);
            $val->apts = $apts;
            $val->is_home_cats = 1;
            $val->is_ad = 0;
            $res1[] = $val;
        }
        

        $this->load->model('Prof_cats_model');
        $prof_cats = $this->Prof_cats_model->view_cats();

        foreach ($prof_cats as $val) {
            $img_url = $val->image;
            $val->image = base_url() . 'uploads/aprt_img/' . $img_url;
            $prof_cat_id = $val->id;
            $total_profs = total_nearby_profs($lat, $long, $radius, $prof_cat_id);
            $val->total = $total_profs;
            $profs = get_nearby_profs($lat, $long, $radius, $prof_cat_id);
            $val->profs = $profs;
            $val->is_home_cats = 0;
            $val->is_ad = 0;
            $res2[] = $val;
        }
        
        if (isset($res1)||isset($res)) {
            // Set the response and exit
            $this->response(array('status'=>'success','cats'=>$res1, 'profs'=>$res2), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            // Set the response and exit
            $this->response(array('status' => 'failed', 'message' => 'No results were found'), REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

}
