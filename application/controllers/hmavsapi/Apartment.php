<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Apartment extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_post() {
        $apt_id = $this->post('apt_id');

        $this->load->model('Project_model');
        $apt_det = (array) $this->Project_model->get_apt($apt_id);
        $apt_gallery = $this->Project_model->get_project_gal($apt_id);

        $apt_det['total_verified_profs'] = rand(0, 20);

        $gal = array();
        if (!empty($apt_gallery)) {
            $this->load->helper('apt_img');
            foreach ($apt_gallery as $val) {
                $img_url = $val->apt_gall_img;
                $val->apt_gall_img = base_url() . 'uploads/aprt_img/' . $img_url;
                $val->is_user_liked = rand(0, 1);
                $val->total_likes = rand(100, 10000);
                $val->total_qn_answd = count(get_apt_img_qns($val->id));
                $val->qns = get_apt_img_qns($val->id);
                $gal[] = (array) $val;
            }
        }
        $apt_det['gallery'] = $gal;

        if ($apt_det) {
            // Set the response and exit
            $apt_det['status'] = 'success';
            $this->response($apt_det, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            // Set the response and exit
            $this->response(array('status' => 'failed', 'message' => 'No results were found'), REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

}
