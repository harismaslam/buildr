<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Prof extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function detail_post() {

        $query = $this->post();

        //user id proffessional
        $prof_id = $query['prof_id'];

        $this->load->model('Profs_model');
        $prof_det = $this->Profs_model->get_prof_detail($prof_id);

        if (!empty($prof_det->photo)) {
            $prof_det->photo = base_url() . 'uploads/user_profile/' . $prof_det->photo;
        } else {
            $prof_det->photo = base_url() . 'uploads/user_profile/user-avatar.png';
        }

        $prof_det->company = empty($prof_det->company) ? '' : $prof_det->company;
        $prof_det->avg_rat = empty($prof_det->avg_rat) ? 0 : $prof_det->avg_rat;
        $prof_det->tot_rw = empty($prof_det->tot_rw) ? 0 : $prof_det->tot_rw;

        unset(
                $prof_det->prof_cat_id, $prof_det->otp, $prof_det->otp_time, $prof_det->username, $prof_det->id, $prof_det->password, $prof_det->ip_address, $prof_det->salt, $prof_det->activation_code, $prof_det->forgotten_password_code, $prof_det->forgotten_password_time, $prof_det->remember_code, $prof_det->created_on, $prof_det->last_login, $prof_det->active, $prof_det->image, $prof_det->user_id, $prof_det->added_on, $prof_det->is_verified, $prof_det->in_trash
        );
        $this->load->model('Review_model');
        //@param user id
        $reviews = $this->Review_model->get_user_reviews($prof_det->uid);
        $review_arr = array();
        $tot_1_rat = $tot_2_rat = $tot_3_rat = $tot_4_rat = $tot_5_rat = 0;
        if (!empty($reviews)) {
            foreach ($reviews as $review) {
                $reviewer = $this->ion_auth->user($review->reviewed_by)->row();
                $review->reviewer_name = $reviewer->first_name . ' ' . $reviewer->last_name;
                if ($review->rating < 1) {
                    $tot_1_rat += 1;
                } else if ($review->rating < 2) {
                    $tot_2_rat += 1;
                } else if ($review->rating < 3) {
                    $tot_3_rat += 1;
                } else if ($review->rating < 4) {
                    $tot_4_rat += 1;
                } else if ($review->rating <= 5) {
                    $tot_5_rat += 1;
                }
                unset(
                        $review->user_id, $review->added_on, $review->user_id
                );
                $reviewer_img = $reviewer->photo;
                if (!empty($reviewer_img)) {
                    $review->reviewer_photo = base_url() . 'uploads/user_profile/' . $reviewer_img;
                } else {
                    $review->reviewer_photo = base_url() . 'uploads/user_profile/user-avatar.png';
                }
                $review_arr[] = $review;
            }
        }
        $prof_det->reviews = empty($review_arr) ? array() : $review_arr;
        $prof_det->tot_rat_1 = $tot_1_rat;
        $prof_det->tot_rat_2 = $tot_2_rat;
        $prof_det->tot_rat_3 = $tot_3_rat;
        $prof_det->tot_rat_4 = $tot_4_rat;
        $prof_det->tot_rat_5 = $tot_5_rat;

        $this->load->model('Project_model');
        $projects = $this->Project_model->get_apts($prof_id);
        $prof_prj_gal = array();
        if (!empty($projects)) {
            foreach ($projects as $project) {
                $project->apt_img = !empty($project->apt_img) ? base_url() . 'uploads/aprt_img/' . $project->apt_img : '';

                $gallery = $this->Project_model->get_project_gal($project->id);

                $gal_arr = array();
                foreach ($gallery as $gal) {
                    $gal->apt_gall_img = !empty($gal->apt_gall_img) ? base_url() . 'uploads/aprt_img/' . $gal->apt_gall_img : '';
                    $gal_arr[] = $gal;
                    $prof_prj_gal[] = $gal;
                }

                $project->gallery = $gal_arr;

                $project_arr[] = $project;
            }
        }
        $prof_det->projects = empty($project_arr) ? array() : $project_arr;
        $prof_det->gall = empty($prof_prj_gal) ? array() : $prof_prj_gal;
        if ($prof_det) {
            $this->response(array('status' => 'success', 'prof_det' => $prof_det), REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => FALSE, 'message' => 'No results were found'), REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function add_review_post() {
        $query = $this->post();

        $data['rating'] = $query['rating'];
//        $data['review_title'] = $query['review_title'];
        $data['review_msg'] = $query['review_msg'];
        $prof_id = $data['user_id'] = $query['prof_id'];
        $user_id = $data['reviewed_by'] = $query['reviewed_by'];
        $data['added_on'] = date('Y-m-d H:i:s');

//        $this->load->model('Profs_model');
        $this->load->model('Review_model');
        $review_exists = $this->Review_model->get_user_prof_review($user_id, $prof_id);
        if (isset($review_exists) && $review_exists == TRUE) {
            $review_id = $review_exists->id;
            $upddata['review_msg'] = $data['review_msg'];
            $upddata['rating'] = $data['rating'];
            $inserted = $this->Review_model->update_review($review_id, $upddata);
        } else {
            $inserted = $this->Review_model->add_review($data);
        }

        if ($inserted) {
            $this->response(array('status' => 'success', 'message' => 'Review added successfully'), REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'failed', 'message' => 'Error in adding review'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function all_reviews_post() {
        $query = $this->post();

        $user_id = $query['prof_id'];

        $this->load->model('Review_model');
        $reviews = $this->Review_model->get_user_reviews($user_id);

        if (!empty($reviews)) {
            foreach ($reviews as $review) {
                $reviewer = $this->ion_auth->user($review->reviewed_by)->row();
                $review->reviewer_name = $reviewer->first_name . ' ' . $reviewer->last_name;
                if ($review->rating < 1) {
                    $tot_1_rat += 1;
                } else if ($review->rating < 2) {
                    $tot_2_rat += 1;
                } else if ($review->rating < 3) {
                    $tot_3_rat += 1;
                } else if ($review->rating < 4) {
                    $tot_4_rat += 1;
                } else if ($review->rating <= 5) {
                    $tot_5_rat += 1;
                }
                unset(
                        $review->user_id, $review->added_on, $review->user_id
                );
                $reviewer_img = $reviewer->photo;
                if (!empty($reviewer_img)) {
                    $review->reviewer_photo = base_url() . 'uploads/user_profile/' . $reviewer_img;
                } else {
                    $review->reviewer_photo = base_url() . 'uploads/user_profile/user-avatar.png';
                }
            }
        }

        if (!empty($reviews)) {
            $this->response(array('status' => 'success', 'reviews' => $reviews), REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'failed', 'message' => 'No reviews'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
