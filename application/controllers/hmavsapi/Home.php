<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Home extends REST_Controller {

    public $site_url;

    function __construct() {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

//    public function index_post() {
//        $query = $this->post();
//
////        $query2 = json_decode($query[0], TRUE);
////        $query = $query2;
//
//        $lat = !empty($query['lat']) ? $query['lat'] : 8.553522;
//        $long = !empty($query['long']) ? $query['long'] : 76.840611;
//        $radius = !empty($query['radius']) ? $query['radius'] : 200;
//        $prof_cat_id = !empty($query['prof_cat_id']) ? $query['prof_cat_id'] : 2;
//
//        $this->load->helper('buildr');
//
//        $this->load->model('Prof_cats_model');
//        $prof_cats = $this->Prof_cats_model->view_cats();
//        foreach ($prof_cats as $val) {
//            $img_url = $val->image;
//            $val->image = base_url() . 'uploads/aprt_img/' . $img_url;
//            $res1[] = $val;
//        }
//
//        $profs = top_nearby_profs($lat, $long, $radius);
//        $profs = !empty($profs) ? $profs : '';
//
//        if (isset($res1) || isset($profs)) {
//            $this->response(array('status' => 'success', 'profs_cat' => $res1, 'profs' => $profs), REST_Controller::HTTP_OK);
//        } else {
//            $this->response(array('status' => 'failed', 'message' => 'No results were found'), REST_Controller::HTTP_NOT_FOUND);
//        }
//    }

    public function index_post() {

        $query = $this->post();

//        $query2 = json_decode($query[0], TRUE);
//        $query = $query2;

        $lat = !empty($query['lat']) ? $query['lat'] : 8.553522;
        $long = !empty($query['long']) ? $query['long'] : 76.840611;
        $radius = !empty($query['radius']) ? $query['radius'] : 200;
        $prof_cat_id = !empty($query['prof_cat_id']) ? $query['prof_cat_id'] : 1;

        $this->load->model('Prof_cats_model');
        $prof_cats = $this->Prof_cats_model->view_cats();
        $prof_cats_arr = array();

        if (!empty($prof_cats)) {
            foreach ($prof_cats as $val) {
                $img_url = $val->image;
                $val->image = base_url() . 'uploads/cat_img/' . $img_url;
                $prof_cats_arr[] = $val;
            }
        }

        $this->load->helper('buildr');
        $profs = get_nearby_profs($lat, $long, $radius, $prof_cat_id);

        $this->load->model('Profs_model');
        $this->load->model('Review_model');
        if (!empty($profs)) {
            foreach ($profs as $val) {
                
                $val->company = empty($val->company)?'':$val->company;
                $val->avg_rat = empty($val->avg_rat)?'':$val->avg_rat;
                $val->tot_rw = empty($val->tot_rw)?'':$val->tot_rw;
                
                $prof_user_img = $val->photo;
                if (!empty($prof_user_img)) {
                    $val->photo = base_url() . 'uploads/user_profile/' . $prof_user_img;
                } else {
                    $val->photo = base_url() . 'uploads/user_profile/user-avatar.png';
                }

                unset(
                        $val->prof_cat_id, $val->otp, $val->otp_time, $val->username, $val->id, $val->password, $val->ip_address, $val->salt, $val->activation_code, $val->forgotten_password_code, $val->forgotten_password_time, $val->remember_code, $val->created_on, $val->last_login, $val->active, $val->image, $val->user_id, $val->added_on, $val->is_verified, $val->in_trash
                );
                //@param user id
                $reviews = $this->Review_model->get_user_reviews($val->uid);

                $review_arr = array();
                if (!empty($reviews)) {
                    foreach ($reviews as $review) {
                        $reviewer = $this->ion_auth->user($review->reviewed_by)->row();
                        $review->reviewer_name = $reviewer->first_name . ' ' . $reviewer->last_name;
                        unset(
                                $review->user_id, $review->added_on, $review->user_id
                        );
                        $reviewer_img = $reviewer->photo;
                        if (!empty($reviewer_img)) {
                            $review->reviewer_photo = base_url() . 'uploads/user_profile/' . $reviewer_img;
                        } else {
                            $review->reviewer_photo = base_url() . 'uploads/user_profile/user-avatar.png';
                        }
                        $review_arr[] = $review;
                    }
                }
                $val->review = empty($review_arr) ? array() : $review_arr;
                
            }
        } else {
            $profs = array();
        }

//        usort($profs, "cmp");

        if (isset($prof_cats_arr) || isset($profs)) {
            $this->response(array('status' => 'success', 'profs_cat' => $prof_cats_arr, 'profs' => $profs), REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'failed', 'message' => 'No results were found'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

}

function cmp($a, $b) {
    if ($a->avg_rating == $b->avg_rating) {
//        return 0;
        return ($a->total_reviews > $b->total_reviews) ? -1 : 1;
    }
    return ($a->avg_rating > $b->avg_rating) ? -1 : 1;
}
