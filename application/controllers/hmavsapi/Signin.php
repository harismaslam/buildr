<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Signin extends REST_Controller {

    public $site_url;

    function __construct() {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_post() {
        $query = $this->post();


//        $query2 = json_decode($query[0], TRUE);
//        $query = $query2;

        $phone_num = $query['phone_num'];
        $otp = $query['otp'];


//        $this->load->helper('file');
//        $txt_data = "phone=$phone_num, otp=$otp";
//        write_file('newfile.txt', "\n" . $txt_data, 'a');

        if (empty($phone_num) && empty($otp)) {
            $this->response(array('status' => 'failed', 'message' => 'Fields cannot be empty'), REST_Controller::HTTP_NOT_FOUND);
            exit();
        }

        $this->load->model('User_model');
        $user = $this->User_model->get_user_by_phone($phone_num);

        $timestamp = time();

        if (($user->otp == $otp) && ($timestamp - $user->otp_time < 3600)) {
            $this->response(array('status' => 'success', 'message' => 'Signin successful','user_id'=>$user->id), REST_Controller::HTTP_OK);
        } elseif ($timestamp - $user->otp_time > 3600) {
            $this->response(array('status' => 'failed', 'message' => 'OTP expired'), REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'failed', 'message' => 'Incorrect OTP'), REST_Controller::HTTP_OK);
        }
    }

    public function request_otp_post() {
        $query = $this->post();
              
//        $query2 = json_decode($query[0], TRUE);
//        $query = $query2;

        $phone_num = $query['phone_num'];

        if (empty($phone_num)) {
            $this->response(array('status' => 'failed', 'message' => 'Phone number is empty'), REST_Controller::HTTP_NOT_FOUND);
            exit();
        }

        //generate otp
        $otp = rand(1123,9898);
        $timestamp = time();

        $this->load->model('User_model');
        $user = $this->User_model->get_user_by_phone($phone_num);

        if (!empty($user)) {
            $data = array(
                'otp' => $otp,
                'otp_time' => $timestamp
            );
            $updated = $this->ion_auth->update($user->id, $data);
        } else {
            $username = $phone_num;
            $password = $phone_num;
            $email = '';
            $additional_data = array(
                'phone' => $phone_num,
                'otp' => $otp,
                'otp_time' => $timestamp
            );
            $updated = $this->ion_auth->register($username, $password, $email, $additional_data, array(2));
        }
        if(isset($updated)){
            $this->load->helper('buildr');
            $message1 = "Your OTP for signin is $otp. Don't share with anyone";
            $message = str_replace(" ", "%20", $message1);
            $otp_api = "http://alerts.ebensms.com/api/v3/index.php?method=sms&api_key=A45a2f192ecda08875f163567400dacac&to=$phone_num&sender=buildr&message=$message";
            $otp_sent = file_get_contents($otp_api);
            $otp_status = json_decode($otp_sent,TRUE);
        }

        if ($updated) {
            $this->response(array('status' => 'success', 'message' => 'OTP sent'), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            // Set the response and exit
            $this->response(array('status' => 'failed', 'message' => 'No results were found'), REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

}
