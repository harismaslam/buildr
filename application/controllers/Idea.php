
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Idea extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));

        $this->form_validation->set_error_delimiters('', '');

        $this->data['bodyclass'] = 'signed-in';

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
    }

    public function index() {
//        if (user_has_privilege('view_project')) {
        $this->load->model('Idea_model');
        $this->data['ideas'] = $aptts = $this->Idea_model->view_idea();
        $this->load->view('ideas/list', $this->data);
//        }
    }

    public function add() {
        $this->load->model('Profs_model');
        $this->data['designers'] = $this->Profs_model->get_profs();
        $this->data['proj_cats'] = $this->project_cats();

        $this->load->view('ideas/add', $this->data);
    }

    protected function project_cats() {
        $this->load->model('Project_cats_model');
        $proj_cats = $this->Project_cats_model->view_cats();
        $cats_arr = array();
        foreach ($proj_cats as $val) {
            $cats_arr[$val->id] = $val->name;
        }
        return $cats_arr;
    }

    public function save() {
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('apt_designer', 'Professional', 'required');

        if ($this->form_validation->run() == true) {
            $data['title'] = ($this->input->post('title') != NULL) ? $this->input->post('title') : '';
            $data['content'] = ($this->input->post('content') != NULL) ? $this->input->post('content') : '';
            $data['user_name'] = ($this->input->post('apt_designer') != NULL) ? $this->input->post('apt_designer') : '';
            $data['user_id'] = ($this->input->post('apt_designer_id') != NULL) ? $this->input->post('apt_designer_id') : '';
            $data['cat_id'] = ($this->input->post('proj_cat_id') != NULL) ? $this->input->post('proj_cat_id') : '';
            $data['tags'] = ($this->input->post('tags') != NULL) ? $this->input->post('tags') : '';

            $this->load->model('Idea_model');
            $idea_id = $this->Idea_model->add_idea($data);

            if ($idea_id) {
                $this->session->set_flashdata('msg_type', 'alert-success');
                $this->session->set_flashdata('message', 'Idea added');
                redirect('idea');
            }
        } else {
            $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->session->set_flashdata('message', $message);
            $this->session->set_flashdata('msg_type', 'alert-danger');
            redirect('idea/add');
        }
    }

    public function edit($id) {
                $this->load->model('Idea_model');
        if ($this->input->post('edit') != NULL) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('apt_designer', 'Professional', 'required');

            if ($this->form_validation->run() == true) {
                $data['title'] = ($this->input->post('title') != NULL) ? $this->input->post('title') : '';
                $data['content'] = ($this->input->post('content') != NULL) ? $this->input->post('content') : '';
                $data['user_name'] = ($this->input->post('apt_designer') != NULL) ? $this->input->post('apt_designer') : '';
                $data['user_id'] = ($this->input->post('apt_designer_id') != NULL) ? $this->input->post('apt_designer_id') : '';
                $data['cat_id'] = ($this->input->post('proj_cat_id') != NULL) ? $this->input->post('proj_cat_id') : '';
                $data['tags'] = ($this->input->post('tags') != NULL) ? $this->input->post('tags') : '';

                $updated = $this->Idea_model->update_idea($data, $id);

                if ($updated) {
                    $this->session->set_flashdata('msg_type', 'alert-success');
                    $this->session->set_flashdata('message', 'Idea updated');
                }
            } else {
                $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                $this->session->set_flashdata('message', $message);
                $this->session->set_flashdata('msg_type', 'alert-danger');
            }
        }
        $this->load->model('Profs_model');
        $this->data['designers'] = $this->Profs_model->get_profs();
        $this->data['proj_cats'] = $this->project_cats();
        
        $this->data['id'] = $id;
        $this->data['idea'] =  $idea= $this->Idea_model->get_idea($id);
        $this->load->view('ideas/edit', $this->data);
    }

}
