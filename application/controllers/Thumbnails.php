<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Thumbnails extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function service_provider() {
        $this->load->model('Profs_model');
        $rests = $this->Profs_model->get_profs();

        $this->load->library('image_lib');

        foreach ($rests as $val) {
            $filename = $val->photo;

            if (!empty($filename)) {
                $thumb_configs = array();
                $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/88x103/$filename", 'width' => 88, 'height' => 103, 'maintain_ratio' => TRUE);
                $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/458x257/$filename", 'width' => 458, 'height' => 9999, 'maintain_ratio' => TRUE);

                foreach ($thumb_configs as $thumb_config) {
                    $this->image_lib->thumb($thumb_config, FCPATH . 'uploads/user_profile/');
                }
            }
        }
    }

}
