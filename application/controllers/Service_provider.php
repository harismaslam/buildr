<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Service_provider extends CI_Controller {

    public function view($cat_id) {
        $this->load->model('Prof_cats_model');
        $this->data['prof_cats'] = $this->Prof_cats_model->view_cats();
//        $cat_id = ($this->input->post('cat') != NULL) ? $this->input->post('cat') : '';
        $profs = array();
        if (!empty($cat_id)) {
            $this->load->model('Profs_model');
            $profs = $this->Profs_model->get_profs($cat_id);
        }
        if (!empty($profs)) {
            $this->load->model('Review_model');
            $this->load->model('Project_model');
            foreach ($profs as $val) {
                $prof_user_img = $val->photo;
                if (!empty($prof_user_img)) {
                    $val->photo = base_url() . 'uploads/user_profile/thumbs/458x257/' . $prof_user_img;
                }
                unset(
                        $val->prof_cat_id, $val->otp, $val->otp_time, $val->username, $val->id, $val->password, $val->ip_address, $val->salt, $val->activation_code, $val->forgotten_password_code, $val->forgotten_password_time, $val->remember_code, $val->created_on, $val->last_login, $val->active, $val->image, $val->user_id, $val->added_on, $val->is_verified, $val->in_trash, $val->added_on
                );
                //@param user id
                $reviews = $this->Review_model->get_user_reviews($val->uid);
                $review_arr = array();
                if (!empty($reviews)) {
                    foreach ($reviews as $review) {
                        $reviewer = $this->ion_auth->user($review->reviewed_by)->row();
                        $review->reviewer_name = $reviewer->first_name . ' ' . $reviewer->last_name;
                        unset(
                                $review->user_id, $review->added_on, $review->user_id
                        );
                        $reviewer_img = $reviewer->photo;
                        if (!empty($reviewer_img)) {
                            $review->reviewer_photo = base_url() . 'uploads/user_profile/' . $reviewer_img;
                        } else {
                            $review->reviewer_photo = base_url() . 'uploads/user_profile/user-avatar.png';
                        }
                        $review_arr[] = $review;
                    }
                }
                $project_imgs = $this->Project_model->get_profs_project_img($val->uid, $limit = 2);
                $val->proj_imgs = $project_imgs;
                $val->review = empty($review_arr) ? array() : $review_arr;
            }
        }
        $this->data['profs'] = $profs;
        $this->load->model('Prof_cats_model');
        $this->data['cat_name'] = $this->Prof_cats_model->get_category($cat_id)->name;
        $this->load->view('service_provider', $this->data);
    }

    public function detail($id) {
        $this->load->model('Prof_cats_model');
        $this->data['prof_cats'] = $this->Prof_cats_model->view_cats();
        $prof_id = $id;

        $this->load->model('Profs_model');
        $prof_det = $this->Profs_model->get_prof_detail($prof_id);

        if (!empty($prof_det->photo)) {
            $prof_det->photo = base_url() . 'uploads/user_profile/thumbs/458x257/' . $prof_det->photo;
        } else {
            $prof_det->photo = base_url() . 'uploads/user_profile/user-avatar.png';
        }

        $prof_det->company = empty($prof_det->company) ? '' : $prof_det->company;
        $prof_det->avg_rat = empty($prof_det->avg_rat) ? 0 : $prof_det->avg_rat;
        $prof_det->tot_rw = empty($prof_det->tot_rw) ? 0 : $prof_det->tot_rw;

        unset(
                $prof_det->prof_cat_id, $prof_det->otp, $prof_det->otp_time, $prof_det->username, $prof_det->id, $prof_det->password, $prof_det->ip_address, $prof_det->salt, $prof_det->activation_code, $prof_det->forgotten_password_code, $prof_det->forgotten_password_time, $prof_det->remember_code, $prof_det->created_on, $prof_det->last_login, $prof_det->active, $prof_det->image, $prof_det->user_id, $prof_det->added_on, $prof_det->is_verified, $prof_det->in_trash
        );
        $this->load->model('Review_model');
        //@param user id
        $reviews = $this->Review_model->get_user_reviews($prof_det->uid);
        $review_arr = array();
        $tot_1_rat = $tot_2_rat = $tot_3_rat = $tot_4_rat = $tot_5_rat = 0;
        if (!empty($reviews)) {
            foreach ($reviews as $review) {
                $reviewer = $this->ion_auth->user($review->reviewed_by)->row();
                $review->reviewer_name = $reviewer->first_name . ' ' . $reviewer->last_name;
                if ($review->rating < 1) {
                    $tot_1_rat += 1;
                } else if ($review->rating < 2) {
                    $tot_2_rat += 1;
                } else if ($review->rating < 3) {
                    $tot_3_rat += 1;
                } else if ($review->rating < 4) {
                    $tot_4_rat += 1;
                } else if ($review->rating <= 5) {
                    $tot_5_rat += 1;
                }
                unset(
                        $review->user_id, $review->added_on, $review->user_id
                );
                $reviewer_img = $reviewer->photo;
                if (!empty($reviewer_img)) {
                    $review->reviewer_photo = base_url() . 'uploads/user_profile/' . $reviewer_img;
                } else {
                    $review->reviewer_photo = base_url() . 'uploads/user_profile/user-avatar.png';
                }
                $review_arr[] = $review;
            }
        }
        $prof_det->reviews = empty($review_arr) ? array() : $review_arr;
        $prof_det->tot_rat_1 = $tot_1_rat;
        $prof_det->tot_rat_2 = $tot_2_rat;
        $prof_det->tot_rat_3 = $tot_3_rat;
        $prof_det->tot_rat_4 = $tot_4_rat;
        $prof_det->tot_rat_5 = $tot_5_rat;

        $this->load->model('Project_model');
        $projects = $this->Project_model->get_apts($prof_id);
        $prof_prj_gal = array();
        if (!empty($projects)) {
            foreach ($projects as $project) {
                $project->apt_img = !empty($project->apt_img) ? base_url() . 'uploads/aprt_img/' . $project->apt_img : '';
                $gallery = $this->Project_model->get_project_gal($project->id);

                $gal_arr = array();
                if (!empty($gallery)) {
                    foreach ($gallery as $gal) {
                        $gal->apt_gall_orig = !empty($gal->apt_gall_img) ? base_url() . 'uploads/aprt_img/' . $gal->apt_gall_img : '';
                        $gal->apt_gall_thumb = !empty($gal->apt_gall_img) ? base_url() . 'uploads/aprt_img/thumbs/224x155/' . $gal->apt_gall_img : '';
                        $gal_arr[] = $gal;
                        $prof_prj_gal[] = $gal;
                    }
                }

                $project->gallery = $gal_arr;
                $project_arr[] = $project;
            }
        }
        $prof_det->projects = empty($project_arr) ? array() : $project_arr;
        $prof_det->gall = empty($prof_prj_gal) ? array() : $prof_prj_gal;
        $this->data['prof_det'] = $prof_det;
        $this->load->view('service_provider_detail', $this->data);
    }

}
