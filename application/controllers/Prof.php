<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prof extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'dynamicload'));

        $this->form_validation->set_error_delimiters('', '');

        $this->data['bodyclass'] = 'signed-in';

        if (!$this->ion_auth->logged_in()) {
            redirect('login', 'refresh');
        }
    }

    public function index() {
        if (user_has_privilege('view_profs')) {
            $this->load->model('Profs_model');
            $this->data['users'] = $this->Profs_model->get_profs();
            $this->load->view('profs/index', $this->data);
        }
    }

    public function add() {
        if (user_has_privilege('add_profs')) {
            $this->load->model('Prof_cats_model');
            $prof_cats = $this->Prof_cats_model->view_cats();
            $cats_arr = array();
            foreach ($prof_cats as $val) {
                $cats_arr[$val->id] = $val->name;
            }
            $this->data['prof_cats'] = $cats_arr;
            $this->load->view('profs/add', $this->data);
        }
    }

    public function save() {
        if (user_has_privilege('add_profs')) {
            $tables = $this->config->item('tables', 'ion_auth');
            $this->form_validation->set_rules('first_name', 'Frist Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == true) {
                $username = $this->input->post('first_name') . '_' . $this->input->post('last_name');
                $password = $this->input->post('password');
                $email = strtolower($this->input->post('email'));

                $input_date = $this->input->post('dob');
                $dob = '';
                if (!empty($input_date)) {
                    list($d, $m, $y) = explode('/', $input_date);
                    $dob = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y));
                }

                if (!empty($_FILES['userfile']['name'])) {
                    $this->load->library('upload');

                    $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . '/uploads/user_profile/';
                    $config['upload_path'] = $upload_path;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '2048';

                    $filename = time() . $_FILES['userfile']['name'];
                    $config['file_name'] = $filename;

                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('userfile')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('message', $error['error']);
                        $this->session->set_flashdata('msg_type', 'alert-danger');
                        redirect('prof/add');
                    }
                    $img_data = array('upload_data' => $this->upload->data());
                    $photo = $img_data['upload_data']['file_name'];

                    $thumb_configs = array();
                    $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/88x103/$filename", 'width' => 88, 'height' => 103, 'maintain_ratio' => TRUE);
                    $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/458x257/$filename", 'width' => 458, 'height' => 9999, 'maintain_ratio' => TRUE);
                    // Loop through the array to create thumbs
                    $this->load->library('image_lib');
                    foreach ($thumb_configs as $thumb_config) {
                        $this->image_lib->thumb($thumb_config, $upload_path);
                    }
                }

                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
//                    'state' => $this->input->post('state'),
                    'zipcode' => $this->input->post('zipcode'),
//                    'dob' => $dob,
//                    'gender' => $this->input->post('gender'),
                    'phone' => $this->input->post('phone'),
                    'contact_num' => $this->input->post('contact_num'),
                    'company' => $this->input->post('company'),
                    'website' => $this->input->post('website'),
                    'estd' => $this->input->post('estd'),
                    'description' => $this->input->post('description'),
                    'photo' => isset($photo) ? $photo : '',
//                    'lat' => $this->input->post('lat'),
//                    'long' => $this->input->post('long'),
                    'prof_cat_id' => $this->input->post('prof_cat_id')
                );
                $group_id = array(2);
                $success = $this->ion_auth->register($username, $password, $email, $additional_data, $group_id);
                if ($success) {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    $this->session->set_flashdata('msg_type', 'alert-success');
                    redirect('prof');
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    $this->session->set_flashdata('msg_type', 'alert-danger');
                    redirect('prof/add');
                }
            } else {
                $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                $this->session->set_flashdata('message', $message);
                $this->session->set_flashdata('msg_type', 'alert-danger');
            }
        }
    }

    public function delete($id_enc) {
        if (user_has_privilege('add_profs')) {
            $id = decode_url($id_enc);
            $this->load->model('Profs_model');
            $this->Profs_model->delete_user($id);
            $this->session->set_flashdata('msg_type', 'alert-success');
            $this->session->set_flashdata('message', 'Proffessional deleted');
            redirect("prof");
        }
        redirect("prof");
    }

    public function edit($id_enc) {
        if (user_has_privilege('add_profs')) {
            $id = decode_url($id_enc);
            if ($this->input->post('edit') != NULL) {
                $this->form_validation->set_rules('first_name', 'Frist Name', 'required');
                $this->form_validation->set_rules('last_name', 'Last Name', 'required');

                $tables = $this->config->item('tables', 'ion_auth');
                $curr_email = $this->ion_auth->user($id)->row()->email;
                if (($this->input->post('email') != NULL) && $curr_email != $this->input->post('email')) {
                    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
                    $this->form_validation->set_message('is_unique', '%s is already registered.');
                }
                if ($this->input->post('password') != NULL) {
                    $this->form_validation->set_rules('password', 'Password', 'required');
                }
                if ($this->form_validation->run() == true) {
                    $data['first_name'] = $this->input->post('first_name');
                    $data['last_name'] = $this->input->post('last_name');
                    $data['email'] = $this->input->post('email');
                    if ($this->input->post('password') != NULL) {
                        $data['password'] = $this->input->post('password');
                    }
                    $data['address'] = $this->input->post('address');
                    $data['city'] = $this->input->post('city');
//                    $data['state'] = $this->input->post('state');
                    $data['zipcode'] = $this->input->post('zipcode');
                    $data['company'] = $this->input->post('company');
                    $data['website'] = $this->input->post('website');
                    $data['estd'] = $this->input->post('estd');
                    $data['description'] = $this->input->post('description');
//                    $input_date = $this->input->post('dob');
//                    if (!empty($input_date)) {
//                        list($d, $m, $y) = explode('/', $input_date);
//                        $data['dob'] = date('Y-m-d', mktime(0, 0, 0, $m, $d, $y));
//                    }

                    if (!empty($_FILES['userfile']['name'])) {
                        $this->load->library('upload');

                        $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . '/uploads/user_profile/';
                        $config['upload_path'] = $upload_path;
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '2048';

                        $filename = time() . $_FILES['userfile']['name'];
                        $config['file_name'] = $filename;

                        $this->upload->initialize($config);

                        if (!$this->upload->do_upload('userfile')) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message', $error['error']);
                            $this->session->set_flashdata('msg_type', 'alert-danger');
                            redirect("prof/edit/$id");
                        }
                        $img_data = array('upload_data' => $this->upload->data());
                        $data['photo'] = $img_data['upload_data']['file_name'];

                        //Thumbnail generation
                        $thumb_configs = array();
                        $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/88x103/$filename", 'width' => 88, 'height' => 103, 'maintain_ratio' => TRUE);
                        $thumb_configs[] = array('source_image' => $filename, 'new_image' => "thumbs/458x257/$filename", 'width' => 458, 'height' => 9999, 'maintain_ratio' => TRUE);
                        // Loop through the array to create thumbs
                        $this->load->library('image_lib');
                        foreach ($thumb_configs as $thumb_config) {
                            $this->image_lib->thumb($thumb_config, $upload_path);
                        }

                        $prev_img = $this->ion_auth->user($id)->row()->photo;
                        $prev_img_path = $upload_path . $prev_img;

                        if (!empty($prev_img) && file_exists($prev_img_path)) {
                            unlink($prev_img_path);
                        }
                        if (!empty($prev_img) && file_exists($upload_path . "thumbs/88x103/" . $prev_img)) {
                            unlink($upload_path . "thumbs/88x103/" . $prev_img);
                        }
                        if (!empty($prev_img) && file_exists($upload_path . "thumbs/458x257/" . $prev_img)) {
                            unlink($upload_path . "thumbs/458x257/" . $prev_img);
                        }
                    }

//                    $data['gender'] = $this->input->post('gender');
                    $data['phone'] = $this->input->post('phone');
                    $data['contact_num'] = $this->input->post('contact_num');
//                    $data['lat'] = $this->input->post('lat');
//                    $data['long'] = $this->input->post('long');
                    $data['prof_cat_id'] = $this->input->post('prof_cat_id');
                    $updated = $this->ion_auth->update($id, $data);

                    if ($updated) {
                        $this->session->set_flashdata('msg_type', 'alert-success');
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                    }
                } else {
                    $message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                    $this->session->set_flashdata('message', $message);
                    $this->session->set_flashdata('msg_type', 'alert-danger');
                }
            }
            $this->load->model('Prof_cats_model');
            $prof_cats = $this->Prof_cats_model->view_cats();
            $cats_arr = array();
            foreach ($prof_cats as $val) {
                $cats_arr[$val->id] = $val->name;
            }
            $this->data['prof_cats'] = $cats_arr;
            $this->load->model('Profs_model');
            $this->data['user'] = $this->Profs_model->get_prof($id);
            $this->data['id'] = $id;
            $this->load->view('profs/edit', $this->data);
        }
    }

    public function remove_picture($del_img, $prof_id_enc) {

        $prof_id = decode_url($prof_id_enc);
        if (user_has_privilege('add_profs')) {

            $upload_path = dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/user_profile/";

            $data['photo'] = '';
            $updated = $this->ion_auth->update($prof_id, $data);

            if ($updated == TRUE) {
                if (!empty($del_img) && file_exists($upload_path . $del_img)) {
                    unlink($upload_path . $del_img);
                }
                $this->session->set_flashdata('msg_type', 'alert-success');
                $this->session->set_flashdata('message', 'Photo deleted');
            }
            redirect("prof/edit/$prof_id_enc");
        }
    }

}
