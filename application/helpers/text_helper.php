<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('short_text')) {

    function short_text($string, $your_desired_width) {
        if(strlen($string)>$your_desired_width){
        return substr($string, 0, strpos(wordwrap($string, $your_desired_width), "\n"));
        }
        else{
            return $string;
        }
    }

}

if (!function_exists('print_stars')) {

    function print_stars($val) {
        switch (round($val)) {
            case 1: echo '*';
                break;
            case 2: echo '**';
                break;
            case 3: echo '***';
                break;
            case 4: echo '****';
                break;
            case 5: echo '*****';
                break;
            default : echo ' ';
        }
    }

}


