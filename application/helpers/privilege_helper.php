<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('user_has_privilege')) {

    function user_has_privilege($privilege) {
        $ci = & get_instance();

        $user_groups = $ci->ion_auth->get_users_groups()->result();
        $user_type = $user_groups[0]->name;

        $ci->db->select('*');
        $ci->db->from('user_privileges');
        $ci->db->where('privilege', $privilege);
        $ci->db->where($user_type, 1);

        $query = $ci->db->get();
        return ($query->num_rows() != 0) ? TRUE : show_error('No Access');
    }

}

if (!function_exists('is_users_item')) {

    function is_users_item($tbl_name, $item_id) {
        $ci = & get_instance();
        $admin_id = $ci->ion_auth->get_user_id();

        $ci->db->select('*');
        $ci->db->from($tbl_name);
        $ci->db->where('admin_id', $admin_id);
        $ci->db->where('id', $item_id);
        $query = $ci->db->get();
        
//        echo $ci->db->last_query();

        return ($query->num_rows() > 0) ? TRUE : show_error('No Access');
    }

}

function encode_url($string, $key = "", $url_safe = TRUE) {
    $ci = & get_instance();

    $ci->load->library(array('encrypt'));
    if ($key == null || $key == "") {
        $key = "hma1988_url_enc";
    }

    $ret = $ci->encrypt->encode($string, $key);

    if ($url_safe) {
        $ret = strtr(
                $ret, array(
            '+' => '.',
            '=' => '-',
            '/' => '~'
                )
        );
    }

//    return $ret;
    return $string;
}

function decode_url($string, $key = "") {
    $ci = & get_instance();

    $ci->load->library(array('encrypt'));
    if ($key == null || $key == "") {
        $key = "hma1988_url_enc";
    }
//    $string = strtr(
//            $string, array(
//        '.' => '+',
//        '-' => '=',
//        '~' => '/'
//            )
//    );

//    return $ci->encrypt->decode($string, $key);
    return $string;
}