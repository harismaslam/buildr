<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('total_nearby_profs')) {

    function total_nearby_profs($lat, $long, $radius, $cat_id) {
        $ci = & get_instance();

        $ci->load->model('Profs_model');
        $profs = $ci->Profs_model->get_profs($cat_id);
        foreach ($profs as $val) {
            if (!empty($val->lat) && !empty($val->long))
                $prof_cords[] = array($val->lat, $val->long);
        }
        $center = array($lat, $long);
        $cords = get_coordinates_within_radius($prof_cords, $center, $radius);
        return count($cords);
    }

}

if (!function_exists('get_nearby_profs')) {

    function get_nearby_profs($lat, $long, $radius, $cat_id) {
        $ci = & get_instance();
        $prof_arr = array();

        $ci->load->model('Profs_model');
        $profs = $ci->Profs_model->get_profs($cat_id);
//        if (!empty($profs)) {
//            foreach ($profs as $val) {
//                if (!empty($val->lat) && !empty($val->long)) {
//                    if (isCordinatewithinRadius($lat, $long, $val->lat, $val->long, $radius) == TRUE) {
//                        $prof_arr[] = $val;
//                    }
//                }
//            }
//        }
//        return $prof_arr;
        return $profs;
    }

}

if (!function_exists('total_nearby_projs')) {

    function total_nearby_projs($lat, $long, $radius, $cat_id) {
        $ci = & get_instance();

        $ci->load->model('Project_model');
        $apts = $ci->Project_model->get_apts($cat_id);
        foreach ($apts as $val) {
            if (!empty($val->apt_lat) && !empty($val->apt_long))
                $apt_cords[] = array($val->apt_lat, $val->apt_long);
        }
        $center = array($lat, $long);
        $cords = get_coordinates_within_radius($apt_cords, $center, $radius);
        return count($cords);
    }

}

if (!function_exists('get_nearby_projs')) {

    function get_nearby_projs($lat, $long, $radius, $cat_id) {
        $ci = & get_instance();
        $apt_arr = array();
        $ci->load->model('Project_model');
        $apts = $ci->Project_model->get_apts($cat_id);
        foreach ($apts as $val) {
            if (!empty($val->apt_lat) && !empty($val->apt_long)) {
                if (isCordinatewithinRadius($lat, $long, $val->apt_lat, $val->apt_long, $radius) == TRUE) {
                    $img_url = $val->apt_img;
                    $val->apt_img = base_url() . 'uploads/aprt_img/' . $img_url;
                    $val->profileImage = base_url() . "uploads/user_profile/1.jpeg";
                    $apt_gallery = $ci->Project_model->get_project_gal($val->id);

                    $gallery = array();
                    if (!empty($apt_gallery)) {
                        $ci->load->helper('apt_img');
                        foreach ($apt_gallery as $gal) {
                            $gal_img_url = $gal->apt_gall_img;
                            $gal->apt_gall_img = base_url() . 'uploads/aprt_img/' . $gal_img_url;
                            $gal->is_user_liked = rand(0, 1);
                            $gal->total_likes = rand(100, 10000);
                            $gal->total_qn_answd = count(get_apt_img_qns($gal->id));
                            $gal->qns = get_apt_img_qns($gal->id);
                            $gallery[] = $gal;
                        }
                    }
                    $val->gallery = (array) $gallery;
                    $apt_arr[] = $val;
                }
            }
        }
        return $apt_arr;
    }

}

function isCordinatewithinRadius($lat1, $long1, $lat2, $long2, $radius) {
    $distance = 3959 * acos(cos(radians($lat1)) * cos(radians($lat2)) * cos(radians($long2) - radians($long1)) + sin(radians($lat1)) * sin(radians($lat2)));
    if ($distance < $radius) {
        return TRUE;
    }
    return FALSE;
}

function get_coordinates_within_radius($coordinateArray, $center, $radius) {
    $resultArray = array();
    $lat1 = $center[0];
    $long1 = $center[1];
    foreach ($coordinateArray as $coordinate) {
        $lat2 = $coordinate[0];
        $long2 = $coordinate[1];
        $distance = 3959 * acos(cos(radians($lat1)) * cos(radians($lat2)) * cos(radians($long2) - radians($long1)) + sin(radians($lat1)) * sin(radians($lat2)));
        if ($distance < $radius)
            $resultArray[] = $coordinate;
    }
    return $resultArray;
}

function radians($deg) {
    return $deg * M_PI / 180;
}

if (!function_exists('top_nearby_profs')) {

    function top_nearby_profs($lat, $long, $radius) {
        $ci = & get_instance();

        $ci->load->model('Prof_cats_model');
        $ci->load->model('Profs_model');
        $ci->load->model('Review_model');
        $prof_cats = $ci->Prof_cats_model->view_cats();

        foreach ($prof_cats as $val) {
//@param proffesionals category id
            $profs[] = $ci->Profs_model->get_top_profs($val->id);
        }
        foreach ($profs as $val) {
            $img_url = $val->photo;
            $val->photo = !empty($img_url) ? base_url() . 'uploads/user_profile/' . $img_url : base_url() . 'uploads/user_profile/user-avatar.png';
//@param user id
//            $review = $ci->Review_model->get_user_top_reviews($val->id);
            $reviews = $ci->Review_model->get_user_reviews($val->id);
            if (!empty($reviews)) {
                foreach ($reviews as $review) {
                    $reviewer = $ci->ion_auth->user($review->user_id)->row();
                    $review->reviewer_name = $reviewer->first_name;
                    $review->reviewer_image = !empty($reviewer->photo) ? base_url() . 'uploads/user_profile/' . $reviewer->photo : base_url() . 'uploads/user_profile/user-avatar.png';
                    $reviews[] = $review;
                }
            }
            $val->review = !empty($reviews) ? $reviews : array();
        }
//        $prof_arr = array();
//        $ci->load->model('Profs_model');
//        $profs = $ci->Profs_model->get_profs($cat_id);
//        foreach ($profs as $val) {
//            if (!empty($val->lat) && !empty($val->long)) {
//                if (isCordinatewithinRadius($lat, $long, $val->lat, $val->long, $radius) == TRUE) {
//                    $prof_arr[] = $val;
//                }
//            }
//        }
        return (!empty($profs)) ? $profs : '';
    }

}