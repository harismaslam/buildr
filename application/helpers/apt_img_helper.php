<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_apt_img_qns')) {
    
    function get_apt_img_qns($img_id){
        $ci = & get_instance();

        $ci->load->model('Project_img_qn_model');
        $qns = $ci->Project_img_qn_model->get_qns($img_id);

        return $qns;
    }
}