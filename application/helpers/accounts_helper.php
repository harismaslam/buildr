<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('profile_pic')) {

    function profile_pic() {
        $ci = & get_instance();

        $user_id = $ci->ion_auth->get_user_id();
        if (!empty($user_id)) {
            $ci->load->model('Accounts_settings_model');
            $sett = $ci->Accounts_settings_model->get_sch_set($user_id);
            if (!empty($sett)) {
                $photo = $sett->photo;
            }
        }

        return (isset($photo)) ? $photo : '';
    }

}

function multi_arr_search($array, $key, $value) {
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, multi_arr_search($subarray, $key, $value));
        }
    }

    return $results;
}

?>