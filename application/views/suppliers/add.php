<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Suppliers</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php echo form_open("suppliers/save", array('class' => 'form commonform', 'id' => 'supplier-add', 'role' => 'form')); ?>
        <div class="form-group">
            <label class="">First Name</label>
            <?php
            $first_name = array('name' => 'first_name',
                'id' => 'first_name',
                'class' => 'required form-control',
            );
            $fname_val = !empty($user->first_name) ? $user->first_name : '';
            ?>
            <?php echo form_input($first_name, $fname_val); ?>
        </div>
        <div class="form-group">
            <label class="">Last Name</label>
            <?php
            $last_name = array('name' => 'last_name',
                'id' => 'last_name',
                'class' => 'required form-control',
            );
            $lname_val = !empty($user->last_name) ? $user->last_name : '';
            ?>
            <?php echo form_input($last_name, $lname_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Email Address</label>
            <?php
            $email = array('name' => 'email',
                'id' => 'email',
                'class' => 'required form-control',
//                'readonly' => 'true',
                'style' => 'margin-bottom:15px;'
            );
            if (!empty($user->email)) {
                $email_val = $user->email;
            } else {
                $email_val = '';
            }
            ?>
            <?php echo form_input($email, $email_val); ?>
        </div>
        <div class="form-group">
            <label class="">Password</label>
            <?php
            $pswd_req = !empty($user->password) ? '' : 'required';
            $pswd = array('name' => 'password',
                'id' => 'password',
                'class' => "form-control $pswd_req",
                'style' => 'margin-bottom:15px;',
            );
            echo form_password($pswd);
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Address</label>
            <?php
            $address = array('name' => 'address',
                'id' => 'address',
                'class' => 'form-control',
            );
            $addr_val = !empty($user->address) ? $user->address : '';
            ?>
            <?php echo form_input($address, $addr_val); ?>
        </div>
        <div class="form-group">
            <label class="">City</label>
            <?php
            $city = array('name' => 'city',
                'id' => 'city',
                'class' => 'form-control',
            );
            $city_val = !empty($user->city) ? $user->city : '';
            ?>
            <?php echo form_input($city, $city_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">State</label>
            <?php $this->load->helper('state'); ?>
            <?php
//            $state_val = !empty($user->state) ? $user->state : '';
            $state_val = array(
                'kl' => 'Kerala',
                'ka' => 'Karnataka',
                'tn' => 'Tamil Nadu'
            );
            $state = array('name' => 'state',
                'id' => 'state',
                'class' => 'form-control',
                'style' => 'margin-bottom:15px;'
            );
            ?>
            <?php echo form_dropdown($state, $state_val, ''); ?>
        </div>
        <div class="form-group">
            <label class="">Zip Code</label>
            <?php
            $zipcode = array('name' => 'zipcode',
                'id' => 'zipcode',
                'class' => 'form-control',
                'style' => 'margin-bottom:15px;'
            );
            $zip_val = !empty($user->zipcode) ? $user->zipcode : '';
            ?>
            <?php echo form_input($zipcode, $zip_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Date of Birth</label>
            <?php
            $dob = array('name' => 'dob',
                'id' => 'dob',
                'class' => 'form-control',
//                'readonly' => 'true',
            );
            if (!empty($user->dob) && ($user->dob != '0000-00-00')) {
                $dob_val1 = $user->dob;
                $dob_val2 = date_create($dob_val1);
                $dob_val = date_format($dob_val2, "m-d-Y");
            } else {
                $dob_val = '';
            }
            ?>
            <?php echo form_input($dob, $dob_val); ?>
        </div>
        <div class="form-group">
            <label class="">Gender</label>
            <span style="margin-right: 5px;"> Male</span>
            <?php
            $male = array('name' => 'gender',
                'id' => 'gender',
                'class' => 'form-control',
                'style' => 'margin-right:10px;display:inline '
            );
            $gender_val = !empty($user->gender) ? $user->gender : '';
            if (!empty($user) && ($user->gender == 1)) {
                $is_male = TRUE;
            } else {
                $is_male = FALSE;
            }
            if (empty($user)) {
                $is_male = TRUE;
            }
            ?>
            <?php echo form_radio($male, '1', $is_male); ?>
            <span style="margin-right: 5px;">Female</span>
            <?php
            $female = array('name' => 'gender',
                'id' => 'gender',
                'class' => 'form-control',
                'style' => 'margin-right:10px; display:inline'
            );
            if (!empty($user) && ($user->gender == 0))
                $is_female = TRUE;
            else
                $is_female = FALSE;
            ?>
            <?php echo form_radio($female, '0', $is_female); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Phone</label>
            <?php
            $phone = array('name' => 'phone',
                'id' => 'phone',
                'class' => 'form-control',
            );
            $phone_val = !empty($user->phone) ? $user->phone : '';
            ?>
            <?php echo form_input($phone, $phone_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form_btn_wrap">
            <?php echo form_submit(array('class' => 'btn btn-default save', 'id' => 'save', 'name' => 'add'), 'Save'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#dob').datetimepicker({
            timepicker: false,
            format: 'd/m/Y'
        });
    });
</script>