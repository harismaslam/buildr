<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <?php
        $prof_id_enc = encode_url($prof_id);
        ?>
        <h1 class="page-header">
            Projects
            <a href="<?php echo site_url("project/add/$prof_id_enc") ?>" class="btn btn-default pull-right" style="margin-right: 5px;">Add Project</a>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php if (!empty($aptmts)): ?>
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="cats-list">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="nosort">Address</th>
                            <!--<th class="nosort">Image</th>-->
<!--                            <th>Owner</th>
                            <th>Designer</th>-->
                            <th class="nosort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($aptmts as $val): ?>
                            <tr>
                                <td style="vertical-align: middle">
                                    <?php echo $val->apt_name; ?>
                                </td>
                                <td style="vertical-align: middle">
                                    <?php echo $val->apt_addr; ?>
                                </td>
<!--                                <td>
                                    <img style="max-width: 150px;" class="img-responsive" src="<?php echo base_url(); ?>/uploads/aprt_img/<?php echo $val->apt_img; ?>"/>
                                </td>-->
<!--                                <td style="vertical-align: middle">
                                    <?php echo $val->apt_owner; ?>
                                </td>
                                <td style="vertical-align: middle">
                                    <?php echo $val->apt_designer; ?>
                                </td>-->
                                <td style="vertical-align: middle">
                                    <?php
                                    $edit_attr = array(
                                        "title" => "Edit"
                                    );
                                    echo anchor("project/edit/$val->id/$prof_id_enc", "<i class='fa fa-edit fa-lg'></i>", $edit_attr);
                                    ?>
                                    <?php
                                    $del_attr = array(
                                        "onClick" => "return confirm('Are you sure you want to delete?')",
                                        "title" => "Delete"
                                    );
                                    echo anchor("project/delete/$val->id/$prof_id_enc", "<i class='fa fa-trash fa-lg'></i>", $del_attr);
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
        No Projects
        <?php endif; ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<!-- DataTables CSS -->
<link href="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="<?php echo base_url(); ?>assets/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    jQuery(document).ready(function () {
        jQuery('#cats-list').DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>