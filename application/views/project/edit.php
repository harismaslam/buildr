<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<?php
$prof_id = $this->uri->segment(4);
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Project</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php echo form_open_multipart("project/edit/$id/$prof_id", array('class' => 'form commonform', 'id' => 'project-add', 'role' => 'form')); ?>
        <div class="form-group">
            <label class="">Name</label>
            <?php
            $apt_name = array('name' => 'apt_name',
                'id' => 'apt_name',
                'class' => 'required form-control',
            );
            $apt_name_val = !empty($apt->apt_name) ? $apt->apt_name : '';
            ?>
            <?php echo form_input($apt_name, $apt_name_val); ?>
        </div>
        <div class="form-group">
            <label class="">Address</label>
            <?php
            $apt_addr = array('name' => 'apt_addr',
                'id' => 'apt_addr',
                'class' => 'form-control',
            );
            $apt_addr_val = !empty($apt->apt_addr) ? $apt->apt_addr : '';
            ?>
            <?php echo form_input($apt_addr, $apt_addr_val); ?>
        </div>
        <div class="clearfix"></div>
<!--        <div class="form-group">
            <label class="">Lat</label>
            <?php
//            $apt_lat = array('name' => 'apt_lat',
//                'id' => 'apt_lat',
//                'class' => 'form-control',
//            );
//            $apt_lat_val = !empty($apt->apt_lat) ? $apt->apt_lat : '';
            ?>
            <?php // echo form_input($apt_lat, $apt_lat_val); ?>
        </div>-->
<!--        <div class="form-group">
            <label class="">Long</label>
            <?php
//            $apt_long = array('name' => 'apt_long',
//                'id' => 'apt_long',
//                'class' => 'form-control',
//            );
//            $apt_long_val = !empty($apt->apt_long) ? $apt->apt_long : '';
            ?>
            <?php // echo form_input($apt_long, $apt_long_val); ?>
        </div>-->
        <div class="clearfix"></div>
        <!--<div class="form-group">-->
            <!--<label class="">Profile Picture</label>-->
            <?php
//            $file = array(
//                'name' => 'userfile',
//                'id' => 'userfile',
//            );
            ?>
            <?php // echo form_upload($file); ?>    
            <?php // if (!empty($apt->apt_img)): ?>
                <!--<img style="max-width: 150px; margin-top: 15px;" class="img-responsive" src="<?php echo base_url(); ?>/uploads/aprt_img/<?php echo $apt->apt_img; ?>"/>-->
            <?php // endif; ?>
        <!--</div>-->
<!--        <div class="form-group">
            <label class="">Designer</label>
            <?php
//            $apt_designer = array('name' => 'apt_designer',
//                'id' => 'apt_designer',
//                'class' => "form-control typeahead required",
//                'style' => 'margin-bottom:15px;',
////                'autocomplete'=>'off'
//            );
//            $designer_val = !empty($apt->apt_designer) ? $apt->apt_designer : '';
//            echo form_input($apt_designer, $designer_val);
//            $des_hid_val = !empty($apt->apt_designer_id) ? $apt->apt_designer_id : '';
            ?>
            <?php // echo form_hidden('apt_designer_id', $des_hid_val); ?>
        </div>-->
<!--        <div class="form-group">
            <label class="">Owner</label>
            <?php
            $apt_owner = array('name' => 'apt_owner',
                'id' => 'apt_owner',
                'class' => "form-control typeahead",
                'style' => 'margin-bottom:15px;',
//                'autocomplete'=>'off'
            );
            $owner_val = !empty($apt->apt_owner) ? $apt->apt_owner : '';
            echo form_input($apt_owner, $owner_val);
            $own_hid_val = !empty($apt->apt_owner_id) ? $apt->apt_owner_id : '';
            ?>
            <?php echo form_hidden('apt_owner_id', $own_hid_val); ?>
        </div>-->
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Category</label>
            <?php
            $proj_cat = array('name' => 'proj_cat_id',
                'id' => 'proj_cat_id',
                'class' => 'form-control',
            );
            $proj_cat_val = !empty($apt->proj_cat_id) ? $apt->proj_cat_id : '';
            ?>
            <?php echo form_dropdown($proj_cat, $proj_cats, $proj_cat_val); ?>
        </div>
        <div class="clearfix"></div>
        <div id="image-gallery">
            <label>Image Gallery</label>
            <?php if (!empty($gal)): ?>
                <?php foreach ($gal as $val): ?>
                    <div style="display: table;" class="apt-gal-item">
                        <?php if (!empty($val->apt_gall_img)): ?>
                            <div>
                                <img style="max-width: 150px; margin-top: 15px;" class="img-responsive" src="<?php echo base_url(); ?>/uploads/aprt_img/<?php echo $val->apt_gall_img; ?>"/>
                                <?php echo $val->apt_gall_img_title; ?>
                            </div>
                            <div>
                                <a href="<?php echo site_url("project/del_gal_img/$val->id/$id/$prof_id"); ?>">Remove</a>
                            </div>

                        <?php endif; ?>
                    </div>

                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <input type="checkbox" name="add_gal" id="add_gal"/> Add Gallery Items
        </div>
        <div class="clearfix"></div>
        <div id="itemRows" style="display: none;">
            <div class="form-group">
                <label>Image Title</label>
                <input type="text" class="form-control required" name="gal_title[]"/>
                <label>Image</label>
                <input class="gal_img form-control required" type="file" name="gal_file[]">
                <input id="addrow22" onclick="addRow(this.form);" type="button" value="Add row" />
            </div>
        </div>
        <div class="form_btn_wrap">
            <a class="btn btn-default pull-left" href="<?php echo site_url("project/view/" . encode_url($prof_id)); ?>">
                Back
            </a>
            <?php echo form_submit(array('class' => 'btn btn-default save', 'id' => 'save', 'name' => 'edit'), 'Save'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<?php
$own_optn = array();
if (!empty($owners)) {
    foreach ($owners as $user) {
        $arr = array();
        $arr['usrId'] = $user->uid;
        $arr['usrName'] = $user->first_name . ' ' . $user->last_name;
        $own_optn[] = $arr;
    }
}

$dsgn_optn = array();
if (!empty($designers)) {
    foreach ($designers as $user) {
        $arr = array();
        $arr['usrId'] = $user->uid;
        $arr['usrName'] = $user->first_name . ' ' . $user->last_name;
        $dsgn_optn[] = $arr;
    }
}
?>
<link href="<?php echo base_url(); ?>assets/css/fileinput.min.css" rel="stylesheet"/>
<script src="<?php echo base_url(); ?>assets/js/fileinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap3-typeahead.min.js" type="text/javascript"></script>
<script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('#add_gal').click(function () {
                                jQuery('#itemRows').toggle();
                            });
                            jQuery("#userfile").fileinput({
                                showUpload: false
                            });
//                            jQuery('#apt_owner.typeahead').typeahead({
//                                source: function (query, process) {
//                                    states = [];
//                                    map = {};
//                                    var data = <?php echo json_encode($own_optn); ?>;
//                                    $.each(data, function (i, state) {
//                                        map[state.usrName] = state;
//                                        states.push(state.usrName);
//                                    });
//                                    process(states);
//                                },
//                                updater: function (item) {
//                                    selectedCus = map[item].usrId;
//                                    $('input[name=apt_owner_id]').val(selectedCus);
//                                    return item;
//                                }
//                            });
//                            jQuery('#apt_designer.typeahead').typeahead({
//                                source: function (query, process) {
//                                    states = [];
//                                    map = {};
//                                    var data = <?php echo json_encode($dsgn_optn); ?>;
//                                    $.each(data, function (i, state) {
//                                        map[state.usrName] = state;
//                                        states.push(state.usrName);
//                                    });
//                                    process(states);
//                                },
//                                updater: function (item) {
//                                    selectedCus = map[item].usrId;
//                                    $('input[name=apt_designer_id]').val(selectedCus);
//                                    return item;
//                                }
//                            });

                        });
                        var rowNum = 0;
                        function addRow(frm) {
                            rowNum++;
                            var row = '<div class="form-group" id="rowNum' + rowNum + '"><label>Image Title</label>\n\
                    <input type="text" class="form-control required" name="gal_title[]"/>\n\
 <label>Image</label>\n\
<input class="gal_img form-control required" type="file" name="gal_file[]"> \n\
<input type="button" value="Remove" onclick="removeRow(' + rowNum + ');"></div>';
                            jQuery('#itemRows').append(row);
                        }

                        function removeRow(rnum) {
                            jQuery('#rowNum' + rnum).remove();
                        }
                        jQuery(".gal_img").fileinput({
                            showUpload: false
                        });
                        function fileupld() {
                            jQuery(".gal_img").fileinput({
                                showUpload: false
                            });
                        }

                        jQuery(document).on("click", "#addrow22", function () {
                            fileupld();
                        });
</script>