<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Service Provider</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (!empty($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php echo form_open_multipart("prof/edit/" . encode_url($id), array('class' => 'form', 'id' => 'profs-edit', 'role' => 'form')); ?>
        <div class="form-group">
            <label class="">First Name</label>
            <?php
            $first_name = array('name' => 'first_name',
                'id' => 'first_name',
                'class' => 'required form-control',
            );
            $fname_val = !empty($user->first_name) ? $user->first_name : '';
            ?>
            <?php echo form_input($first_name, $fname_val); ?>
        </div>
        <div class="form-group">
            <label class="">Last Name</label>
            <?php
            $last_name = array('name' => 'last_name',
                'id' => 'last_name',
                'class' => 'required form-control',
            );
            $lname_val = !empty($user->last_name) ? $user->last_name : '';
            ?>
            <?php echo form_input($last_name, $lname_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Email Address</label>
            <?php
            $email = array('name' => 'email',
                'id' => 'email',
                'class' => 'required form-control',
//                'readonly' => 'true',
                'style' => 'margin-bottom:15px;'
            );
            if (!empty($user->email)) {
                $email_val = $user->email;
            } else {
                $email_val = '';
            }
            ?>
            <?php echo form_input($email, $email_val); ?>
        </div>
        <div class="form-group">
            <label class="">Password</label>
            <?php
            $pswd_req = !empty($user->password) ? '' : 'required';
            $pswd = array('name' => 'password',
                'id' => 'password',
                'class' => "form-control $pswd_req",
                'style' => 'margin-bottom:15px;',
            );
            echo form_password($pswd);
            ?>
        </div>
        <div class="clearfix"></div>
<!--        <div class="form-group">
            <label class="">Lat</label>
            <?php
//            $lat = array('name' => 'lat',
//                'id' => 'lat',
//                'class' => 'form-control',
//            );
//            $lat_val = !empty($user->lat) ? $user->lat : '';
            ?>
            <?php // echo form_input($lat, $lat_val); ?>
        </div>-->
<!--        <div class="form-group">
            <label class="">Long</label>
            <?php
//            $long = array('name' => 'long',
//                'id' => 'long',
//                'class' => 'form-control',
//            );
//            $long_val = !empty($user->long) ? $user->long : '';
            ?>
            <?php // echo form_input($long, $long_val); ?>
        </div>-->
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Address</label>
            <?php
            $address = array('name' => 'address',
                'id' => 'address',
                'class' => 'form-control',
            );
            $addr_val = !empty($user->address) ? $user->address : '';
            ?>
            <?php echo form_input($address, $addr_val); ?>
        </div>
        <div class="form-group">
            <label class="">City</label>
            <?php
            $city = array('name' => 'city',
                'id' => 'city',
                'class' => 'form-control',
            );
            $city_val = !empty($user->city) ? $user->city : '';
            ?>
            <?php echo form_input($city, $city_val); ?>
        </div>
        <div class="clearfix"></div>
<!--        <div class="form-group">
            <label class="">State</label>
            <?php $this->load->helper('state'); ?>
            <?php
            $state_val = !empty($user->state) ? $user->state : '';
            ?>
            <?php echo state_dropdown('state', $state_val, 'state', 'form-control'); ?>
        </div>-->
        <div class="form-group">
            <label class="">Zip Code</label>
            <?php
            $zipcode = array('name' => 'zipcode',
                'id' => 'zipcode',
                'class' => 'form-control',
                'style' => 'margin-bottom:15px;'
            );
            $zip_val = !empty($user->zipcode) ? $user->zipcode : '';
            ?>
            <?php echo form_input($zipcode, $zip_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Company</label>
            <?php
            $company = array('name' => 'company',
                'id' => 'company',
                'class' => 'form-control',
                'style' => 'margin-bottom:15px;'
            );
            $company_val = !empty($user->company) ? $user->company : '';
            ?>
            <?php echo form_input($company, $company_val); ?>
        </div>
        <div class="form-group">
            <label class="">Company Webste</label>
            <?php
            $website = array('name' => 'website',
                'id' => 'website',
                'class' => 'form-control',
                'style' => 'margin-bottom:15px;'
            );
            $website_val = !empty($user->website) ? $user->website : '';
            ?>
            <?php echo form_input($website, $website_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Company Established on</label>
            <?php
            $estd = array('name' => 'estd',
                'id' => 'estd',
                'class' => 'form-control',
                'style' => 'margin-bottom:15px;'
            );
            $estd_val = !empty($user->estd) ? $user->estd : '';
            ?>
            <?php echo form_input($estd, $estd_val); ?>
        </div>
        <div class="form-group">
            <label class="">Official description</label>
            <?php
            $description = array('name' => 'description',
                'id' => 'description',
                'class' => 'form-control',
                'style' => 'margin-bottom:15px;'
            );
            $description_val = !empty($user->description) ? $user->description : '';
            ?>
            <?php echo form_textarea($description, $description_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Phone</label>
            <?php
            $phone = array('name' => 'phone',
                'id' => 'phone',
                'class' => 'form-control',
            );
            $phone_val = !empty($user->phone) ? $user->phone : '';
            ?>
            <?php echo form_input($phone, $phone_val); ?>
        </div>
        <div class="form-group">
            <label class="">Contact Numbers</label>
            <?php
            $contact_num = array('name' => 'contact_num',
                'id' => 'contact_num',
                'class' => 'form-control',
            );
            $contact_num_val = !empty($user->contact_num) ? $user->contact_num : '';
            ?>
            <?php echo form_input($contact_num, $contact_num_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Category</label>
            <?php
            $prof_cat = array('name' => 'prof_cat_id',
                'id' => 'prof_cat_id',
                'class' => 'form-control',
            );
            $prof_cat_val = !empty($user->prof_cat_id) ? $user->prof_cat_id : '';
            ?>
            <?php echo form_dropdown($prof_cat, $prof_cats, $prof_cat_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Profile Picture</label>
            <?php
            $file = array(
                'name' => 'userfile',
                'id' => 'userfile',
            );
            ?>
            <?php echo form_upload($file); ?>          
        </div>
        <div class="form-group">
            <?php
            if (!empty($user->photo)):
                ?>
                <img style="max-width: 250px;" src="<?php echo base_url() . 'uploads/user_profile/' . $user->photo ?>"/>
                <?php
                $del_attr = array(
                    "onClick" => "return confirm('Are you sure you want to remove?')",
                    "class" => "btn btn-default img-remove"
                );
                echo anchor("prof/remove_picture/$user->photo/" . encode_url($id), "Remove", $del_attr);
            endif;
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="form_btn_wrap">
            <a class="btn btn-default pull-left" href="<?php echo site_url('prof'); ?>">
                Back
            </a>
            <?php echo form_submit(array('class' => 'btn btn-default save', 'id' => 'save', 'name' => 'edit'), 'Save'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<link href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/css/fileinput.min.css" rel="stylesheet"/>
<script src="<?php echo base_url(); ?>assets/js/fileinput.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#dob').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
        jQuery("#userfile").fileinput({
            showUpload: false
        });
        jQuery("#profs-edit").validate({
            rules: {
                email: {
                    required: true,
                    fullEmail: true
                },
                password: {
                    minlength: 8
                },
                zipcode: {
                    number: true,
                    maxlength: 6
                },
//                phone: {
//                    number: true,
//                    maxlength: 10
//                }
            },
            messages: {
                zipcode: {number: "Please enter a valid zipcode."},
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "ret_date") {
                    error.appendTo("#ret-fg");
                } else {
                    error.insertAfter(element);
                }
            }
        });
    });
</script>