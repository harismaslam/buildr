<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <title>Home - Buildr</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    </head>
    <body class="home">
        <div id="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">
                        <img id="logo" class="img-responsive pull-left" src="<?php echo base_url(); ?>assets/images/web/logo.png"/>
                        <span class="pull-left" style="display: inline-block; margin: 59px 0px 0px 5px;">
                            Share, Collaborate and be Social. Read reviews, See projects and get Connected
                        </span>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <img src="<?php echo base_url(); ?>assets/images/web/location-img.jpg"/>
                        <select id="location" class="text-left" style="">
                            <option>Kochi</option>
                            <option>Bangalore</option>
                        </select>
                        <!--<p id="location" class="text-right"></p>-->
                    </div>
                </div>
            </div>
        </div>
        <div id="banner">
            <div id="search-form">
                <h1>SOCIALIZING REALTY</h1>
                <?php echo form_open("home", array('class' => 'form commonform', 'id' => 'home-search', 'role' => 'form')); ?>
                <div class="form-group">
                    <div class="input-group">
                        <?php
                        $cat = array('name' => 'cat',
                            'id' => 'cat',
                            'class' => 'required form-control',
                        );
                        $cat_val = !empty($cat_val) ? $cat_val : set_value('cat');
                        $cat_optn = array();
                        $cat_optn[0] = 'Select the category, you want to look at !';
                        if (!empty($prof_cats)) {
                            foreach ($prof_cats as $val) {
                                $cat_optn[$val->id] = $val->name;
                            }
                        }
                        ?>
                        <?php echo form_dropdown($cat, $cat_optn, $cat_val); ?>
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div id="prof-group">
            <?php if (!empty($profs)): ?>
                <?php
                $i = 1;
                $this->load->helper('text');
                ?>
                <?php foreach ($profs as $prof_val): ?>
                    <div class="prof-group-item">
                        <div class="container">
                            <div class="row">
                                <div class="top-sec">
                                    <div class="col-sm-7">
                                        Ranked # <?php echo $i; ?> <?php echo $prof_val->name; ?> near you | Rating <?php echo ($prof_val->avg_rat != 0) ? $prof_val->avg_rat : 0; ?> / 5
                                    </div>
                                    <div class="col-sm-5">
                                        <span>
                                            <?php if ($prof_val->tot_rw != 0): ?>
                                                Showing <?php echo ($prof_val->tot_rw < 2) ? $prof_val->tot_rw : 2; ?> of <?php echo $prof_val->tot_rw; ?>
                                            <?php else: ?> No <?php endif; ?>
                                            reviews
                                        </span>
                                        <span class="pull-right">
                                            Add reviews <a class="btn btn-default add-review">+</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="main-sec">
                                    <div class="col-sm-5">
                                        <img class="img-responsive main-img" src="<?php echo $prof_val->photo; ?>"/>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php if (!empty($prof_val->proj_imgs)): ?>
                                            <?php foreach ($prof_val->proj_imgs as $img_val): ?>
                                                <img class="img-responsive proj-img" src="<?php echo base_url(); ?>/uploads/aprt_img/<?php echo $img_val->apt_gall_img; ?>"/>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <img class="img-responsive proj-img" src="<?php echo base_url(); ?>assets/images/web/project1.jpg"/>
                                            <img class="img-responsive proj-img" src="<?php echo base_url(); ?>assets/images/web/project2.jpg"/>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-5">
                                        <?php if (!empty($prof_val->review)): ?>
                                            <?php $r = 0; ?>
                                            <?php foreach ($prof_val->review as $rw_val): ?>
                                                <?php
                                                if ($r > 1) {
                                                    break;
                                                }
                                                ?>
                                                <div class="review-item" style="">
                                                    <img class="img-responsive reviewer-img" src="<?php echo $rw_val->reviewer_photo; ?>"/>
                                                    <p>
                                                        <span>
                                                            <?php echo $rw_val->reviewer_name; ?> | 
                                                            Rating: <span class="star">
                                                                <?php
                                                                print_stars($rw_val->rating);
                                                                ?>
                                                            </span> <?php echo $rw_val->rating; ?> / 5
                                                        </span><br/>
                                                        <?php echo short_text($rw_val->review_msg, 95) . '...'; ?>
                                                    </p>
                                                </div>
                                                <div class="clearfix"></div>
                                                <?php $r++; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="bott-sec">
                                    <div class="col-sm-10">
                                        <?php echo $prof_val->first_name . ' ' . $prof_val->last_name; ?><?php echo (!empty($prof_val->first_name) && !empty($prof_val->last_name) && !empty($prof_val->company)) ? ', ' : ''; ?>
                                        <?php echo $prof_val->company; ?><br/>
                                        <?php if (!empty($prof_val->address) && !empty($prof_val->city)): ?>
                                            <?php echo $prof_val->address; ?><?php echo (!empty($prof_val->address) && !empty($prof_val->city)) ? ', ' : ''; ?><?php echo $prof_val->city; ?> Area 
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <a class="btn btn-default follow pull-right">Follow</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <footer class="text-center">
            <a class="btn-section">next section</a>
            <h2 class="text-center">Contact Buildr</h2>
            <p><a href="mailto:contact@buildr.in">contact@buildr.in</a></p>
            <ul class="social-list">
                <li><a href="https://twitter.com/Workup_health">Twitter</a></li>
                <li class="facebook"><a href="https://www.facebook.com/workuphealth">Facebook</a></li>
                <li class="instagram"><a href="http://instagram.com/workup_health">Instagram</a></li>
                <li class="linkedin"><a href="https://www.linkedin.com/company/workup-inc">LinkedIn</a></li>
            </ul>
            <span class="text-box">For inquiries <br>contact <a href="mailto:support@buildr.in">support@buildr.in</a></span>
            <p class="copy text-center">
                © Buildr, 2016 | All rights reserved
            </p>
        </footer>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>
