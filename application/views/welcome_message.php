<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html  lang="en">
    <!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>buildr</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css">
        <link href="<?php echo base_url(); ?>assets/front/css/builder.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="full">
        <div class="fixed">
            <div class="container">
                <div class="row first">
                    <div class="imgcol col-md-2 ">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/front/images/logo.jpg" class="img-responsive"></a>
                    </div>
                    <div class="textcol col-md-5">
                        <p class="fsttext"></p>
                    </div>
                    <div class="icon col-md-1 col-md-offset-3">
                        <i class="fa fa-paper-plane-o plne fa-lg" aria-hidden="true"></i>
                    </div>
                    <div class="bnglrcol col-md-1">
                        <p class="bnglr">Bangalore</p>
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-inverse iteamlist">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>

                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav iteams">
                        <li><a href="<?php echo site_url('about'); ?>"id="fist">About project & US</a></li>
                        <li><a href="tab2" data-toggle="tab">|</a></li>
                        <li ><a href="<?php echo site_url('about'); ?>#firstd" id="second">Contact & Get access</a></li>
                        <li><a href="tab4" data-toggle="tab">|</a></li>
                        <li><a href="<?php echo site_url('about'); ?>#seconddes"id="third">FAQs & Concerns</a></li>
                    </ul>
                </div>

            </nav> 

        </div>
        <div class="container-fluid">
            <div class="row banner">

                <h1 class="fstheading">SOCIALIZING REALITY</h1>
                <h2 class="secondheading">Builder is a beta stage social realty project that gives you insights to your nearest Service Provider</h2>
                <div class="dropdown">
                    <div class="left-inner-addon ">
                        <i class="fa fa-search fa-lg"></i>

                        <input type="text"
                               class="form-control fst dropdown-toggle" 
                               placeholder="Select the category,You want to look at !" data-toggle="dropdown" />
                        <ul class="dropdown-menu">
                            <?php foreach ($prof_cats as $val) : ?>
                                <li><a href="<?php echo site_url("service_provider/view/$val->id") ?>"><?php echo $val->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>

                    </div>
                </div>




            </div>
        </div>
        <div class="container-fluid thirdcontainer">
            <div class="row third">
                <p class="jstfory">Just for you<br>
                    <span class="ins">Inspiring crafts from Service Providers</span></p>
                <div class="container inner">
                    <div class="imgcol1 col-md-4 ">
                        <a href="<?php echo site_url("service_provider/view/1") ?>">
                            <img src="<?php echo base_url(); ?>assets/front/images/img1.jpg" class="img-responsive img1">
                            <p class="architec">Architects</p>
                        </a>
                    </div>
                    <div class="imhcol2 col-md-4">
                        <a href="<?php echo site_url("service_provider/view/2") ?>">
                            <img src="<?php echo base_url(); ?>assets/front/images/img2.jpg" class="img-responsive img2">
                            <p class="int">Interior Designers</p>
                        </a>
                    </div>
                    <div class="imgcol3 col-md-4">
                        <a href="<?php echo site_url("service_provider/view/3") ?>">
                            <img src="<?php echo base_url(); ?>assets/front/images/img3.jpg" class="img-responsive img3">
                            <p class="modular">Modular Kitchen <br>Specialists</p>
                        </a>
                    </div>
                </div>
            </div>
            <?php if (!empty($prof_cats)): ?>
                <div class="container">
                    <div class="row formcntrl">
                        <div class="row formcntrl">
                            <div class="col-md-4 col-md-offset-4">
                                <ul class="nav nav-tabs">
                                    <li class="dropdown1">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="tabspn">See all categories,that we start with!</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu tabsecond">
                                            <?php foreach ($prof_cats as $val) : ?>
                                                <li><a href="<?php echo site_url("service_provider/view/$val->id") ?>"><?php echo $val->name; ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="container-fluid">
            <div class="row forth" id="video">
                <!--<h2>HOW IT WORK</h2>-->
                <!--<div class="you col-md-5 col-md-offset-3">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/JbJitELGqvY" frameborder="0" allowfullscreen></iframe>
                </div>-->
                <p class="hwitsworks">HOW IT WORKS</p>
                <div class="spn12">How social realty makes your life easier? Lets have a look at Mr.Bob Story!</div>
                <!--<i class="fa fa-play-circle-o play fa-5x" aria-hidden="true"></i>--><img src="<?php echo base_url(); ?>assets/front/images/Play_Button.png" class="img-responsive playyy" id="v"></div>
        </div>

        <div class="container-fluid lst">
            <div class="row sixth">
                <div class="col-md-12">
                    <p class="wnt">Want to get in touch?, We'd love to hear from you.</p>
                    <p class="callus">Call us +9180 4207 5287, support@buildr.in<br>
                        We are available: MON-FRI 11AM-5PM IST, Bangalore, India</p>
                    <p class="abt"><span class="abt"><a href="<?php echo site_url('about'); ?>">ABOUT PROJECT & US</a> </span> <span class="fsti">|</span>  <span class="cnt"><a href="<?php echo site_url('about'); ?>">CONTACT & GET ACCESS </a></span> <span class="scndi">|</span>  <span class="faq"><a href="<?php echo site_url('about'); ?>"> FAQs & CONCERNS</a></span></p>
                    <p class="all"><span class="ftr1">Buildr.in</span> <span class="ftr2">|</span> <span class="ftr3">All rights reserved</span><span class="ftr4"> | </span><span class="ftr5">Renascence Information Service Private Limited - 2016 </span><i class="fa fa-copyright ilst" aria-hidden="true"></i></p>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url(); ?>assets/front/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
        <script   src="https://code.jquery.com/jquery-1.12.3.min.js"   integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="   crossorigin="anonymous"></script>
        <script>
            $('#v').on('click', function () {
                $('#video').html('<iframe src="https://www.youtube.com/embed/JbJitELGqvY?rel=0&autoplay=1&showinfo=0&controls=0" width="100%" height="100%" frameborder="0" allowfullscreen="true">').css('background', 'none');
            });
        </script>
    </body>
</html>