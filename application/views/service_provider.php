<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html  lang="en">
    <!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>buildr | Interior</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css">
        <link href="<?php echo base_url(); ?>assets/front/css/interior.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="full">
        <div class="fixed">
            <div class="container-fluid">
                <div class="row first">
                    <div class="imgcol col-md-2 ">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/front/images/logo.jpg" class="img-responsive"></a>
                    </div>
                    <div class="textcol col-md-7">
                        <div class="dropdown">
                            <div class="left-inner-addon ">
                                <i class="fa fa-search fa-lg"></i>
                                <input type="text"
                                       class="form-control fst dropdown-toggle" 
                                       placeholder="Select the catogory,You want to look at !" data-toggle="dropdown" />
                                <ul class="dropdown-menu listview">
                                    <?php foreach ($prof_cats as $val) : ?>
                                        <li><a href="<?php echo site_url("service_provider/view/$val->id") ?>"><?php echo $val->name; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="icon col-md-1 col-md-offset-1">
                        <i class="fa fa-paper-plane-o plne fa-lg" aria-hidden="true"></i>
                    </div>
                    <div class="bnglrcol col-md-1">
                        <p class="bnglr">Banglore</p>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-inverse iteamlist">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>

                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav iteams">
                        <li><a href="<?php echo site_url('about'); ?>"id="fist"  >About project & US</a></li>
                        <li><a href="tab2" data-toggle="tab">|</a></li>
                        <li><a href="<?php echo site_url('about'); ?>#firstd" id="second">Contact & Get access</a></li>
                        <li><a href="tab4" data-toggle="tab">|</a></li>
                        <li><a href="<?php echo site_url('about'); ?>#seconddes"id="third">FAQs & Concerns</a></li>
                    </ul>

                </div>

            </nav>
        </div>
        <div class=" bg">
            <div class="container">
                <div class="row second">
                    <div class="showingarccol col-md-12">
                        <p class="showing">Showing <?php echo $cat_name; ?> near you</p>
                    </div>
                </div>
            </div>
            <hr class="container-fluid hr1">
            <?php if (!empty($profs)): ?>
                <?php
                $i = 1;
                $this->load->helper('text');
                $cnt = count($profs);
                ?>
                <?php foreach ($profs as $prof_val): ?>
                    <div class="container">

                        <div class="row third">
                            <div class="profileimgcol col-md-5">
                                <p class="rated">Rated # <?php echo $i; ?> <?php echo $cat_name; ?> near you | Rating <?php echo ($prof_val->avg_rat != 0) ? round($prof_val->avg_rat, 2) : 0; ?>/5</p>
                                <a href="<?php echo site_url("service_provider/detail/$prof_val->uid"); ?>">
                                    <?php if (!empty($prof_val->photo)): ?>
                                        <img src="<?php echo $prof_val->photo; ?>" class="img-responsive fstimg cover" height="287">
                                    <?php else: ?>
                                        <img src="<?php echo base_url(); ?>assets/front/images/no-user-list-view.png" class="img-responsive fstimg">
                                    <?php endif; ?>
                                </a>
                                <p class="sandra"><span class="subname">
                                        <?php echo ucfirst($prof_val->first_name) . ' ' . ucfirst($prof_val->last_name); ?><?php echo (!empty($prof_val->first_name) && !empty($prof_val->last_name) && !empty($prof_val->company)) ? ', ' : ''; ?><?php echo $prof_val->company; ?></span><br>
                                    <?php if (!empty($prof_val->address) && !empty($prof_val->city)): ?>
                                            <?php echo $prof_val->address; ?><?php echo (!empty($prof_val->address) && !empty($prof_val->city)) ? ', ' : ''; ?><?php echo $prof_val->city; ?> Area 
                                        <?php endif; ?>
                                </p>
                            </div>
                            <div class="col-md-2 imgsecndcol">
                                <div class="row imgrowone">
                                    <div class="col-md-12">
                                        <a href="<?php echo site_url("service_provider/detail/$prof_val->uid"); ?>">
                                            <?php $proj_gal = $prof_val->proj_imgs[0]; ?>
                                            <?php if (!empty($proj_gal->apt_gall_img)): ?>
                                                <img src="<?php echo base_url() . '/uploads/aprt_img/thumbs/195x125/' . $proj_gal->apt_gall_img; ?>" class="img-responsive scndimg">
                                            <?php else: ?>
                                                <img src="<?php echo base_url(); ?>assets/front/images/hope.jpg" class="img-responsive scndimg cover1" height="140">
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="row imgrowtwo">
                                    <div class="col-md-12">
                                        <a href="<?php echo site_url("service_provider/detail/$prof_val->uid"); ?>">
                                            <?php $proj_gal = $prof_val->proj_imgs[1]; ?>
                                            <?php if (!empty($proj_gal->apt_gall_img)): ?>
                                                <img src="<?php echo base_url() . '/uploads/aprt_img/thumbs/195x125/' . $proj_gal->apt_gall_img; ?>" class="img-responsive thirdimg">
                                            <?php else: ?>
                                                <img src="<?php echo base_url(); ?>assets/front/images/hope.jpg" class="img-responsive thirdimg cover2" height="140">
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="userreviewcol col-md-5">
                                <div class="row sub1">
                                    <div class="ulcol col-md-12">
                                        <ul class="fstlist">
                                            <li class="iteams1"> <?php if ($prof_val->tot_rw != 0): ?>
                                                    Showing <?php echo ($prof_val->tot_rw < 2) ? $prof_val->tot_rw : 2; ?> of <?php echo $prof_val->tot_rw; ?>
                                                <?php else: ?> No <?php endif; ?>
                                                reviews
                                            </li>

                                            <!--<li class="iteams12">Add reviews</li>-->
                                            <!--<li class="iteams1"><i class="fa fa-plus fstfont fa-lg" data-toggle="modal" data-target="#myModal" aria-hidden="true"></i></li>-->
                                        </ul>
                                        <a class="btn btn-default fstdefaultbtn" href="<?php echo site_url('member/add_review/' . $prof_val->uid); ?>">Add review</a>
                                    </div>
                                </div>
                                <?php if (!empty($prof_val->review[0])): ?>
                                    <div class="row sub2">
                                        <a href="<?php echo site_url("service_provider/detail/$prof_val->uid"); ?>">
                                            <?php $rw_val = $prof_val->review[0]; ?>
                                            <div class="imgsubcol col-md-3">
                                                <?php if (!empty($rw_val->reviewer_photo)): ?>
                                                    <img src="<?php echo $rw_val->reviewer_photo; ?>" class="img-responsive cover3" height="114">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url(); ?>uploads/user_profile/user-avatar.png" class="img-responsive">
                                                <?php endif; ?>
                                            </div>
                                            <div class="subcntntcol col-md-9">
                                                <div class="rating" style="color: #333;"><?php echo ucfirst($rw_val->reviewer_name); ?> | Rating, <span class="ratingsymbol"><?php print_stars($rw_val->rating); ?> </span><?php echo $rw_val->rating; ?>/5</div>
                                                <p style="margin-right: 15px; color: #333;" class="fstctnt"><?php echo short_text($rw_val->review_msg, 150) . '...'; ?></p>
                                            </div>
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <div class="row sub2">
                                        <a href="<?php echo site_url("service_provider/detail/$prof_val->uid"); ?>">
                                            <div class="imgsubcol col-md-3">
                                                <img src="<?php echo base_url(); ?>uploads/user_profile/user-avatar.png" class="img-responsive">
                                            </div>
                                            <div class="subcntntcol col-md-9">
                                                <img src="<?php echo base_url(); ?>assets/front/images/no-review-list-view.png" class="img-responsive">
                                            </div>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($prof_val->review[1])): ?>
                                    <?php $rw_val = $prof_val->review[1]; ?>
                                    <div class="row sub3">
                                        <a href="<?php echo site_url("service_provider/detail/$prof_val->uid"); ?>">
                                            <div class="imgsubcol col-md-3">
                                                <?php if (!empty($rw_val->reviewer_photo)): ?>
                                                    <img src="<?php echo $rw_val->reviewer_photo; ?>" class="img-responsive cover4" height="114">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url(); ?>uploads/user_profile/user-avatar.png" class="img-responsive">
                                                <?php endif; ?>
                                            </div>
                                            <div class="subcntntcol col-md-9">
                                                <div class="rating" style="color: #333;"><?php echo ucfirst($rw_val->reviewer_name); ?> | Rating, <span class="ratingsymbol"><?php print_stars($rw_val->rating); ?> </span><?php echo $rw_val->rating; ?>/5</div>
                                                <p style="margin-right: 15px; color: #333;" class="fstctnt"><?php echo short_text($rw_val->review_msg, 150) . '...'; ?></p>
                                            </div>
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <div class="row sub3">
                                        <a href="<?php echo site_url("service_provider/detail/$prof_val->uid"); ?>">
                                            <div class="imgsubcol col-md-3">
                                                <img src="<?php echo base_url(); ?>uploads/user_profile/user-avatar.png" class="img-responsive">
                                            </div>
                                            <div class="subcntntcol col-md-9">
                                                <img src="<?php echo base_url(); ?>assets/front/images/no-review-list-view.png" class="img-responsive">
                                            </div>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                    <?php if (($cnt + 1) != $i): ?>
                        <hr class="container-fluid hr1">
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="container-fluid lst">
            <div class="row sixth">
                <div class="col-md-12">
                    <p class="wnt">want to get in touch?, we'd love to hear from you</p>
                    <p class="callus">Call us +9180 4207 5287,support@buildr.in<br>
                        We are available:MON-FRI 11AM-5PA IST,Bangalore India</p>
                    <p class="abt"><span class="abt"><a href="<?php echo site_url('about'); ?>">ABOUT PROJECT & US</a> </span> <span class="fsti">|</span>  <span class="cnt"><a href="<?php echo site_url('about'); ?>">CONTACT & GET ACCESS </a></span> <span class="scndi">|</span>  <span class="faq"><a href="<?php echo site_url('about'); ?>"> FAQs & CONCERNS</a></span></p>
                    <p class="all"><span class="ftr1">Buildr.in</span> <span class="ftr2">|</span> <span class="ftr3">All rights reserved</span><span class="ftr4"> | </span><span class="ftr5">Renenscence Information Service Private Limited - 2016 </span><i class="fa fa-copyright ilst" aria-hidden="true"></i></p>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url(); ?>assets/front/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
    </body>
</html>