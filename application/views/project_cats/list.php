<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Apartment Categories</h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php if (!empty($cats)): ?>
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="cats-list">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th class="nosort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($cats as $cat): ?>
                            <tr>
                                <td style="vertical-align: middle">
                                    <?php echo $cat->name; ?>
                                </td>
                                <td>
                                    <img style="max-width: 150px;" class="img-responsive" src="<?php echo base_url(); ?>/uploads/project_cats/<?php echo $cat->image; ?>"/>
                                </td>
                                <td style="vertical-align: middle">
                                    <?php
                                    $edit_attr = array(
                                        "title" => "Edit"
                                    );
                                    echo anchor("project_cats/edit/$cat->id", "<i class='fa fa-edit'></i>", $edit_attr);
                                    ?>
                                    <?php
                                    $del_attr = array(
                                        "onClick" => "return confirm('Are you sure you want to delete?')",
                                        "title" => "Delete"
                                    );
                                    echo anchor("project_cats/delete/$cat->id", "<i class='fa fa-trash'></i>", $del_attr);
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<!-- DataTables CSS -->
<link href="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="<?php echo base_url(); ?>assets/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    jQuery(document).ready(function () {
        jQuery('#cats-list').DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>