<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Projects Category</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php echo form_open_multipart("project_cats/save", array('class' => 'form commonform', 'id' => 'proj-cat-add', 'role' => 'form')); ?>
        <div class="form-group">
            <label class="">Category Name</label>
            <?php
            $name = array('name' => 'name',
                'id' => 'name',
                'class' => 'required form-control',
            );
            $name_val = !empty($user->name) ? $user->name : '';
            ?>
            <?php echo form_input($name, $name_val); ?>
        </div>
        <div class="form-group">
            <label class="">Category Image</label>
            <?php
            $file = array(
                'name' => 'userfile',
                'id' => 'userfile',
                'class' => 'required',
            );
            ?>
            <?php echo form_upload($file); ?>          
        </div>
        <div class="form_btn_wrap">
            <a class="btn btn-default pull-left" href="<?php echo site_url('project_cats'); ?>">
                Back
            </a>
            <?php echo form_submit(array('class' => 'btn btn-default save', 'id' => 'save', 'name' => 'add'), 'Save'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<link href="<?php echo base_url(); ?>assets/css/fileinput.min.css" rel="stylesheet"/>
<script src="<?php echo base_url(); ?>assets/js/fileinput.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#userfile").fileinput({
            showUpload: false
        });
    });
</script>