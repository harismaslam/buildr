<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Idea</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php echo form_open_multipart("idea/save", array('class' => 'form commonform', 'id' => 'project-add', 'role' => 'form')); ?>
        <div class="form-group">
            <label class="">Title</label>
            <?php
            $title = array('name' => 'title',
                'id' => 'title',
                'class' => 'required form-control',
            );
            $title_val = !empty($idea->title) ? $idea->title : '';
            ?>
            <?php echo form_input($title, $title_val); ?>
        </div>
        <div class="form-group">
            <label class="">Content</label>
            <?php
            $content = array('name' => 'content',
                'id' => 'content',
                'class' => 'form-control',
            );
            $content_val = !empty($idea->content) ? $idea->content : '';
            ?>
            <?php echo form_textarea($content, $content_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Professional</label>
            <?php
            $apt_designer = array('name' => 'apt_designer',
                'id' => 'apt_designer',
                'class' => "form-control typeahead required",
                'style' => 'margin-bottom:15px;',
            );
            echo form_input($apt_designer);
            $des_hid_val = !empty($idea->apt_designer_id) ? $idea->apt_designer_id : '';
            ?>
            <?php echo form_hidden('apt_designer_id', $des_hid_val); ?>
        </div>
        <div class="form-group">
            <label class="">Category</label>
            <?php
            $proj_cat = array('name' => 'proj_cat_id',
                'id' => 'proj_cat_id',
                'class' => 'form-control',
            );
            $proj_cat_val = !empty($idea->proj_cat_id) ? $idea->proj_cat_id : '';
            ?>
            <?php echo form_dropdown($proj_cat, $proj_cats, $proj_cat_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Tags</label>
            <?php
            $tags = array('name' => 'tags',
                'id' => 'tags',
                'class' => 'form-control',
            );
            $tags_val = !empty($idea->tags) ? $idea->tags : '';
            ?>
            <?php echo form_input($tags, $tags_val); ?>
        </div>
        <div class="form_btn_wrap">
            <?php echo form_submit(array('class' => 'btn btn-default save', 'id' => 'save', 'name' => 'add'), 'Save'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<?php
$dsgn_optn = array();
if (!empty($designers)) {
    foreach ($designers as $user) {
        $arr = array();
        $arr['usrId'] = $user->uid;
        $arr['usrName'] = $user->first_name . ' ' . $user->last_name;
        $dsgn_optn[] = $arr;
    }
}
?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap3-typeahead.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#apt_designer.typeahead').typeahead({
            source: function (query, process) {
                states = [];
                map = {};
                var data = <?php echo json_encode($dsgn_optn); ?>;
                $.each(data, function (i, state) {
                    map[state.usrName] = state;
                    states.push(state.usrName);
                });
                process(states);
            },
            updater: function (item) {
                selectedCus = map[item].usrId;
                $('input[name=apt_designer_id]').val(selectedCus);
                return item;
            }
        });

    });
</script>