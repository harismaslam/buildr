<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Ideas</h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php if (!empty($ideas)): ?>
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="cats-list">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Content</th>
                            <th>User</th>
                            <th>Category</th>
                            <th>Tags</th>
                            <th class="nosort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($ideas as $val): ?>
                            <tr>
                                <td style="vertical-align: middle">
                                    <?php echo $val->title; ?>
                                </td>
                                <td style="vertical-align: middle">
                                    <?php echo substr($val->content, 0, 60) ; ?>
                                </td>
                                <td style="vertical-align: middle">
                                    <?php echo $val->user_name; ?>
                                </td>
                                <td style="vertical-align: middle">
                                    <?php echo $val->cat_name; ?>
                                </td>
                                <td style="vertical-align: middle">
                                    <?php echo $val->tags; ?>
                                </td>
                                <td style="vertical-align: middle">
                                    <?php
                                    $edit_attr = array(
                                        "title" => "Edit"
                                    );
                                    echo anchor("idea/edit/$val->iid", "<i class='fa fa-edit'></i>", $edit_attr);
                                    ?>
                                    <?php
                                    $del_attr = array(
                                        "onClick" => "return confirm('Are you sure you want to delete?')",
                                        "title" => "Delete"
                                    );
                                    echo anchor("idea/delete/$val->iid", "<i class='fa fa-trash'></i>", $del_attr);
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<!-- DataTables CSS -->
<link href="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="<?php echo base_url(); ?>assets/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    jQuery(document).ready(function () {
        jQuery('#cats-list').DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>