<!DOCTYPE html>
<html  lang="en">
    <!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>buildr</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css">

        <link href="<?php echo base_url(); ?>assets/front/css/opt.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="full">

        <div class="container-fluid">
            <div class="row first">
                <div class="icon col-md-1 col-md-offset-10 ">
                    <i class="fa fa-paper-plane-o plne fa-lg" aria-hidden="true"></i>
                </div>
                <div class="bnglrcol col-md-1">
                    <p class="bnglr">Bangalore</p>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-inverse iteamlist">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>

            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav iteams">
                    <li><a href="<?php echo site_url('about'); ?>"id="fist">About project & US</a></li>
                        <li><a href="tab2" data-toggle="tab">|</a></li>
                        <li ><a href="<?php echo site_url('about'); ?>#firstd" id="second">Contact & Get access</a></li>
                        <li><a href="tab4" data-toggle="tab">|</a></li>
                        <li><a href="<?php echo site_url('about'); ?>#seconddes"id="third">FAQs & Concerns</a></li>
                </ul>

            </div>

        </nav> 



        <div class="container-fluid">
            <div class="secondtop row">
                <div class="imgcol col-md-2 col-md-offset-5">
                    <img src="<?php echo base_url(); ?>assets/front/images/logo.png" class="img-responsive">
                </div>    
            </div>
            <div class="row third" >
                <div class="col-md-6 col-md-offset-3">
                    <p class="jstadd">Sign in for your builder account</p>
                    <?php echo form_open("member/otp/", array('class' => 'form commonform', 'id' => 'member-otp', 'role' => 'form')); ?>
                    <?php
                    $message = $this->session->flashdata('message');
                    $msg_type = $this->session->flashdata('msg_type');
                    ?>
                    <?php if (isset($message)): ?>
                        <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                            <?php echo $message; ?>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <?php
                        $otp = array('name' => 'otp',
                            'id' => 'otp',
                            'class' => 'required form-control brd number',
                            'placeholder' => 'Enter the Otp(One time password) received on your mobile phone'
                        );
                        $otp_val = !empty($user->otp) ? $user->otp : set_value('otp');
                        ?>
                        <?php echo form_input($otp, $otp_val); ?>
                    </div>
                    <div class="submitlast">
                        <!--<a  href="#">sign in ></a>-->
                        <?php echo form_submit(array('class' => 'btn btn-default save', 'id' => 'otp', 'name' => 'signin'), 'SIGN IN'); ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script   src="https://code.jquery.com/jquery-1.12.3.min.js"   integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="   crossorigin="anonymous"></script>
    </body>
</html>