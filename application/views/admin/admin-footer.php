<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<!--<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/script.js" type="text/javascript"></script>
</body>

</html>