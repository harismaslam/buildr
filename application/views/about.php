<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html  lang="en">
    <!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>builder|about</title>




        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css">
        <link href="<?php echo base_url(); ?>assets/front/css/about.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="full">
        <div class="fixed">
            <div class="container">
                <div class="row first">
                    <div class="imgcol col-md-2 ">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/front/images/logo.jpg" class="img-responsive"></a>
                    </div>
                    <div class="textcol col-md-5">
                        <!--<div class="left-inner-addon ">
                           <i class="fa fa-search fa-lg"></i>
                           <input type="text"
                                  class="form-control fst" 
                                  placeholder="Select the service provider category !" />
                       </div>-->
                    </div>
                    <div class="icon col-md-1 col-md-offset-3">
                        <i class="fa fa-paper-plane-o plne fa-lg" aria-hidden="true"></i>
                    </div>
                    <div class="bnglrcol col-md-1">
                        <p class="bnglr">Bangalore</p>
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-inverse iteamlist">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>

                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav iteams">
                        <li><a  id="fist"  >About Project & Us</a></li>
                        <li><a href="tab2" data-toggle="tab">|</a></li>
                        <li ><a id="second">Contact & Get Access</a></li>
                        <li><a href="tab4" data-toggle="tab">|</a></li>
                        <li><a id="third">FAQs & Concerns</a></li>

                    </ul>

                </div>

            </nav>
        </div>
        <div class="tab-content" id="tabs">
            <div class="container-fluid containfluid">
                <div class="container second">

                    <div class="row second">
                        <div class="abtcol col-md-9 " >
                            <p class="apus tab-pane" id="abtpjct">About Project & Us</p>
                            <p class="fstcntnt"  >Buildr was founded in November, 2015 in Trivandrum, India and established its presence in Bangalore, India. Buildr is a beta stage social realty project which gives far insights of services providers. It's a social marketplace where end user, home owners, service providers and suppliers can seemlessly communicate, collaborate and share with each other. Builder is the easiest way to found the top service providers which suits our time, budget and ease of access. You can now see your nearest realty space, all at your fingertips-online or from a mobile device.</p>
                            <img src="<?php echo base_url(); ?>assets/front/images/img06.jpg" class="img-responsive fstimg" >
                        </div>
                    </div>
                    <div class="sub"id="firstd"></div>
                    <div class="row third" >
                        <div class="cntctcol col-md-9">
                            <p  class="cntct tab-pane" >Contact and Get Access</p>
                            <p class="jst">Just fill up the form and we will contact back with in a short time, you can also directly mail us at support@buildr.in</p>
                            <div  class="form-group">


                                <input type="text" class="form-control brd" id="name" placeholder="Your Name">

                            </div>
                            <div class="form-group">


                                <input type="text" class="form-control brd" id="name" placeholder="Your Mobile Number">

                            </div>
                            <div class="form-group">


                                <input type="text" class="form-control brd" id="name" placeholder="Your Email Address">

                            </div>
                        </div>
                    </div>
                    <div class="row forth " >
                        <div class="faqcol col-md-9 ">
                            <p id="seconddes"class="faq ">FAQs and Concerns</p>
                            <p class="brief" >You can read our brief FAQs for your doubts and clarifications. If you have more concerns regarding our services please directly<br>
                                contact us at support@buildr.in</p>
                            <ul class="linked">
                                <li class="items"><a></a><span id="vn-click" style="cursor:pointer"> What is there for the service providers?</span><p id="vn-info" style="display:none;" >Ans:Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p></li>

                                <li class="items"><a></a><span id="vn-click1" style="cursor:pointer"> What is there for the customers?</span><p id="vn-info1" style="display:none;" >Ans:Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p></li>

                                <li class="items"><a ></a><span id="vn-click2" style="cursor:pointer">Is social commerce involved?</span><p id="vn-info2" style="display:none;" >Ans:Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p></li>

                                <li class="items"><a ></a><span id="vn-click3" style="cursor:pointer">Is the location mapped?</span><p id="vn-info3" style="display:none;" >Ans:Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p></li>

                                <li class="items"><a ></a><span id="vn-click4" style="cursor:pointer">What about privacy and security?</span></li><p id="vn-info4" style="display:none;" >Ans:Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid lst">
                <div class="row sixth">
                    <div class="col-md-12">
                        <p class="wnt">Want to get in touch?, We'd love to hear from you.</p>
                        <p class="callus">Call us +9180 4207 5287,support@buildr.in<br>
                            We are available: MON-FRI 11AM-5PM IST, Bangalore, India</p>
                        <p class="abt"><span class="abt"><a href="#">ABOUT PROJECT & US</a> </span> <span class="fsti">|</span>  <span class="cnt"><a href="#">CONTACT & GET ACCESS </a></span> <span class="scndi">|</span>  <span class="faqand"><a href="#"> FAQs & CONCERNS</a></p></span>
                        <p class="all"><span class="ftr1">Buildr.in</span> <span class="ftr2">|</span> <span class="ftr3">All rights reserved</span><span class="ftr4"> | </span><span class="ftr5">Renascence Information Service Private Limited - 2016 </span><i class="fa fa-copyright ilst" aria-hidden="true"></i></p>
                    </div>
                </div>
            </div>
            <script src="<?php echo base_url(); ?>assets/front/js/jquery.js"></script>
            <script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
            <script>



                $("#vn-click").click(function () {
                    $("#vn-info").slideToggle("slow");
                });

                $("#vn-click1").click(function () {
                    $("#vn-info1").slideToggle("slow");
                });

                $("#vn-click2").click(function () {
                    $("#vn-info2").slideToggle("slow");
                });

                $("#vn-click3").click(function () {
                    $("#vn-info3").slideToggle("slow");
                });

                $("#vn-click4").click(function () {
                    $("#vn-info4").slideToggle("slow");
                });

                $("#second").click(function () {
                    $('html, body').animate({
                        scrollTop: $("#firstd").offset().top
                    }, 2000);
                });

                $("#third").click(function () {
                    $('html, body').animate({
                        scrollTop: $("#seconddes").offset().top
                    }, 2000);
                });

            </script>

    </body>
</html>