<!DOCTYPE html>
<html  lang="en">
    <!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>buildr | service provider</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front/css/jquery.fancybox.css" />

        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css">
        <link href="<?php echo base_url(); ?>assets/front/css/interiordesigners.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="full">
        <div class="fixed">
            <div class="container-fluid">
                <div class="row first">
                    <div class="imgcol col-md-2 ">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/front/images/logo.jpg " class="img-responsive"></a>
                    </div>
                    <div class="textcol col-md-7">
                        <div class="dropdown">
                            <div class="left-inner-addon ">
                                <i class="fa fa-search fa-lg"></i>
                                <input type="text"
                                       class="form-control fst dropdown-toggle" 
                                       placeholder="Select the catogory,You want to look at !" data-toggle="dropdown" />
                                <ul class="dropdown-menu detailedlistview">
                                    <?php foreach ($prof_cats as $val) : ?>
                                        <li><a href="<?php echo site_url("service_provider/view/$val->id") ?>"><?php echo $val->name; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="icon col-md-1 col-md-offset-1">
                        <i class="fa fa-paper-plane-o plne fa-lg" aria-hidden="true"></i>
                    </div>
                    <div class="bnglrcol col-md-1">
                        <p class="bnglr">Banglore</p>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-inverse iteamlist">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav iteams">
                        <li><a href="<?php echo site_url('about'); ?>"id="fist">About project & US</a></li>
                        <li><a href="tab2" data-toggle="tab">|</a></li>
                        <li ><a href="<?php echo site_url('about'); ?>#firstd" id="second">Contact & Get access</a></li>
                        <li><a href="tab4" data-toggle="tab">|</a></li>
                        <li><a href="<?php echo site_url('about'); ?>#seconddes"id="third">FAQs & Concerns</a></li>
                    </ul>

                </div>
            </nav>
        </div>
        <div class="containfluid">
            <div class="container">
                <div class="row second">
                    <?php $prof_val = $prof_det; ?>
                    <?php $this->load->helper('text'); ?>
                    <h3>
                        <?php echo ucfirst($prof_val->first_name) . ' ' . ucfirst($prof_val->last_name); ?><?php echo (!empty($prof_val->first_name) && !empty($prof_val->last_name) && !empty($prof_val->company)) ? ', ' : ''; ?><?php echo $prof_val->company; ?>
                    </h3>
                    <?php
                    $message = $this->session->flashdata('message');
                    $msg_type = $this->session->flashdata('msg_type');
                    ?>
                    <?php if (isset($message)): ?>
                        <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                            <?php echo $message; ?>
                        </div>
                    <?php endif; ?>
                    <h7>
                        <?php if (!empty($prof_val->address) && !empty($prof_val->city)): ?>
                            <?php echo $prof_val->city; ?> Area 
                        <?php endif; ?>
                    </h7>
                   <!-- <img src="<?php echo base_url(); ?>assets/front/images/ddd.png" class="img-responsive fstimg">-->
                    <!--<button class="btn1">Follow</button>-->
                    <a class="btn btn-default btn1" href="<?php echo site_url('member/add_review/' . $prof_val->uid); ?>">Add review</a>
                </div>
            </div>
            <hr class="hr1">
            <div class="container containerfst">
                <div class="row">
                    <div class="detailedimgcol col-md-5">
                        <?php if (!empty($prof_val->photo)): ?>
                            <img src="<?php echo $prof_val->photo; ?>" class="img-responsive scndimg cover"height="287">
                        <?php else: ?>
                            <img src="<?php echo base_url(); ?>assets/front/images/img.jpg" class="img-responsive scndimg " >
                        <?php endif; ?>
                    </div>
                    <div class="detailscol col-md-7">
                        <div class="row sub2">
                            <p class="official"><span class="spn1">Official description</span><br>
                                <?php echo short_text($prof_val->description, 240) . ' ...'; ?>
                            </p>
                            <h7 class="see">See full description</h7>
                        </div>
                        <div class="row sub3">
                            <div class="col-md-7">
                                <p class="Address"><span class="spn2">Address and location</span><br>
                                    <?php echo $prof_val->address; ?><br/>
                                    <?php echo $prof_val->city; ?><?php echo!empty($prof_val->zipcode) ? ' - ' : '' ?><?php echo $prof_val->zipcode; ?>
                                </p>
                                <p class="rating">Rating:<span class="spn3"> <?php print_stars($prof_val->avg_rat); ?>  </span><?php echo round($prof_val->avg_rat, 2); ?>/5</p>
                                <p class="est">Established: <?php echo $prof_val->estd; ?> | view projects</p>
                            </div>
                            <div class="col-md-3 col-md-offset-2">
                                <iframe src="http://maps.google.com/?q=<?php echo $prof_val->address . ', ' . $prof_val->city; ?>&output=embed" width="100%" height="80%" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="hr1">
            <div class="container containersecond">
                <div class="row">
                    <h6 class="thirdhead">Projects</h6>
                    <?php // if (!empty($prof_val->gall)): ?>
                        <div class='list-group'>
                            <?php if (!empty($prof_val->gall[0])): ?>
                                <?php $gallery = $prof_val->gall[0] ?>
                                <div class="fancyboxfstcol col-md-2">
                                    <a class=" fancybox" rel="ligthbox" href="<?php echo $gallery->apt_gall_orig; ?>">
                                        <img class="img-responsive cover1" height="155" alt="" src="<?php echo $gallery->apt_gall_thumb; ?>" />
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="fancyboxfstcol col-md-2">
                                    <img class="img-responsive cover1" height="155" alt="" src="<?php echo base_url(); ?>assets/front/images/no-project-detail-view.png" />
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($prof_val->gall[1])): ?>
                                <?php $gallery = $prof_val->gall[1] ?>
                                <div class="fancyboxscndcol col-md-2">
                                    <a class=" fancybox" rel="ligthbox" href="<?php echo $gallery->apt_gall_orig; ?>">
                                        <img class="img-responsive cover2" height="155" alt="" src="<?php echo $gallery->apt_gall_thumb; ?>" />
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="fancyboxscndcol col-md-2">
                                    <img class="img-responsive cover1" height="155" alt="" src="<?php echo base_url(); ?>assets/front/images/no-project-detail-view.png" />
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($prof_val->gall[2])): ?>
                                <?php $gallery = $prof_val->gall[2] ?>
                                <div class="fancyboxthrdcol col-md-2">
                                    <a class=" fancybox" rel="ligthbox" href="<?php echo $gallery->apt_gall_orig; ?>">
                                        <img class="img-responsive cover3" height="155" alt="" src="<?php echo $gallery->apt_gall_thumb; ?>" />
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="fancyboxthrdcol col-md-2">
                                    <img class="img-responsive cover1" height="155" alt="" src="<?php echo base_url(); ?>assets/front/images/no-project-detail-view.png" />
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($prof_val->gall[3])): ?>
                                <?php $gallery = $prof_val->gall[3] ?>
                                <div class="fancyboxfrthcol col-md-2">
                                    <a class=" fancybox" rel="ligthbox" href="<?php echo $gallery->apt_gall_orig; ?>">
                                        <img class="img-responsive cover4" height="155" alt="" src="<?php echo $gallery->apt_gall_thumb; ?>" />
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="fancyboxfrthcol col-md-2">
                                    <img class="img-responsive cover1" height="155" alt="" src="<?php echo base_url(); ?>assets/front/images/no-project-detail-view.png" />
                                </div>
                            <?php endif; ?>
                            <?php if (!empty($prof_val->gall[4])): ?>
                                <?php $gallery = $prof_val->gall[4] ?>
                                <div class="fancyboxfvthcol col-md-2">
                                    <a class=" fancybox" rel="ligthbox" href="<?php echo $gallery->apt_gall_orig; ?>">
                                        <img class="img-responsive cover5" height="155" alt="" src="<?php echo $gallery->apt_gall_thumb; ?>" />
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="fancyboxfvthcol col-md-2">
                                    <img class="img-responsive cover1" height="155" alt="" src="<?php echo base_url(); ?>assets/front/images/no-project-detail-view.png" />
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php // else: ?>
<!--                        <div class="col-md-12">
                            No projects listed
                        </div>-->
                    <?php // endif; ?>
                </div>
            </div>
            <hr class="hr1">
            <div class="container">
                <div class="row personrowstart">
                    <p class="review">Review</p>
                </div>
                <?php if (!empty($prof_val->reviews)): ?>
                    <?php $i = 1; ?>
                    <?php foreach ($prof_val->reviews as $review): ?>
                        <div class="persnrow row">
                            <div class="perfstcol col-md-1">
                                <img src="<?php echo $review->reviewer_photo; ?>" class="img-responsive">
                            </div>
                            <div class="perscndcol col-md-11">
                                <div class="individualdetails">
                                    <?php echo ucfirst($review->reviewer_name); ?> | Rating: <span class="ratingsymbol12"><?php print_stars($review->rating); ?> </span><?php echo $review->rating; ?>/5<br>
                                    <div class="details">
                                        <?php echo $review->review_msg; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ((($i == 3) || ($i == $prof_val->tot_rw)) && ($i < 4)): ?>
                            <a href="<?php echo site_url('member/add_review/' . $prof_val->uid); ?>" title="click to add review">
                                <div class="row rounds">
                                    <div class="roundcol col-md-3">
                                        <i class="fa fa-circle-thin fa-lg roundshape" aria-hidden="true"></i>
                                        <i class="fa fa-circle-thin fa-lg roundshape" aria-hidden="true"></i>
                                        <i class="fa fa-circle-thin fa-lg roundshape" aria-hidden="true"></i>
                                        <i class="fa fa-circle-thin fa-lg roundshape" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </a>
                        <?php endif; ?>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                <?php else: ?>
                    <div class="persnrow row">
                        <div class="perfstcol col-md-1">
                            <img src="<?php echo base_url(); ?>uploads/user_profile/user-avatar.png" class="img-responsive">
                        </div>
                        <div class="perscndcol col-md-11">
                            <div class="individualdetails">
                                <img src="<?php echo base_url(); ?>assets/front/images/no-review-list-view.png" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo site_url('member/add_review/' . $prof_val->uid); ?>" title="click to add review">
                        <div class="row rounds">
                            <div class="roundcol col-md-3">
                                <i class="fa fa-circle-thin fa-lg roundshape" aria-hidden="true"></i>
                                <i class="fa fa-circle-thin fa-lg roundshape" aria-hidden="true"></i>
                                <i class="fa fa-circle-thin fa-lg roundshape" aria-hidden="true"></i>
                                <i class="fa fa-circle-thin fa-lg roundshape" aria-hidden="true"></i>
                            </div>
                        </div>
                    </a>
                <?php endif; ?>
            </div>
            <!--<hr class="hr1">-->
            <!--            <div class="container">
                            <div class="row lastone">
                                <p class="help">Help us to improve this page | Own this page</p>
                            </div>
                        </div>-->

        </div>
        <div class="container-fluid lst">
            <div class="row sixth">
                <div class="col-md-12">
                    <p class="wnt">want to get in touch?, we'd love to hear from you</p>
                    <p class="callus">Call us +9180 4207 5287,support@buildr.in<br>
                        We are available:MON-FRI 11AM-5PA IST,Bangalore India</p>
                    <p class="abt"><span class="abt"><a href="<?php echo site_url('about'); ?>">ABOUT PROJECT & US</a> </span> <span class="fsti">|</span>  <span class="cnt"><a href="<?php echo site_url('about'); ?>">CONTACT & GET ACCESS </a></span> <span class="scndi">|</span>  <span class="faq"><a href="<?php echo site_url('about'); ?>"> FAQs & CONCERNS</a></span></p>
                    <p class="all"><span class="ftr1">Buildr.in</span> <span class="ftr2">|</span> <span class="ftr3">All rights reserved</span><span class="ftr4"> | </span><span class="ftr5">Renenscence Information Service Private Limited - 2016 </span><i class="fa fa-copyright ilst" aria-hidden="true"></i></p>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url(); ?>assets/front/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
        <script>
            $(document).ready(function () {
                //FANCYBOX
                //https://github.com/fancyapps/fancyBox
                $(".fancybox").fancybox({
                    openEffect: "none",
                    closeEffect: "none"
                });
            });
        </script>
    </body>
</html>