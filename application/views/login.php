<!DOCTYPE html>
<html  lang="en">
    <!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>buildr</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css">

        <link href="<?php echo base_url(); ?>assets/front/css/login.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/validate.css">
    </head>
    <body class="full">
        <div class="container-fluid">
            <div class="row first">
                <div class="icon col-md-1 col-md-offset-10 ">
                    <i class="fa fa-paper-plane-o plne fa-lg" aria-hidden="true"></i>
                </div>
                <div class="bnglrcol col-md-1">
                    <p class="bnglr">Bangalore</p>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-inverse iteamlist">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>

            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav iteams">
                    <li><a href="<?php echo site_url('about'); ?>"id="fist">About project & US</a></li>
                    <li><a href="tab2" data-toggle="tab">|</a></li>
                    <li ><a href="<?php echo site_url('about'); ?>#firstd" id="second">Contact & Get access</a></li>
                    <li><a href="tab4" data-toggle="tab">|</a></li>
                    <li><a href="<?php echo site_url('about'); ?>#seconddes"id="third">FAQs & Concerns</a></li>
                </ul>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="secondtop row">
                <div class="imgcol col-md-2 col-md-offset-5">
                    <a href="<?php echo base_url(); ?>">
                        <img src="<?php echo base_url(); ?>assets/front/images/logo.png" class="img-responsive">
                    </a>
                </div>    
            </div>
            <div class="row third" >
                <div class="col-md-6 col-md-offset-3">
                    <p class="jstadd">Sign in for your buildr account !</p>
                    <?php echo form_open("member/login/", array('class' => 'form commonform', 'id' => 'member-login', 'role' => 'form')); ?>
                    <?php
                    $message = $this->session->flashdata('message');
                    $msg_type = $this->session->flashdata('msg_type');
                    ?>
                    <?php if (isset($message)): ?>
                        <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                            <?php echo $message; ?>
                        </div>
                    <?php endif; ?>
                    <div  class="form-group">
                        <?php
                        $first_name = array('name' => 'first_name',
                            'id' => 'first_name',
                            'class' => 'required form-control brd',
                            'placeholder' => 'Enter your name'
                        );
                        $fname_val = !empty($user->first_name) ? $user->first_name : set_value('first_name');
                        ?>
                        <?php echo form_input($first_name, $fname_val); ?>
                        <!--<input type="text" class="form-control " id="name" placeholder="Enter your name">-->
                    </div>
                    <div class="form-group">
                        <?php
                        $phone = array('name' => 'phone',
                            'id' => 'phone',
                            'class' => 'required form-control brd number',
                            'placeholder' => 'Enter your mobile number'
                        );
                        $phone_val = !empty($user->phone) ? $user->phone : set_value('phone');
                        ?>
                        <?php echo form_input($phone, $phone_val); ?>
                        <!--<input type="text" class="form-control brd" id="name" placeholder="Enter your mobile number">-->
                    </div>
                    <div class="add">
                        <input type="checkbox" name="terms" class="required"> 
                        I agree to the buildr 
                        <a href="#">Terms of Service & Privacy Policy</a>
                    </div>
                    <p class="add1">
                        <!--<a href="#">NEXT ></a>-->
                        <?php echo form_submit(array('class' => 'btn btn-default save', 'id' => 'login', 'name' => 'login'), 'NEXT'); ?>
                    </p>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url(); ?>assets/front/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.3.min.js"   integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="   crossorigin="anonymous"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/script.js" type="text/javascript"></script>
    </body>
</html>