<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add User</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php echo form_open_multipart("user/save", array('class' => 'form commonform', 'id' => 'user-add', 'role' => 'form')); ?>
        <div class="form-group">
            <label class="">First Name</label>
            <?php
            $first_name = array('name' => 'first_name',
                'id' => 'first_name',
                'class' => 'required form-control',
            );
            $fname_val = !empty($user->first_name) ? $user->first_name : '';
            ?>
            <?php echo form_input($first_name, $fname_val); ?>
        </div>
        <div class="form-group">
            <label class="">Last Name</label>
            <?php
            $last_name = array('name' => 'last_name',
                'id' => 'last_name',
                'class' => 'required form-control',
            );
            $lname_val = !empty($user->last_name) ? $user->last_name : '';
            ?>
            <?php echo form_input($last_name, $lname_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Email Address</label>
            <?php
            $email = array('name' => 'email',
                'id' => 'email',
                'class' => 'required form-control',
                'style' => 'margin-bottom:15px;'
            );
            $email_val = !empty($user->email) ? $user->email : '';
            ?>
            <?php echo form_input($email, $email_val); ?>
        </div>
        <div class="form-group">
            <label class="">Password</label>
            <?php
            $pswd_req = !empty($user->password) ? '' : 'required';
            $pswd = array('name' => 'password',
                'id' => 'password',
                'class' => "form-control $pswd_req",
                'style' => 'margin-bottom:15px;',
            );
            echo form_password($pswd);
            ?>
        </div>
        <div class="clearfix"></div>
        <!--lat and long-->
        <div class="form-group">
            <label class="">Lat</label>
            <?php
            $lat = array('name' => 'lat',
                'id' => 'lat',
                'class' => 'form-control',
            );
            $lat_val = !empty($user->lat) ? $user->lat : '';
            ?>
            <?php echo form_input($lat, $lat_val); ?>
        </div>
        <div class="form-group">
            <label class="">Long</label>
            <?php
            $long = array('name' => 'long',
                'id' => 'long',
                'class' => 'form-control',
            );
            $long_val = !empty($user->long) ? $user->long : '';
            ?>
            <?php echo form_input($long, $long_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Address</label>
            <?php
            $address = array('name' => 'address',
                'id' => 'address',
                'class' => 'form-control',
            );
            $addr_val = !empty($user->address) ? $user->address : '';
            ?>
            <?php echo form_input($address, $addr_val); ?>
        </div>
        <div class="form-group">
            <label class="">City</label>
            <?php
            $city = array('name' => 'city',
                'id' => 'city',
                'class' => 'form-control',
            );
            $city_val = !empty($user->city) ? $user->city : '';
            ?>
            <?php echo form_input($city, $city_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">State</label>
            <?php $this->load->helper('state'); ?>
            <?php
            $state_val = !empty($user->state) ? $user->state : '';
            ?>
            <?php echo state_dropdown('state', $state_val, 'state', 'form-control'); ?>
        </div>
        <div class="form-group">
            <label class="">Zip Code</label>
            <?php
            $zipcode = array('name' => 'zipcode',
                'id' => 'zipcode',
                'class' => 'form-control',
                'style' => 'margin-bottom:15px;'
            );
            $zip_val = !empty($user->zipcode) ? $user->zipcode : '';
            ?>
            <?php echo form_input($zipcode, $zip_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Date of Birth</label>
            <?php
            $dob = array('name' => 'dob',
                'id' => 'dob',
                'class' => 'form-control',
            );
            if (!empty($user->dob) && ($user->dob != '0000-00-00')) {
                $dob_val1 = $user->dob;
                $dob_val2 = date_create($dob_val1);
                $dob_val = date_format($dob_val2, "m-d-Y");
            } else {
                $dob_val = '';
            }
            ?>
            <?php echo form_input($dob, $dob_val); ?>
        </div>
        <div class="form-group">
            <label class="">Gender</label><br/>
            <div style="">
                <span style="margin-right: 5px; float: left;"> Male</span>
                <?php
                $male = array('name' => 'gender',
                    'id' => 'gender',
                    'class' => 'form-control',
                    'style' => 'margin-right:10px; float:left; width:auto;height:auto;'
                );
                if (!empty($user) && ($user->gender == 1)) {
                    $is_male = TRUE;
                } else {
                    $is_male = FALSE;
                }
                if (empty($user)) {
                    $is_male = TRUE;
                }
                ?>
                <?php echo form_radio($male, '1', $is_male); ?>
                <span style="margin-right: 5px;  float: left;">Female</span>
                <?php
                $female = array('name' => 'gender',
                    'id' => 'gender',
                    'class' => 'form-control',
                    'style' => 'margin-right:10px; float:left; width:auto; height:auto;'
                );
                if (!empty($user) && ($user->gender == 0))
                    $is_female = TRUE;
                else
                    $is_female = FALSE;
                ?>
                <?php echo form_radio($female, '0', $is_female); ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Phone</label>
            <?php
            $phone = array('name' => 'phone',
                'id' => 'phone',
                'class' => 'form-control',
            );
            $phone_val = !empty($user->phone) ? $user->phone : '';
            ?>
            <?php echo form_input($phone, $phone_val); ?>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="">Profile Picture</label>
            <?php
            $file = array(
                'name' => 'userfile',
                'id' => 'userfile',
            );
            ?>
            <?php echo form_upload($file); ?>          
        </div>
        <div class="clearfix"></div>
        <div class="form_btn_wrap">
            <?php echo form_submit(array('class' => 'btn btn-default save', 'id' => 'save', 'name' => 'add'), 'Save'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<link href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/css/fileinput.min.css" rel="stylesheet"/>
<script src="<?php echo base_url(); ?>assets/js/fileinput.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#dob').datetimepicker({
            format: 'd/m/Y',
            timepicker:false
        });
        jQuery("#userfile").fileinput({
            showUpload: false
        });
    });
</script>