<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('admin/admin-header'); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Users</h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
        $message = $this->session->flashdata('message');
        $msg_type = $this->session->flashdata('msg_type');
        ?>
        <?php if (isset($message)): ?>
            <div class="alert <?php echo!empty($msg_type) ? $msg_type : ''; ?>">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php if (!empty($users)): ?>
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="suppliers-list">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th class="nosort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td>
                                    <?php echo $user->first_name; ?>
                                </td>
                                <td>
                                    <?php echo $user->last_name; ?>
                                </td>
                                <td>
                                    <?php echo $user->email; ?>
                                </td>
                                <td>
                                    <?php
                                    $edit_attr = array(
                                        "title" => "Edit"
                                    );
                                    echo anchor("user/edit/$user->uid", "<i class='fa fa-edit'></i>", $edit_attr);
                                    ?>
                                    <?php
                                    $del_attr = array(
                                        "onClick" => "return confirm('Are you sure you want to delete?')",
                                        "title" => "Delete"
                                    );
                                    echo anchor("user/delete/$user->uid", "<i class='fa fa-trash'></i>", $del_attr);
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php $this->load->view('admin/admin-footer'); ?>
<!-- DataTables CSS -->
<link href="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="<?php echo base_url(); ?>assets/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    jQuery(document).ready(function () {
        jQuery('#suppliers-list').DataTable({
            'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': ['nosort']
                }]
        });
    });
</script>